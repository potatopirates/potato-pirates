#include "EnemyHG.h"



EnemyHG::EnemyHG()
{
}

EnemyHG::EnemyHG(System system, glm::vec2 startPosition, Detection * playerDetection) :
	EnemyBaseClass(system, startPosition, playerDetection, 2)
{
	movementPatternID_ = LINEAR;
	animatedTexture_ = PotatoEngine::AnimatedTexture(system_.animationManager->getAnimation("Gunner"));

	size_ = glm::vec2(
		system_.cameraView->getSize().x / 16,
		system_.cameraView->getSize().y / 9);

	detectionSize_ = glm::vec2(size_.x * 2, size_.y * 2);
	detectionRadious = detectionSize_.x / 2;


	//Set life
	life_ = 3;
	//Setup hitbox
	hitBoxMargin_ = glm::vec4(
		size_.x * -0.3,
		0,
		size_.x * 0.55,
		0
		);
}


EnemyHG::~EnemyHG()
{
}

void EnemyHG::fire(std::vector<Bullet>* bullets, Player* player)
{
	if (playerDetection_->detected && fireDelay_ <= 0)
	{
		movementPatternID_ = SLOW_SINOID;
		
		glm::vec2 bulletPos = position_;
		bulletPos.y += 37.5;
		bulletDirection_.x = -1;
		bulletDirection_.y = -0.5;
		



		for (int i = 0; i < 3; i++)
		{
			
			bullets->emplace_back(
				system_,
				bulletPos,
				bulletDirection_,
				BulletOwner::ENEMY
				);
			bulletDirection_.y += 0.5;
		}
		fireDelay_ = 60;
	}
}

void EnemyHG::update(float deltaTime, Player* player)
{
	if (hitColor_ < 255)
	{
		hitColor_ += 10 * deltaTime;
		color_.r = hitColor_;
		color_.g = hitColor_;
		color_.b = hitColor_;
	}
	else if (hitColor_ > 255)
	{
		hitColor_ = 255;
		color_.r = hitColor_;
		color_.g = hitColor_;
		color_.b = hitColor_;
	}


	if (fireDelay_ > 0)
	{
		fireDelay_ -= 1 * deltaTime;
	}

	//Moves enemy plane
	movementPattern(deltaTime, player);
}
