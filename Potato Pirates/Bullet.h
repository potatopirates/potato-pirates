#ifndef POTATOPIRATES_BULLET_H
#define POTATOPIRATES_BULLET_H

#include <PotatoEngine\SpriteBatch.h>
#include <glm\glm.hpp>
#include "System.h"

enum BulletOwner
{
	PLAYER,
	ENEMY
};

class Bullet
{
public:
	//Constructors
	Bullet();
	Bullet(System system, glm::vec2 position, glm::vec2 direction, BulletOwner bulletOwner, float bulletSpeed = 5);
	//Deconstructor
	~Bullet();

	bool update(float deltaTime);

	void draw(PotatoEngine::SpriteBatch* spriteBatch);

	bool collideWithObject(glm::vec4 objectPS);

	int getDamage() const;

	BulletOwner getOwner() const;

	glm::vec4 getHitBox() const;

private:
	System system_;
	glm::vec2 position_;
	glm::vec2 direction_;

	glm::vec4 hitBoxMargin_;
	PotatoEngine::AnimatedTexture animatedTexture_;
	glm::vec2 size_;

	int damage_;
	float speed_;
	float bulletWidth_ = 25;
	BulletOwner bulletOwner_;

};


#endif //POTATOPIRATES_BULLET_H