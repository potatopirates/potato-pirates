#include "SoundBufferCache.h"



SoundBufferCache::SoundBufferCache()
{
}


SoundBufferCache::~SoundBufferCache()
{
}

sf::SoundBuffer * SoundBufferCache::getSoundBuffer(std::string soundPath)
{
	//Lookup the soundbuffer and see if its in the map
	std::map<std::string, sf::SoundBuffer>::iterator mit = soundBufferMap_.find(soundPath);

	//Check if its not in the map
	if (mit == soundBufferMap_.end())
	{

		//Create the soundbuffer
		sf::SoundBuffer newSoundBuffer;
		//And load it from the file path
		newSoundBuffer.loadFromFile(soundPath);

		//Insert the soundbuffer into the map
		soundBufferMap_.insert(make_pair(soundPath, newSoundBuffer));

		//Return the loaded soundbuffer.
		return &soundBufferMap_.at(soundPath);
	}

	//Return the soundbuffer that is in the map.
	return &mit->second;
}
