#include "EntityCreation.h"
#include <iostream>
#include <fstream>

#include "EnemyAce.h"
#include "EnemyBlimp.h"
#include "EnemyHG.h"
#include "EnemyHunter.h"
#include "EnemyScout.h"

EntityCreation::EntityCreation()
{
}

EntityCreation::EntityCreation(System system, Detection* playerDetection, std::vector<EnemyBaseClass*>* enemies, std::vector<Pickup>* pickUps) :
	system_(system),
	playerDetection_(playerDetection),
	waveNumber_(0),
	waveDelay_(150),
	enemies_(enemies),
	pickUps_(pickUps)
{
	
}


EntityCreation::~EntityCreation()
{
}

void EntityCreation::update(float deltaTime)
{
	waveDelay_ -= deltaTime * 1;
	if (waveDelay_ <= 0 &&
		levelData_.size() > waveNumber_)
	{
		waveDelay_ = 300;
		waveCreation(waveNumber_);
		waveNumber_++;
	}
}


void EntityCreation::loadWaves(std::string filepath)
{
	std::ifstream file;
	file.open(filepath);
	//Error checking
	if (file.fail())
	{
		std::cout << "Failed to open enemy spawn file" << std::endl;
	}

	std::string data;
	std::vector<std::string> waveData;

	while (std::getline(file, data))
	{
		if (data == "NEW_WAVE")
		{
			levelData_.emplace_back(waveData);
			waveData.clear();
		}
		else
		{
			waveData.push_back(data);
		}
	}
}


void EntityCreation::waveCreation(int waveNumber)
{
	unsigned int yLines = static_cast<unsigned int>(levelData_[0].size());
	unsigned int xLines = static_cast<unsigned int>(levelData_.size());

	std::vector<std::string> waveData;
	waveData = levelData_[waveNumber];
	sf::Vector2f xPos = system_.window->mapPixelToCoords(sf::Vector2i(system_.window->getSize().x, system_.window->getSize().y));
	for (unsigned int x = 0; x < waveData.size(); x++)
		{
			xPos.x += system_.cameraView->getSize().x / 16;

			for (unsigned int y = 0; y < waveData[x].size(); y++)
			{
				//Grab the data
				char tile = waveData[x][y];
				if (tile == '0')
					continue;

				float yPos = y * system_.cameraView->getSize().y / 20;
				yPos -= system_.cameraView->getSize().y / 20;

				switch (tile)
				{
				case 'A':
					enemies_->push_back(new EnemyAce(system_,
						glm::vec2(
							xPos.x,
							yPos),
						playerDetection_));
					break;
				case 'S':
					if (!playerDetection_->detected)
					{
						enemies_->push_back(new EnemyScout(system_,
							glm::vec2(
								xPos.x,
								yPos),
							playerDetection_));
						break;
					}
					else if (playerDetection_->detected)
					{
						enemies_->push_back(new EnemyAce(system_,
							glm::vec2(
								xPos.x,
								yPos),
							playerDetection_));
						break;
					}
				case 'G':
					enemies_->push_back(new EnemyHG(system_,
						glm::vec2(
							xPos.x,
							yPos),
						playerDetection_));
					break;
				case 'B':
					enemies_->push_back(new EnemyBlimp(system_,
						glm::vec2(
							xPos.x,
							yPos),
						playerDetection_));
					break;
				case 'H':
					enemies_->push_back(new EnemyHunter(system_,
						glm::vec2(
							xPos.x,
							yPos),
						playerDetection_));
					break;
				case 'F':
					pickUps_->push_back(Pickup(system_,
						glm::vec2(
							xPos.x,
							yPos),
						glm::vec2(
							120,
							120),
						FIREWORK
							));
					break;
				case 'M':
					pickUps_->push_back(Pickup(system_,
						glm::vec2(
							xPos.x,
							yPos),
						glm::vec2(120,
							120),
						MOONSHINE
						));
					break;
				case 'P':
					pickUps_->push_back(Pickup(system_,
						glm::vec2(
							xPos.x,
							yPos),
						glm::vec2(120,
							120),
						POTATOCLOUD
						));
					break;
				case 'C':
					pickUps_->push_back(Pickup(system_,
						glm::vec2(
							xPos.x,
							yPos),
						glm::vec2(120,
							120),
						CARGO
						));
					break;
				default:
					break;
				}
			}
	}
}
