#ifndef POTATOENGINE_CAMERA2D_H
#define POTATOENGINE_CAMERA2D_H
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "CollisionManager.h"

namespace PotatoEngine
{
	class Camera2D
	{
	public:
		//Constructors
		Camera2D();
		//Deconstructor
		~Camera2D();

		void init(int screenWidth, int screenHeight, CollisionManager *collisionmanager);	///Initilize the camera

		void update();																		///Update the camera matrix if needed

		glm::vec2 convertScreenToWorld(glm::vec2 screenCoords);								///Convert screen to world cordinates.

		bool isBoxInView(const glm::vec2 &Position, const glm::vec2 &dimensions);			///Returns true if the inputed box is inside the camera view.
		bool isBoxInView(const glm::vec4 &destRect);										///Same as ^ but instead accepts a vec4

																							//setter
		void setPosition(glm::vec2 &newPosition)											///Set the position of the camera
		{
			position_ = newPosition; needs_matrix_update_ = true;
		}
		void setScale(float newScale)														///Set the scale of the camera
		{
			scale_ = newScale; needs_matrix_update_ = true;
		}

		//getters
		glm::vec2 Camera2D::getPosition() { return position_; }							///Returns where the camera is positioned
		float getScale() { return scale_; }												///Returns the scale of the camera


		glm::mat4 getCameraMatrix() { return camera_matrix_; }								///REtrusn the generated camera matrix

	private:
		int screen_width_;
		int screen_height_;
		bool needs_matrix_update_;
		float scale_;
		glm::vec2 position_;
		glm::mat4 camera_matrix_;
		glm::mat4 ortho_matrix_;
		CollisionManager* collision_manager_;
	};
}
#endif // POTATOENGINE_CAMERA2D_H