#include "StateNewGame.h"
#include <fstream>
#include <iostream>
#include <Windows.h>

StateNewGame::StateNewGame(System system) :
	system_(system)
{
	initilizeGUI();

	//Setup background
	backgroundImage_ = PotatoEngine::GUIImage(system_.resourceManager,
		glm::vec2(0),
		glm::vec2(system_.cameraView->getSize().x, system_.cameraView->getSize().y),
		"resources/textures/newgameBG.png", glm::vec4(0.0f));
}


StateNewGame::~StateNewGame()
{
}

void StateNewGame::update(float deltaTime)
{
	for (unsigned int i = 0; i < GUIMenuButtons_.size(); i++)
	{
		GUIMenuButtons_[i].update(system_.inputManager, deltaTime);
	}

	GUITextBox_.update(system_.inputManager, deltaTime);

	if (GUITextBox_.wasEnterPressed())
	{
		GUIMenuButtons_[1].runFunction(deltaTime);
	}
}

void StateNewGame::draw()
{
	spriteBatch_.begin();

	//Draw the menu background
	backgroundImage_.draw(&spriteBatch_);

	//Draw all the gui menu objects.
	for (unsigned int i = 0; i < GUIMenuButtons_.size(); i++)
	{
		GUIMenuButtons_[i].draw(&spriteBatch_);
	}
	GUITextBox_.draw(&spriteBatch_);


	//Draw on screen
	spriteBatch_.end();
	system_.window->draw(spriteBatch_);


	//Draw text
	for (unsigned int i = 0; i < GUIMenuButtons_.size(); i++)
	{
		GUIMenuButtons_[i].drawText(system_.window);
	}

	GUITextBox_.drawText(system_.window);

}

void StateNewGame::initilizeGUI()
{
	float xHelper = static_cast<float>(system_.cameraView->getSize().x / 5.0f);
	float yHelper = static_cast<float>(system_.cameraView->getSize().y / 10.0f);

	float xSizeHelper = static_cast<float>(
		system_.cameraView->getSize().x / 2.0f - xHelper / 2);
	
	std::string imagePath = "resources/textures/Mainbutton_spritesheet.png";
	std::string fontPath = "resources/fonts/StardosStencil-Regular.ttf";
	unsigned int fontSize = 50;

	//Back
	GUIMenuButtons_.push_back(PotatoEngine::GUIButton(
		system_.resourceManager,
		glm::vec2(
			system_.cameraView->getSize().x * 0.05,
			system_.cameraView->getSize().y * 0.83),
		glm::vec2(xHelper + xHelper / 2, yHelper),
		imagePath,
		"Back",
		fontSize,
		fontPath,
		[=](PotatoEngine::GUIButton& object, float deltaTime)
	{
		*system_.gameState = GameStates::STATE_MENU;
		system_.audioManager->playSound("resources/sfx/button_click.ogg");
	}));


	PotatoEngine::SettingsParser* tmpSP = system_.playerSaveData;

	//Start Game
	GUIMenuButtons_.push_back(PotatoEngine::GUIButton(
		system_.resourceManager,
		glm::vec2(
			system_.cameraView->getSize().x / 2 - ((xHelper + xHelper / 2)/2),
			system_.cameraView->getSize().y * 0.5),
		glm::vec2(xHelper + xHelper / 2, yHelper),
		imagePath,
		"Start Game",
		fontSize,
		fontPath,
		[=](PotatoEngine::GUIButton& object, float deltaTime)
	{
		if (GUITextBox_.getText() == "")
		{
			return;
		}
		std::string path = *system_.gameDictionary;
		path += "\\saves\\";
		path += GUITextBox_.getText();
		path += ".potato";

		if (std::ifstream(path))
		{
			std::cout << path << ", EXIST!" << std::endl;
		}
		else
		{
			CreateDirectory(std::string(*system_.gameDictionary + "\\saves").c_str(), NULL);
			std::string defaultSaveData = \
				"# Save data, do not change if you don't want to CHEAT and know what you are doing!" \
				\
				"\n\n#CurrentLvl, used ingame, 0 = no level selected." \
				"\ncurrentLvl = 0" \
				\
				"\n\n#Lvl status" \
				"\n#0 first lvl; 1 = lvl 1 completed; 4 = completed game" \
				"\nlvlStatus = 0" \
				\
				"\n\n#Money and things" \
				"\nMoney = 0" \
				"\n\n#Upgrades" \
				"\nWings = 0" \
				"\nEngine = 0" \
				"\nCargo = 0" \
				"\nArmor = 0" \
				\
				"\n\n#Start power; 0 = nothing; 1 = firework; 2 = Moonshine; 3 = Potato Fries" \
				"\nStartPower = 0";
			std::ofstream out(path);
			out << defaultSaveData;
			out.close();

			system_.playerSaveData->loadFromFile(path);



			std::string settingsPath = *system_.gameDictionary;
			settingsPath += "/settings.txt";
			PotatoEngine::SettingsParser tmp;
			tmp.loadFromFile(settingsPath);
			tmp.set("latestGame", GUITextBox_.getText());
			tmp.saveToFile();
			system_.settingsParser->set("latestGame", GUITextBox_.getText());


			//Create file and start game
			*system_.gameState = GameStates::STATE_MISSIONSELECT;
			system_.audioManager->playSound("resources/sfx/button_click.ogg");
		}
	}));

	//Textbox
	GUITextBox_ = PotatoEngine::GUITextBox(
		system_.resourceManager,
		glm::vec2(
			system_.cameraView->getSize().x * 0.30,
			system_.cameraView->getSize().y * 0.45),
		glm::vec2(
			system_.cameraView->getSize().x * 0.40,
			yHelper/2),
		"resources/textures/textBox.png",
		fontPath,
		"");
	GUITextBox_.setInputing(true);


}
