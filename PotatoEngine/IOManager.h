#ifndef POTATOENGINE_IOMANAGER_H
#define POTATOENGINE_IOMANAGER_H
#include <vector>

namespace PotatoEngine
{
	class IOManager
	{
	public:
		IOManager();
		~IOManager();

		static bool readFileToBuffer(
			std::string filePath,std::vector<unsigned char> &buffer);


		static bool readFileToString(
			std::string filePath, std::string &output);

		
	};
}

#endif // POTATOENGINE_IOMANAGER_H