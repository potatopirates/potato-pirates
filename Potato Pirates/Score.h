#ifndef POTATOPIRATES_SCORE_H
#define POTATOPIRATES_SCORE_H
#include "Player.h"

class Score
{
public:
	//Constructors
	Score();
	//Deconstructors
	~Score();

	///<summary>Updates the score.</summary>
	void update(float deltaTime);

	///<summary>Resets the score parameters.</summary>
	void resetScore();

	///<summary>Returns the total in-game score.</summary>
	int getScore();

	///<summary>Returns the total score upon level completion.</summary>
	int getVictoryScore();

	//Variable for keeping track of battlestate actions
	int evasions_;
	int kills_;
	int pickUps_;
	int life_;
	int ammo_;

private:
	int evasionScore_;
	int killScore_;
	int pickUpScore_;
	int totalScore_;
	int lifeScore_;
	float ammoScore_;
	int endScore_;
};

#endif //POTATOPIRATES_SCORE_H