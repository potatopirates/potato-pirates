#ifndef POTATOPIRATES_PICKUP_H
#define POTATOPIRATES_PICKUP_H
#include "System.h"
#include <glm\glm.hpp>
#include <PotatoEngine\SpriteBatch.h>
#include <PotatoEngine\Converter.h>

enum PickupType
{
	CARGO,
	FIREWORK,
	MOONSHINE,
	POTATOCLOUD
};
class Pickup
{
public:
	//COnstructors
	Pickup();
	Pickup(System system,
		glm::vec2 pos,
		glm::vec2 size,
		PickupType pickupType);

	//Deconstructors
	~Pickup();

	bool leftWindow();

	void draw(PotatoEngine::SpriteBatch* spriteBatch);
	void update(float deltaTime);
	//Getters
	PickupType getType() const;

	glm::vec4 getHitBox() const;

private:
	//Variables
	System system_;
	PotatoEngine::AnimatedTexture animatedPickup_;
	//Area/pos stats
	glm::vec4 dest_;
	sf::IntRect uvRect_;
	sf::Color color_;

	//Statery
	PickupType pickupType_;
};

#endif // POTATOPIRATES_PICKUP_H