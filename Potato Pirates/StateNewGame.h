#ifndef POTATOPIRATES_STATENEWGAME_H
#define POTATOPIRATES_STATENEWGAME_H

#include "GameState.h"
#include "System.h"
#include "PotatoEngine\SpriteBatch.h"
#include "PotatoEngine\GUI.h"
class StateNewGame :
	public GameState
{
public:
	//Constructors
	StateNewGame(System system);
	//Deconstructor
	~StateNewGame();


	/// <summary>
	/// Updates the state
	/// </summary>
	///<param name="deltaTime">Time used for physics calculating.</param>
	/// <returns></returns>
	void update(float deltaTime);

	/// <summary>Draw the state on the system window</summary>
	void draw();

private:
	void initilizeGUI();

	//Variables
	System system_;

	PotatoEngine::SpriteBatch spriteBatch_;
	
	//GUI elements
	std::vector<PotatoEngine::GUIButton> GUIMenuButtons_;
	PotatoEngine::GUITextBox GUITextBox_;


	//Background
	PotatoEngine::GUIImage backgroundImage_;
};

#endif // POTATOPIRATES_STATENEWGAME_H