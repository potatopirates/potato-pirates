#include "ResourceLoader.h"


ResourceLoader::ResourceLoader(System system)
	:
	system_(system)
{

	backgroundImage_ = PotatoEngine::GUIImage(
		system_.resourceManager,
		glm::vec2(
			0,
			0),
		glm::vec2(
			system_.window->getSize().x,
			system_.window->getSize().y),
		"resources/textures/resource_loader_BG.png"
		
		);

	loadedResourceMap_["Title"] = PotatoEngine::GUILabel(
		system_.resourceManager,
		glm::vec2(
			system_.window->getSize().x /2,
			system_.window->getSize().y * 0.10),
		"Loading Resources",
		"resources/fonts/StardosStencil-Bold.ttf",
		system_.window->getSize().y * 0.07
		);
	loadedResourceMap_["Title"].setColor(sf::Color(255,255,255,255));
	loadedResourceMap_["Title"].setOrigin(
		PotatoEngine::StylePos::X_MIDDLE | PotatoEngine::StylePos::Y_MIDDLE);


	loadedResourceMap_["Loader"] = PotatoEngine::GUILabel(
		system_.resourceManager,
		glm::vec2(
			system_.window->getSize().x / 2 - system_.window->getSize().x / 4,
			system_.window->getSize().y * 0.20),
		"Loading: Loader",
		"resources/fonts/StardosStencil-Regular.ttf",
		system_.window->getSize().y * 0.05
		);
	loadedResourceMap_["Loader"].setColor(sf::Color(255, 255, 0, 255));
	loadedResourceMap_["Loader"].setOrigin(
		PotatoEngine::StylePos::X_LEFT | PotatoEngine::StylePos::Y_MIDDLE);


	loadedResourceMap_["Animations"] = PotatoEngine::GUILabel(
		system_.resourceManager,
		glm::vec2(
			system_.window->getSize().x / 2 - system_.window->getSize().x / 4,
			system_.window->getSize().y * 0.25),
		"Waiting: Animations",
		"resources/fonts/StardosStencil-Regular.ttf",
		system_.window->getSize().y * 0.05
		);
	loadedResourceMap_["Animations"].setColor(sf::Color(255, 0, 0, 255));
	loadedResourceMap_["Animations"].setOrigin(
		PotatoEngine::StylePos::X_LEFT | PotatoEngine::StylePos::Y_MIDDLE);

	loadedResourceMap_["Backgrounds"] = PotatoEngine::GUILabel(
		system_.resourceManager,
		glm::vec2(
			system_.window->getSize().x / 2 - system_.window->getSize().x / 4,
			system_.window->getSize().y * 0.30),
		"Waiting: Backgrounds",
		"resources/fonts/StardosStencil-Regular.ttf",
		system_.window->getSize().y * 0.05
		);
	loadedResourceMap_["Backgrounds"].setColor(sf::Color(255, 0, 0, 255));
	loadedResourceMap_["Backgrounds"].setOrigin(
		PotatoEngine::StylePos::X_LEFT | PotatoEngine::StylePos::Y_MIDDLE);

	loadedResourceMap_["Level Tiles"] = PotatoEngine::GUILabel(
		system_.resourceManager,
		glm::vec2(
			system_.window->getSize().x / 2 - system_.window->getSize().x / 4,
			system_.window->getSize().y * 0.35),
		"Waiting: Level Tiles",
		"resources/fonts/StardosStencil-Regular.ttf",
		system_.window->getSize().y * 0.05
		);
	loadedResourceMap_["Level Tiles"].setColor(sf::Color(255, 0, 0, 255));
	loadedResourceMap_["Level Tiles"].setOrigin(
		PotatoEngine::StylePos::X_LEFT | PotatoEngine::StylePos::Y_MIDDLE);

	loadedResourceMap_["Hud elements"] = PotatoEngine::GUILabel(
		system_.resourceManager,
		glm::vec2(
			system_.window->getSize().x / 2 - system_.window->getSize().x / 4,
			system_.window->getSize().y * 0.40),
		"Waiting: Hud elements",
		"resources/fonts/StardosStencil-Regular.ttf",
		system_.window->getSize().y * 0.05
		);
	loadedResourceMap_["Hud elements"].setColor(sf::Color(255, 0, 0, 255));
	loadedResourceMap_["Hud elements"].setOrigin(
		PotatoEngine::StylePos::X_LEFT | PotatoEngine::StylePos::Y_MIDDLE);

	loadedResourceMap_["Ingame Objects"] = PotatoEngine::GUILabel(
		system_.resourceManager,
		glm::vec2(
			system_.window->getSize().x / 2 - system_.window->getSize().x / 4,
			system_.window->getSize().y * 0.45),
		"Waiting: Ingame Objects",
		"resources/fonts/StardosStencil-Regular.ttf",
		system_.window->getSize().y * 0.05
		);
	loadedResourceMap_["Ingame Objects"].setColor(sf::Color(255, 0, 0, 255));
	loadedResourceMap_["Ingame Objects"].setOrigin(
		PotatoEngine::StylePos::X_LEFT | PotatoEngine::StylePos::Y_MIDDLE);

	loadedResourceMap_["Other"] = PotatoEngine::GUILabel(
		system_.resourceManager,
		glm::vec2(
			system_.window->getSize().x / 2 - system_.window->getSize().x / 4,
			system_.window->getSize().y * 0.50),
		"Waiting: Other",
		"resources/fonts/StardosStencil-Regular.ttf",
		system_.window->getSize().y * 0.05
		);
	loadedResourceMap_["Other"].setColor(sf::Color(255, 0, 0, 255));
	loadedResourceMap_["Other"].setOrigin(
		PotatoEngine::StylePos::X_LEFT | PotatoEngine::StylePos::Y_MIDDLE);

}

ResourceLoader::~ResourceLoader()
{
	/*Empty*/
}

void ResourceLoader::loadAllResources()
{

	loadedResourceMap_["Loader"].setText("Done: Loader");
	loadedResourceMap_["Loader"].setColor(sf::Color(0, 255, 0, 255));
	loadedResourceMap_["Animations"].setText("Loading: Animations");
	loadedResourceMap_["Animations"].setColor(sf::Color(255, 255, 0, 255));
	updateScreen();
	
	loadAnimations();
	loadedResourceMap_["Animations"].setText("Done: Animations");
	loadedResourceMap_["Animations"].setColor(sf::Color(0, 255, 0, 255));
	loadedResourceMap_["Backgrounds"].setText("Loading: Backgrounds");
	loadedResourceMap_["Backgrounds"].setColor(sf::Color(255, 255, 0, 255));
	updateScreen();


	loadBackgrounds();
	loadedResourceMap_["Backgrounds"].setText("Done: Backgrounds");
	loadedResourceMap_["Backgrounds"].setColor(sf::Color(0, 255, 0, 255));
	loadedResourceMap_["Level Tiles"].setText("Loading: Level Tiles");
	loadedResourceMap_["Level Tiles"].setColor(sf::Color(255, 255, 0, 255));
	updateScreen();

	loadLevelTiles();
	loadedResourceMap_["Level Tiles"].setText("Done: Level Tiles");
	loadedResourceMap_["Level Tiles"].setColor(sf::Color(0, 255, 0, 255));
	loadedResourceMap_["Hud elements"].setText("Loading: Hud elements");
	loadedResourceMap_["Hud elements"].setColor(sf::Color(255, 255, 0, 255));
	updateScreen();

	loadHudElements();
	loadedResourceMap_["Hud elements"].setText("Done: Hud elements");
	loadedResourceMap_["Hud elements"].setColor(sf::Color(0, 255, 0, 255));
	loadedResourceMap_["Ingame Objects"].setText("Loading: Ingame Objects");
	loadedResourceMap_["Ingame Objects"].setColor(sf::Color(255, 255, 0, 255));
	updateScreen();

	ingameObjects();
	loadedResourceMap_["Ingame Objects"].setText("Done: Ingame Objects");
	loadedResourceMap_["Ingame Objects"].setColor(sf::Color(0, 255, 0, 255));
	loadedResourceMap_["Other"].setText("Loading: Other");
	loadedResourceMap_["Other"].setColor(sf::Color(255, 255, 0, 255));
	updateScreen();

	loadOther();
	loadedResourceMap_["Other"].setText("Done: Other");
	loadedResourceMap_["Other"].setColor(sf::Color(0, 255, 0, 255));
	updateScreen();

}

void ResourceLoader::updateScreen()
{
	system_.window->clear(sf::Color::Black);

	//Draw things
	spriteBatch_.begin();
	backgroundImage_.draw(&spriteBatch_);

	spriteBatch_.end();
	system_.window->draw(spriteBatch_);



	for (auto& it : loadedResourceMap_)
	{
		it.second.drawText(system_.window);
	}


	system_.window->display();
}


void ResourceLoader::loadBackgrounds()
{
	system_.resourceManager->getTexture("resources/textures/newgameBG.png");
	system_.resourceManager->getTexture("resources/textures/creditsBG.png");
	system_.resourceManager->getTexture("resources/textures/gameoverBG.png");
	system_.resourceManager->getTexture("resources/textures/highscoreBG.png");
	system_.resourceManager->getTexture("resources/textures/menuBG.png");
	system_.resourceManager->getTexture("resources/textures/missionSelectBG.png");
	system_.resourceManager->getTexture("resources/textures/optionsBG.png");
	system_.resourceManager->getTexture("resources/textures/victoryBG.png");
	system_.resourceManager->getTexture("resources/textures/clouds_bg.png");
	system_.resourceManager->getTexture("resources/textures/loadgameBG.png");
}

void ResourceLoader::loadLevelTiles()
{
	system_.resourceManager->getTexture("resources/maplvl/mapTiles_spritesheet.png");
}

void ResourceLoader::loadHudElements()
{
	system_.resourceManager->getTexture("resources/textures/arrowbtn_spritesheet.png");
	system_.resourceManager->getTexture("resources/textures/arrowbtnR_spritesheet.png");
	system_.resourceManager->getTexture("resources/textures/checkbutton_spritesheet.png");
	system_.resourceManager->getTexture("resources/textures/cursor.png");
	system_.resourceManager->getTexture("resources/textures/ICEHUD_spritesheet.png");
	system_.resourceManager->getTexture("resources/textures/icon.png");
	system_.resourceManager->getTexture("resources/textures/Mainbutton_spritesheet.png");
	system_.resourceManager->getTexture("resources/textures/missionSelectScreen_lvl1.png");
	system_.resourceManager->getTexture("resources/textures/missionSelectScreen_lvl2.png");
	system_.resourceManager->getTexture("resources/textures/missionSelectScreen_lvl3.png");
	system_.resourceManager->getTexture("resources/textures/missionSelectScreen_lvl4.png");

	system_.resourceManager->getTexture("resources/textures/noise_circle_spritesheet.png");
	system_.resourceManager->getTexture("resources/textures/radar.png");
	system_.resourceManager->getTexture("resources/textures/shop_background.png");
	system_.resourceManager->getTexture("resources/textures/shopbutton_spritesheet.png");
	system_.resourceManager->getTexture("resources/textures/slider_spritesheet.png");
	system_.resourceManager->getTexture("resources/textures/star_spritesheet.png");
	system_.resourceManager->getTexture("resources/textures/textBox.png");
	system_.resourceManager->getTexture("resources/textures/tutorial_spritesheet.png");
}

void ResourceLoader::ingameObjects()
{

	system_.resourceManager->getTexture("resources/textures/ace_spritesheet.png");
	system_.resourceManager->getTexture("resources/textures/avatar_spritesheet.png");
	system_.resourceManager->getTexture("resources/textures/blimp_spritesheet.png");
	system_.resourceManager->getTexture("resources/textures/bombCir.png");
	system_.resourceManager->getTexture("resources/textures/bullet_spritesheet.png");
	system_.resourceManager->getTexture("resources/textures/cargo_balloon_spritesheet.png");
	system_.resourceManager->getTexture("resources/textures/detection_circle_spritesheet.png");
	system_.resourceManager->getTexture("resources/textures/enemybullet_spritesheet.png");
	system_.resourceManager->getTexture("resources/textures/explosion_spritesheet.png");
	system_.resourceManager->getTexture("resources/textures/firework_spritesheet.png");
	system_.resourceManager->getTexture("resources/textures/fw_balloon_spritesheet.png");
	system_.resourceManager->getTexture("resources/textures/hg_spritesheet.png");
	system_.resourceManager->getTexture("resources/textures/hunter_spritesheet.png");
	system_.resourceManager->getTexture("resources/textures/ms_balloon_spritesheet.png");
	system_.resourceManager->getTexture("resources/textures/pc_balloon_spritesheet.png");
	system_.resourceManager->getTexture("resources/textures/scout_spritesheet.png");
}

void ResourceLoader::loadOther()
{
	system_.resourceManager->getTexture("resources/textures/hitboxArea.png");
	system_.resourceManager->getTexture("resources/textures/missionSelectHUD.png");
	system_.resourceManager->getTexture("");

	//Preload sounds (music is not preloaded due to it being streamed.
	system_.resourceManager->getSoundBuffer("resources/sfx/avatar hit.ogg");
	system_.resourceManager->getSoundBuffer("resources/sfx/button_click.ogg");
	system_.resourceManager->getSoundBuffer("resources/sfx/cargo.ogg");
	system_.resourceManager->getSoundBuffer("resources/sfx/enemiedead.ogg");
	system_.resourceManager->getSoundBuffer("resources/sfx/enemiehit.ogg");
	system_.resourceManager->getSoundBuffer("resources/sfx/firework explosion.ogg");
	system_.resourceManager->getSoundBuffer("resources/sfx/firework.ogg");
	system_.resourceManager->getSoundBuffer("resources/sfx/moonshine.ogg");
	system_.resourceManager->getSoundBuffer("resources/sfx/pickup sound.ogg");
	system_.resourceManager->getSoundBuffer("resources/sfx/plane explosion.ogg");
	system_.resourceManager->getSoundBuffer("resources/sfx/shooting sound.ogg");

	//Probeler sound ignored due to it being handled like a music
	//system_.resourceManager->getSoundBuffer("resources/sfx/propeller sound.ogg");
}