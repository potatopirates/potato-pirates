#ifndef POTATOPIRATES_HUNTER_H
#define POTATOPIRATES_HUNTER_H

#include "EnemyBaseClass.h"

class EnemyHunter :
	public EnemyBaseClass
{
public:
	//Constructors
	EnemyHunter();
	EnemyHunter(System system, glm::vec2 startPosition, Detection* playerDetection);
	//Deconstructors
	~EnemyHunter();

	void fire(std::vector<Bullet>* bullets, Player* player) override;
	void update(float deltaTime, Player* player) override;

private:
	float fireDelay_;
	float cooldown_;
	int counter_;
	glm::vec2 bulletDirection_;
};

#endif //POTATOPIRATES_HUNTER_H