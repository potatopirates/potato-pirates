#ifndef POTATOENGINE_CONVERTER_H
#define POTATOENGINE_CONVERTER_H
#include <SFML/Graphics.hpp>
#include <SFML\Window.hpp>
#include <glm\glm.hpp>

#include <random>

namespace PotatoEngine
{
	
	static sf::FloatRect convertToFloatRect(glm::vec4 value)
	{
		return sf::FloatRect(
			value.x,
			value.y,
			value.z,
			value.w
			);
	}

	template<class T>
	T base_name(T const & path, T const & delims = "/\\")
	{
		return path.substr(path.find_last_of(delims) + 1);
	}
	template<class T>
	T remove_extension(T const & filename)
	{
		typename T::size_type const p(filename.find_last_of('.'));
		return p > 0 && p != T::npos ? filename.substr(0, p) : filename;
	}



	static int getIntCharacterSize(int number)
	{
		if (number < 10)
			return 1;
		else
			return getIntCharacterSize(number / 10) + 1;
	}

	static float getRandomNumber(float min, float max)
	{
		std::mt19937 mt;
		mt.seed(time(nullptr));
		std::uniform_int_distribution<int> dist(min, max);

		return dist(mt);
	}

}

#endif // POTATOENGINE_CONVERTER_H