#ifndef POTATOPIRATES_RESOURCELOADER_H
#define POTATOPIRATES_RESOURCELOADER_H

#include "System.h"
#include <PotatoEngine/SpriteBatch.h>
#include <PotatoEngine/GUI.h>

class ResourceLoader
{
public:
	//Constructors
	ResourceLoader(System system);

	//Deconstructor
	~ResourceLoader();

	void loadAllResources();


private:
	//Functions
	void updateScreen();

	void loadAnimations();
	void loadBackgrounds();
	void loadLevelTiles();
	void loadHudElements();
	void ingameObjects();
	void loadOther();

	//Variables
	System system_;
	PotatoEngine::SpriteBatch spriteBatch_;

	std::unordered_map<std::string, PotatoEngine::GUILabel> loadedResourceMap_;

	PotatoEngine::GUIImage backgroundImage_;
};

#endif // POTATOPIRATES_RESOURCELOADER_H