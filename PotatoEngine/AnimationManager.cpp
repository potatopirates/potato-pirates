#include "AnimationManager.h"
#include <iostream>
namespace PotatoEngine
{
	Animation::Animation(sf::Texture * texture, float animationSpeed)
		:
		spriteSheet_(texture),
		animationSpeed_(animationSpeed)
	{
		/*Empty*/
	}

	Animation::Animation()
	{

	}


	AnimationManager::AnimationManager()
	{
		/*Empty*/
	}

	AnimationManager::~AnimationManager()
	{
		/*Empty*/
	}

	bool AnimationManager::createAnimation(std::string animationName, 
		sf::Texture * animationTexture,
		float animationSpeed)
	{
		std::map<std::string, Animation>::iterator mit = animationMap_.find(animationName);
		//First check so the animation name isen't in use, if it is. return false.
		if (mit == animationMap_.end())
		{
			Animation newAnimation(animationTexture, animationSpeed);
			animationMap_.insert(make_pair(animationName, newAnimation));
			return true;
		}
		return false;
	}

	void AnimationManager::addFrameToAnimation(std::string animationName, glm::vec4 uvRect)
	{
		std::map<std::string, Animation>::iterator mit = animationMap_.find(animationName);
		
		if (mit != animationMap_.end())
		{
			mit->second.frams_.emplace_back(uvRect.x, uvRect.y, uvRect.z, uvRect.w);
		}
	}

	Animation* AnimationManager::getAnimation(std::string animation)
	{
		std::map<std::string, Animation>::iterator mit = animationMap_.find(animation);

		if (mit != animationMap_.end())
		{
			return &mit->second;
		}
		return nullptr;
	}
	




	AnimatedTexture::AnimatedTexture() :
		currentTick_(0),
		currentFrame_(0)
	{
	}

	AnimatedTexture::AnimatedTexture(
		Animation * animation)
		:
		animation_(animation),
		currentTick_(0),
		currentFrame_(0)
	{

	}

	bool AnimatedTexture::update(float deltaTime)
	{
		if (++currentTick_ >= animation_->animationSpeed_)
		{
			currentTick_ = 0;
			if (++currentFrame_ >= animation_->frams_.size())
			{
				currentFrame_ = 0;
				return true;
			}
		}
		return false;
	}

	void AnimatedTexture::draw(SpriteBatch * spritebatch, glm::vec4 destRect, sf::Color color)
	{
		spritebatch->draw(
			animation_->spriteSheet_,
			sf::FloatRect(
				destRect.x,
				destRect.y,
				destRect.z,
				destRect.w), 
			animation_->frams_[currentFrame_],
			color);
	}
	
	void AnimatedTexture::setAnimation(Animation * animation)
	{
		animation_ = animation;
	}


	void AnimatedTexture::setCurrentFrame(unsigned int frame)
	{
		currentFrame_ = frame;
	}

	unsigned int AnimatedTexture::getTotalFrams() const
	{
		return animation_->frams_.size();
	}
}