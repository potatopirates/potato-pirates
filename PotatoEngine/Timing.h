#ifndef POTATOENGINE_TIMING_H
#define POTATOENGINE_TIMING_H

#include <SFML\System.hpp>
namespace PotatoEngine
{
	class FpsLimiter
	{
	public:
		FpsLimiter();
		void init(float maxFPS);

		void setMaxFPS(float maxFPS);

		void begin();

		//End return the current FPS
		float end();

		//Getters
		float getElapsedTime() const;

	private:
		void calculateFPS();

		float fps_;
		float maxFPS_;
		float frame_time_;
		unsigned int start_ticks_;

		sf::Clock clock_;
	};
}
#endif // POTATOENGINE_TIMING_H