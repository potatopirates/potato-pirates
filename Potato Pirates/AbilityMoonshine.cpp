#include "AbilityMoonshine.h"



AbilityMoonshine::AbilityMoonshine()
{
}

AbilityMoonshine::AbilityMoonshine(System system) :
	system_(system),
	isActive_(false),
	duration_(0),
	updateAnimation_(true)
{
	moonshineEffect_ = PotatoEngine::AnimatedTexture(system_.animationManager->getAnimation("MoonshineEffect"));
	effectColor_ = sf::Color(255, 255, 255, 75);
}


AbilityMoonshine::~AbilityMoonshine()
{
}

void AbilityMoonshine::update(float deltaTime)
{

	if (isActive() && duration_ > 0)
	{
		if (updateAnimation_ && moonshineEffect_.update(deltaTime))
		{
			effectColor_ = sf::Color(255, 255, 255, 150);
			moonshineEffect_.setCurrentFrame(moonshineEffect_.getTotalFrams() - 1);
			updateAnimation_ = false;

		}
		duration_ -= 1 * deltaTime;
	}
	else 
	{
		isActive_ = false;
	}
}


void AbilityMoonshine::draw(PotatoEngine::SpriteBatch * spriteBatch)
{
	moonshineEffect_.draw(spriteBatch,
		glm::vec4(system_.window->mapPixelToCoords(sf::Vector2i(0, 0)).x,
			system_.window->mapPixelToCoords(sf::Vector2i(0, 0)).y,
			1920,
			1080),
		effectColor_);

}

bool AbilityMoonshine::isActive()
{
	return isActive_;
}

void AbilityMoonshine::setDuration(float duration)
{
	duration_ = duration;
}

void AbilityMoonshine::setActive()
{
	isActive_ = true;
	setDuration(300);
	system_.audioManager->playSound("resources/sfx/moonshine.ogg");

	effectColor_ = sf::Color(255, 255, 255, 75);
	moonshineEffect_.setCurrentFrame(0);
	updateAnimation_ = true;

}
