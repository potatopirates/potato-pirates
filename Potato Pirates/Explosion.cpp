#include "Explosion.h"



Explosion::Explosion()
{
	/*Empty*/
}

Explosion::Explosion(System system, glm::vec4 destRect)
	:
	system_(system),
	destRect_(destRect)
{
	animatedTexture_ = PotatoEngine::AnimatedTexture(
		system_.animationManager->getAnimation("Explosion"));
}


Explosion::~Explosion()
{
	/*Empty*/
}

void Explosion::draw(PotatoEngine::SpriteBatch * spriteBatch)
{
	animatedTexture_.draw(
		spriteBatch,
		destRect_);
}

bool Explosion::update(float deltaTime)
{
	return animatedTexture_.update(deltaTime);
}
