#include "EnemyHunter.h"



EnemyHunter::EnemyHunter()
{
}

EnemyHunter::EnemyHunter(System system, glm::vec2 startPosition, Detection * playerDetection) :
	EnemyBaseClass(system, startPosition, playerDetection, 3)
{
	movementPatternID_ = LINEAR;
	animatedTexture_ = PotatoEngine::AnimatedTexture(system_.animationManager->getAnimation("Hunter"));

	size_ = glm::vec2(
		system_.cameraView->getSize().x / 16,
		system_.cameraView->getSize().y / 9);

	detectionSize_ = glm::vec2(size_.x * 2.3, size_.y * 2.3);
	detectionRadious = detectionSize_.x / 2;

	//Set life
	life_ = 1;
	//Setup hitbox
	hitBoxMargin_ = glm::vec4(
		size_.x * -0.3,
		0,
		size_.x * 0.55,
		0
		);
}


EnemyHunter::~EnemyHunter()
{
}

void EnemyHunter::fire(std::vector<Bullet>* bullets, Player * player)
{
	if (playerDetection_->detected && fireDelay_ <= 0)
	{
		movementPatternID_ = HOMING;

		glm::vec2 bulletPos = position_;
		bulletPos.y += 37.5;
		if (counter_ == 0 && player->getPos().x <= position_.x)
		{
			bulletDirection_ = glm::normalize(player->getPos() - bulletPos);
		}
		else if (counter_ == 0 && player->getPos().x > position_.x)
		{
			bulletDirection_ = glm::vec2(-1, 0);
		}

		counter_++;
		fireDelay_ = 15;


		bullets->emplace_back(
			system_,
			bulletPos,
			bulletDirection_,
			BulletOwner::ENEMY,
			15
			);
		if (counter_ >= 5)
		{
			counter_ = 0;
			cooldown_ = 60;
		}
	}

	
}



void EnemyHunter::update(float deltaTime, Player * player)
{
	if (hitColor_ < 255)
	{
		hitColor_ += 10 * deltaTime;
		color_.r = hitColor_;
		color_.g = hitColor_;
		color_.b = hitColor_;
	}
	else if (hitColor_ > 255)
	{
		hitColor_ = 255;
		color_.r = hitColor_;
		color_.g = hitColor_;
		color_.b = hitColor_;
	}


	if (fireDelay_ > 0)
	{
		fireDelay_ -= 1 * deltaTime;
	}

	//Move enemy plane
	movementPattern(deltaTime, player);
}
