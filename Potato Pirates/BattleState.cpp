#include "BattleState.h"
#include <PotatoEngine\ErrorHandler.h>
#include <PotatoEngine\ResourceManager.h>

#include <iostream>
#include <fstream>
#include <random>
#include <math.h>  

#include "EnemyAce.h"
#include "EnemyScout.h"
#include "EnemyBlimp.h"
#include "EnemyHG.h"
#include "EnemyHunter.h"

BattleState::BattleState(System system, Score* score)
	:
	system_(system),
	score_(score),
	drawHitBox_(false),
	blackScreen_(0)
{

	cloudBackgroundTexture_ = system_.resourceManager->getTexture("resources/textures/clouds_bg.png");
	cloudBackgroundTexture_->setRepeated(true);

	testLevelTexture_ = system_.resourceManager->getTexture("resources/maplvl/mapTiles_spritesheet.png");


	lvlProgress_ = 0;

	playerDetection_.alerted = false;
	playerDetection_.detected = false;
	playerDetection_.alertedCounter = 1;

	player_ = Player(system_, &bullets_, &playerDetection_, &fireworks_);
	system_.audioManager->playMusic("resources/music/noire#1 radio.ogg", true);
	propeler = PotatoEngine::AudioManager(system_.resourceManager);
	propeler.setMusicVolume(system_.audioManager->getSoundVolume() * 0.2);

	propeler.playMusic("resources/sfx/propeller sound.ogg", true);


	hud_ = HUD(system_, &player_, &playerDetection_, score_);
	
	entityCreation_ = EntityCreation(system_, &playerDetection_, &enemys_, &pickups_);

	system_.resetView();
	*system_.cursor = false;

	loader = new TMXLoader();
	system_.playerSaveData->get("currentLvl", levelNumber_);

	switch (levelNumber_)
	{
	case 1:
		loader->loadMap("Level", "resources/maplvl/Level_1.tmx");
		entityCreation_.loadWaves("resources/maplvl/Level_1.potatoWave");
		score_->resetScore();
		break;
	case 2:
		loader->loadMap("Level", "resources/maplvl/Level_2.tmx");
		entityCreation_.loadWaves("resources/maplvl/Level_2.potatoWave");
		score_->resetScore();
		break;
	case 3: 
		loader->loadMap("Level", "resources/maplvl/Level_3.tmx");
		entityCreation_.loadWaves("resources/maplvl/Level_3.potatoWave");
		score_->resetScore();
		break;
	case 4:
		loader->loadMap("Level", "resources/maplvl/Level_4.tmx");
		entityCreation_.loadWaves("resources/maplvl/Level_4.potatoWave");
		score_->resetScore();
		break;
	default:
		//ERROR // CRASH
		score_->resetScore();
		break;
	}

	initilizeMenuButtons();
}

BattleState::~BattleState()
{
	delete loader;
	propeler.stopMusic();
}

void BattleState::update(float deltaTime)
{
	menuhandler(deltaTime);
	if (gamePaused_)
	{
		return;
	}

	//Entity wave creation
	entityCreation_.update(deltaTime);

//Moonshine

	if (player_.isMoonshineActive())
	{
		deltaTime /= 2;
	}

	//Update enemies units
	for (unsigned int i = 0; i < enemys_.size();)
	{
		enemys_[i]->update(deltaTime, &player_);
		enemys_[i]->fire(&bullets_, &player_);

		if (system_.collisionManager->AABB(
			player_.getHitBox(),
			enemys_[i]->getHitBox()))
		{
			player_.setLife(0);
		}

		for (unsigned int j = 0; j < fireworks_.size(); j++)
		{
			if (fireworks_[j].isActive() &&
				system_.collisionManager->AABB(
					fireworks_[j].getHitBox(),
					enemys_[i]->getHitBox()))
			{
				fireworks_[j].detonate();
			}
		}

		if (enemys_[i]->getDetectionCir().x + enemys_[i]->getDetectionCir().z < system_.window->mapPixelToCoords(sf::Vector2i(0, 0)).x)
		{
			if (!playerDetection_.detected && player_.getLife() > 0)
			{
				//Show score feedback
				scoreFeedbacks_.emplace_back(
					system_,
					997,
					glm::vec2(
						system_.window->mapPixelToCoords(sf::Vector2i(100, 0)).x,
						enemys_[i]->getDetectionCir().y
						)
					);

				score_->evasions_++;
			}
			delete (enemys_[i]);
			enemys_[i] = enemys_.back();
			enemys_.pop_back();
		}
		else
		{
			i++;
		}
	}



	//Update bullets.
	for (unsigned int i = 0; i < bullets_.size();)
	{
		if (bullets_[i].update(deltaTime))
		{
			bullets_[i] = bullets_.back();
			bullets_.pop_back();
		}
		else
		{
			if (bullets_[i].getOwner() == ENEMY)
			{

				if ( player_.getLife() > 0 && system_.collisionManager->AABB(
					bullets_[i].getHitBox(), player_.getHitBox()))
				{
					bullets_[i] = bullets_.back();
					bullets_.pop_back();
					//Decrease player life
					player_.decreaseLife(1);
				}
				else
				{
					i++;
				}
			}
			else
			{
				bool tmp = true;
				for (int j = 0; j < enemys_.size();)
				{
					if (bullets_[i].collideWithObject(enemys_[j]->getHitBox()))
					{
						bullets_[i] = bullets_.back();
						bullets_.pop_back();
						//Decrease enemy life
						enemys_[j]->setLife(enemys_[j]->getLife() - 1);
						if (enemys_[j]->getLife() == 0)
						{
							killEnemy(j);
						}
						tmp = false;
						break;
					}
					else
					{
						j++;
					}
				}
				if (tmp)
					i++;
			}	
		}
	}
	

	//Update  the pickups
	for (unsigned int i = 0; i < pickups_.size();)
	{
		pickups_[i].update(deltaTime);
		if (pickups_[i].leftWindow())
		{
			pickups_[i] = pickups_.back();
			pickups_.pop_back();
		}
		else if (system_.collisionManager->AABB(pickups_[i].getHitBox(),
			player_.getHitBox()))
		{
			score_->pickUps_++;
			player_.addPickup(pickups_[i].getType());

			//Show score feedback
			scoreFeedbacks_.emplace_back(
				system_,
				1048,
				glm::vec2(
					player_.getNoiseCir().x,
					player_.getNoiseCir().y
					)
				);

			pickups_[i] = pickups_.back();
			pickups_.pop_back();
			//Sound effect
			system_.audioManager->playSound("resources/sfx/pickup sound.ogg");
		}
		else
		{
			i++;
		}
	}

	//Fireworks
	for (unsigned int i = 0; i < fireworks_.size();)
	{
		if (fireworks_[i].isActive())
		{
			if (fireworks_[i].update(deltaTime))
			{
				fireworks_[i].setActive(false);

				//Sound effect
				system_.audioManager->playSound("resources/sfx/firework explosion.ogg");

				//Add explosions area
				glm::vec3 tmp = fireworks_[i].getExplosionCir();
				explosions_.emplace_back(system_,
					glm::vec4(
						tmp.x - tmp.z,
						tmp.y - tmp.z,
						tmp.z * 2,
						tmp.z * 2
						));

				//Kill everyone!

				for (unsigned int j = 0; j < enemys_.size();)
				{
					if (system_.collisionManager->circleRectCollision(
						fireworks_[i].getExplosionCir(),
						enemys_[j]->getHitBox()))
					{
						//Decrease enemy life
						enemys_[j]->setLife(enemys_[j]->getLife() - 3);
						if (enemys_[j]->getLife() <= 0)
						{
							killEnemy(j);
						}
						else
						{
							j++;
						}
					}
					else
					{
						j++;
					}
				}

				for (unsigned int j = 0; j < bullets_.size();)
				{
					if (system_.collisionManager->circleRectCollision(
						fireworks_[i].getExplosionCir(),
						bullets_[j].getHitBox()))
					{
						bullets_[j] = bullets_.back();
						bullets_.pop_back();
					}
					else
					{
						j++;
					}
				}

				if (system_.collisionManager->circleRectCollision(
					fireworks_[i].getExplosionCir(),
					player_.getHitBox()))
				{
					player_.setLife(0);
				}
			}
			i++;
		}
		else
		{
			fireworks_[i] = fireworks_.back();
			fireworks_.pop_back();
		}

	}
	
	//Detection update
	if (!playerDetection_.detected)
	{
		playerDetection_.alerted = false;

		//Update enemey units
		for (int i = 0; i < enemys_.size(); i++)
		{
			if (system_.collisionManager->circleCollision(player_.getNoiseCir(),
				enemys_[i]->getDetectionCir()))
			{
				playerDetection_.alerted = true;
				break;
			}
		}

		if (!playerDetection_.alerted)
		{
			playerDetection_.alertedCounter = 60;
		}
		else
		{
			playerDetection_.alertedCounter -= deltaTime;
			if (playerDetection_.alertedCounter <= 0)
			{
				playerDetection_.detected = true;
			}
		}
	}
	else if (blackScreen_ == 0 && system_.audioManager->getLatestMusicPlayed() != "resources/music/Tri-Tachyon_-_01_-_Little_Lily_Swing.ogg")
	{
		system_.audioManager->playMusic("resources/music/Tri-Tachyon_-_01_-_Little_Lily_Swing.ogg", true);
	}




	//Update explosions
	for (unsigned int i = 0; i < explosions_.size();)
	{
		if (explosions_[i].update(deltaTime))
		{
			explosions_[i] = explosions_.back();
			explosions_.pop_back();
		}
		else
		{
			i++;
		}
	}

	//Update score feedback
	for (unsigned int i = 0; i < scoreFeedbacks_.size();)
	{
		if (scoreFeedbacks_[i].update(deltaTime))
		{
			scoreFeedbacks_[i] = scoreFeedbacks_.back();
			scoreFeedbacks_.pop_back();
		}
		else
		{
			i++;
		}
	}


	hud_.update(deltaTime);
	
	//Check if player wins
	if (player_.getLife() <= 0) //Check if player dead.
	{
		if (blackScreen_ == 0)
		{
			glm::vec3 tmp = player_.getNoiseCir();
			explosions_.emplace_back(
				system_,
				glm::vec4(
					tmp.x - tmp.z / 2,
					tmp.y - tmp.z / 2,
					tmp.z,
					tmp.z
					));

			if (system_.audioManager->getLatestMusicPlayed() != "resources/music/For the Fallen.ogg")
			{
				system_.audioManager->playMusic("resources/music/For the Fallen.ogg", true);
			}
			//Sound effect
			system_.audioManager->playSound("resources/sfx/plane explosion.ogg");

		}
		blackScreen_ += 4 * deltaTime;

		if (blackScreen_ >= 255)
		{
			blackScreen_ = 255;
			*system_.gameState = GameStates::STATE_GAMEOVER;
		}
		return;
	} 
	else if (lvlProgress_ >= 2690)//6)//675) //2690
	{
		if (blackScreen_ == 0)
		{
			glm::vec3 tmp = player_.getNoiseCir();
			explosions_.emplace_back(
				system_,
				glm::vec4(
					tmp.x - tmp.z / 2,
					tmp.y - tmp.z / 2,
					tmp.z,
					tmp.z
					));

			if (system_.audioManager->getLatestMusicPlayed() != "resources/music/Feelin Good radio.ogg")
			{
				system_.audioManager->playMusic("resources/music/Feelin Good radio.ogg", true);
			}

			//Update completed lvl info
			int lvlStatus = 0;
			system_.playerSaveData->get("lvlStatus", lvlStatus);

			if (lvlStatus < levelNumber_)
			{
				lvlStatus = levelNumber_;
				system_.playerSaveData->set("lvlStatus", lvlStatus);
			}
		}
		blackScreen_ += 4 * deltaTime;

		if (blackScreen_ >= 255)
		{
			blackScreen_ = 255;
			*system_.gameState = GameStates::STATE_VICTORY;
		}
		return;
	}

	lvlProgress_ += deltaTime * 0.5f;

	system_.cameraView->move(sf::Vector2f(1 * deltaTime, 0));
	system_.window->setView(*system_.cameraView);
	if (player_.isMoonshineActive())
	{
		player_.update(deltaTime * 1.5);

	}
	else
	{
		player_.update(deltaTime);
	}
	

	score_->update(deltaTime);
	score_->ammo_ = player_.getAmmo();
	score_->life_ = static_cast<int>(player_.getLife());

}

void BattleState::draw()
{
	spriteBatch_.begin();
	// 1920 x 1080
	//Draw background
	int x = static_cast<int>(system_.cameraView->getSize().x);
	int y = static_cast<int>(system_.cameraView->getSize().y);
	//Cord helper
	sf::Vector2f tmp = system_.window->mapPixelToCoords(sf::Vector2i(1920, 1080));

	//Draw Level
	
	unsigned int tileID = 0;
	int tileWidth = loader->getMap("Level")->getTileWidth();
	int tileHeight = loader->getMap("Level")->getTileHeight();

	int levelWdith = loader->getMap("Level")->getWidth();
	int levelHeight = loader->getMap("Level")->getHeight();


	float minWidthPrint = system_.window->mapPixelToCoords(sf::Vector2i(0, 0)).x;
	minWidthPrint = ceil(minWidthPrint / 120) - 1;
	
	
	float maxWidthPrint = system_.window->mapPixelToCoords(sf::Vector2i(system_.window->getSize())).x;
	maxWidthPrint = ceil(maxWidthPrint / 120);

	int tilesX = 16, tilesY = 16;

	for (int i = 0; i < levelHeight; i++)
	{
		for (int j = minWidthPrint;  j < maxWidthPrint && j < levelWdith; ++j)
		{
			// get the tile at current position
			tileID = loader->getMap("Level")->getTileLayer("Tile Layer 1")->getTileVector()[i][j];
			// only render if it is an actual tile (tileID = 0 means no tile / don't render anything here)
			if (tileID > 0)
			{
				spriteBatch_.draw(testLevelTexture_,
					sf::FloatRect(
						j * 120 - 0.1,
						i * 120 - 0.1,
						120 + 0.2,
						120 + 0.2),
					sf::IntRect(
						((tileID - 1) % tilesX) * tileWidth,
						((tileID - 1) / tilesY) * tileHeight,
						tileWidth,
						tileHeight),
					sf::Color(255, 255, 255, 255));
			}

			tileID = loader->getMap("Level")->getTileLayer("Tile Layer 2")->getTileVector()[i][j];
			// only render if it is an actual tile (tileID = 0 means no tile / don't render anything here)
			if (tileID > 0)
			{
				spriteBatch_.draw(testLevelTexture_,
					sf::FloatRect(
						j * 120 - 0.1,
						i * 120 - 0.1,
						120 + 0.2,
						120 + 0.2),
					sf::IntRect(
						((tileID - 1) % tilesX) * tileWidth,
						((tileID - 1) / tilesY) * tileHeight,
						tileWidth,
						tileHeight),
					sf::Color(255, 255, 255, 255));
			}

			//Layer 3
			tileID = loader->getMap("Level")->getTileLayer("Tile Layer 3")->getTileVector()[i][j];
			// only render if it is an actual tile (tileID = 0 means no tile / don't render anything here)
			if (tileID > 0)
			{
				spriteBatch_.draw(testLevelTexture_,
					sf::FloatRect(
						j * 120 - 0.1,
						i * 120 - 0.1,
						120 + 0.2,
						120 + 0.2),
					sf::IntRect(
						((tileID - 1) % tilesX) * tileWidth,
						((tileID - 1) / tilesY) * tileHeight,
						tileWidth,
						tileHeight),
					sf::Color(255, 255, 255, 255));
			}
		}
	}

	//Draw cloud
	spriteBatch_.draw(cloudBackgroundTexture_,
		sf::FloatRect(0.0f, 0.0f, static_cast<float>(tmp.x), static_cast<float>(tmp.y)),
		sf::IntRect(static_cast<int>(lvlProgress_*11), 0, x, system_.window->getSize().y),
		sf::Color(255, 255, 255, 255));

	

	//Draw the bullets on the screen
	for (unsigned int i = 0; i < bullets_.size(); i++)
	{
		bullets_[i].draw(&spriteBatch_);
	}

	//Draw the firework
	for (unsigned int i = 0; i < fireworks_.size(); i++)
	{
		if (fireworks_[i].isActive())
		{
			fireworks_[i].draw(&spriteBatch_);
		}
	}
	
	//Draw pickup ballonss
	for (unsigned int i = 0; i < pickups_.size(); i++)
	{
		pickups_[i].draw(&spriteBatch_);
	}


	//Draw the enemys
	for (unsigned int i = 0; i < enemys_.size(); i++)
	{
		enemys_[i]->draw(&spriteBatch_);
	}

	for (unsigned int i = 0; i < explosions_.size(); i++)
	{
		explosions_[i].draw(&spriteBatch_);
	}



	//Draw the player or black screen
	if (player_.getLife() > 0)
	{
		player_.draw(&spriteBatch_);
	}


	if (drawHitBox_)
	{
		drawHitBoxs();
	}

	hud_.draw(&spriteBatch_);


	if (blackScreen_ > 0)
	{
		spriteBatch_.draw(
			system_.resourceManager->getTexture("resources/textures/hitboxArea.png"),
			sf::FloatRect(
				system_.window->mapPixelToCoords(sf::Vector2i(0, 0)).x,
				system_.window->mapPixelToCoords(sf::Vector2i(0, 0)).y,
				system_.window->mapPixelToCoords(sf::Vector2i(system_.cameraView->getSize())).x,
				system_.window->mapPixelToCoords(sf::Vector2i(system_.cameraView->getSize())).y),
			sf::IntRect(0, 0, 0, 0),
			sf::Color(0, 0, 0, static_cast<sf::Uint8>(blackScreen_)));
	}
	spriteBatch_.end();
	system_.window->draw(spriteBatch_);

	hud_.drawText(system_.window, blackScreen_);

	//Draw score feedback
	for (unsigned int i = 0; i < scoreFeedbacks_.size(); i++)
	{
		scoreFeedbacks_[i].draw(system_.window);
	}


	//Draw menu
	if (gamePaused_)
	{
		drawMenu();
	}
}

void BattleState::drawHitBoxs()
{

	//Draw pickup ballonss
	for (unsigned int i = 0; i < pickups_.size(); i++)
	{
		spriteBatch_.draw(
			system_.resourceManager->getTexture("resources/textures/hitboxArea.png"),
			PotatoEngine::convertToFloatRect(pickups_[i].getHitBox()),
			sf::IntRect(0, 0, 64, 64),
			sf::Color(0, 255, 0, 100)
			);
	}

	//Draw the bullets on the screen
	for (int i = 0; i < bullets_.size(); i++)
	{
		if (bullets_[i].getOwner() == ENEMY)
		{
			spriteBatch_.draw(
				system_.resourceManager->getTexture("resources/textures/hitboxArea.png"),
				PotatoEngine::convertToFloatRect(bullets_[i].getHitBox()),
				sf::IntRect(0, 0, 64, 64),
				sf::Color(255, 0, 0, 100)
				);
		}
		else
		{
			spriteBatch_.draw(
				system_.resourceManager->getTexture("resources/textures/hitboxArea.png"),
				PotatoEngine::convertToFloatRect(bullets_[i].getHitBox()),
				sf::IntRect(0, 0, 64, 64),
				sf::Color(0, 0, 255, 100)
				);
		}
	}

	//Draw the enemys
	for (int i = 0; i < enemys_.size(); i++)
	{
		spriteBatch_.draw(
			system_.resourceManager->getTexture("resources/textures/hitboxArea.png"),
			PotatoEngine::convertToFloatRect(enemys_[i]->getHitBox()),
			sf::IntRect(0, 0, 64, 64),
			sf::Color(255, 0, 0, 100)
			);
	}

	//Draw firework
	for (unsigned int i = 0; i < fireworks_.size(); i++)
	{
		spriteBatch_.draw(
			system_.resourceManager->getTexture("resources/textures/hitboxArea.png"),
			PotatoEngine::convertToFloatRect(fireworks_[i].getHitBox()),
			sf::IntRect(0, 0, 64, 64),
			sf::Color(0, 0, 255, 100)
			);
	}
	//Draw player hitbox
	spriteBatch_.draw(
		system_.resourceManager->getTexture("resources/textures/hitboxArea.png"),
		PotatoEngine::convertToFloatRect(player_.getHitBox()),
		sf::IntRect(0, 0, 64, 64),
		sf::Color(0, 0, 255, 100)
		);



}

void BattleState::initilizeMenuButtons()
{
	float xHelper = static_cast<float>(system_.cameraView->getSize().x / 5.0f);
	float yHelper = static_cast<float>(system_.cameraView->getSize().y / 10.0f);

	float xSizeHelper = static_cast<float>(xHelper + xHelper / 2);

	glm::vec2 margin(
		system_.cameraView->getSize().x / 2 - xHelper - 50,
		150);
	std::string imagePath = "resources/textures/Mainbutton_spritesheet.png";
	std::string fontPath = "resources/fonts/StardosStencil-Regular.ttf";
	unsigned int fontSize = 50;


	GUIMenuButtons_.push_back(PotatoEngine::GUIButton(
		system_.resourceManager,
		glm::vec2(
			system_.cameraView->getSize().x / 2 - xSizeHelper/2,
			system_.cameraView->getSize().y / 2),
		glm::vec2(
			xSizeHelper,
			yHelper),
		imagePath,
		"Continue",
		fontSize,
		fontPath,
		[=](PotatoEngine::GUIButton& object, float deltaTime)
	{
		gamePaused_ = !gamePaused_;
		*system_.cursor = !*system_.cursor;
		system_.audioManager->playSound("resources/sfx/button_click.ogg");
	}));


	GUIMenuButtons_.push_back(PotatoEngine::GUIButton(
		system_.resourceManager,
		glm::vec2(
			system_.cameraView->getSize().x / 2 - xSizeHelper / 2,
			system_.cameraView->getSize().y / 2 * 1.2),
		glm::vec2(
			xSizeHelper,
			yHelper),
		imagePath,
		"Mission Select",
		fontSize,
		fontPath,
		[=](PotatoEngine::GUIButton& object, float deltaTime)
	{
		*system_.gameState = GameStates::STATE_MISSIONSELECT;
		system_.audioManager->playSound("resources/sfx/button_click.ogg");
	}));

	GUIMenuButtons_.push_back(PotatoEngine::GUIButton(
		system_.resourceManager,
		glm::vec2(
			system_.cameraView->getSize().x / 2 - xSizeHelper / 2,
			system_.cameraView->getSize().y / 2 * 1.4),
		glm::vec2(
			xSizeHelper,
			yHelper),
		imagePath,
		"Quit to menu",
		fontSize,
		fontPath,
		[=](PotatoEngine::GUIButton& object, float deltaTime)
	{
		*system_.gameState = GameStates::STATE_MENU;
		system_.audioManager->playSound("resources/sfx/button_click.ogg");
	}));

	GUIMenuButtons_.push_back(PotatoEngine::GUIButton(
		system_.resourceManager,
		glm::vec2(
			system_.cameraView->getSize().x / 2 - xSizeHelper / 2,
			system_.cameraView->getSize().y / 2 * 1.6),
		glm::vec2(
			xSizeHelper,
			yHelper),
		imagePath,
		"Quit to desktop",
		fontSize,
		fontPath,
		[=](PotatoEngine::GUIButton& object, float deltaTime)
	{
		*system_.gameState = GameStates::STATE_EXIT;
		system_.audioManager->playSound("resources/sfx/button_click.ogg");
	}));

	menuBackground_ = PotatoEngine::GUIImage(
		system_.resourceManager,
		glm::vec2(0, 0),
		glm::vec2(
			system_.cameraView->getSize().x,
			system_.cameraView->getSize().y),
		"",
		glm::vec4(0,0,0,0),
		sf::Color(0, 0, 0, 255 / 2));

	
	menuPauseLabel_ = PotatoEngine::GUILabel(
		system_.resourceManager,
		glm::vec2(
			system_.cameraView->getSize().x,
			system_.cameraView->getSize().y / 10),
		"Game Paused!",
		"resources/fonts/StardosStencil-Bold.ttf",
		system_.cameraView->getSize().y / 10);

	menuPauseLabel_.setOrigin(PotatoEngine::X_MIDDLE | PotatoEngine::Y_MIDDLE);
}

void BattleState::menuhandler(float deltaTime)
{
	static int aba = 5;
	if (aba > 0)
		aba--;

	if (system_.inputManager->isKeyPressed(sf::Keyboard::Escape) && aba <= 0)
	{
		system_.inputManager->setKeyPressedStatus(sf::Keyboard::Escape, false);
		gamePaused_ = !gamePaused_;
		*system_.cursor = !*system_.cursor;
		aba = 5;
	}

	if (gamePaused_)
	{
		float xHelper = static_cast<float>(system_.window->getSize().x / 5.0f);
		float xSizeHelper = static_cast<float>(xHelper + xHelper / 2);

		for (unsigned int i = 0; i < GUIMenuButtons_.size(); i++)
		{
			GUIMenuButtons_[i].update(system_.inputManager, deltaTime);
			glm::vec2 tmp = GUIMenuButtons_[i].getPos();

			tmp.x = system_.window->mapPixelToCoords(sf::Vector2i(
				(system_.window->getSize().x / 2) - (xSizeHelper / 2),
				0)).x;

			GUIMenuButtons_[i].setPos(tmp);
		}

		menuBackground_.setPos(
			glm::vec2(
				system_.window->mapPixelToCoords(sf::Vector2i(0,0)).x,
				system_.window->mapPixelToCoords(sf::Vector2i(0, 0)).y
				));

		sf::Vector2f tmp = 
			system_.window->mapPixelToCoords(
				sf::Vector2i(
			system_.window->getSize().x/2, 
			system_.window->getSize().y / 10));

		menuPauseLabel_.setPos(glm::vec2(
			tmp.x,
			tmp.y));

		menuPauseLabel_.setOrigin(PotatoEngine::X_MIDDLE | PotatoEngine::Y_MIDDLE);
	}
}

void BattleState::drawMenu()
{
	spriteBatch_.begin();

	//Draw the menu background
	menuBackground_.draw(&spriteBatch_);

	//Draw all the gui menu objects.
	for (unsigned int i = 0; i < GUIMenuButtons_.size(); i++)
	{
		GUIMenuButtons_[i].draw(&spriteBatch_);
	}

	spriteBatch_.end();

	system_.window->draw(spriteBatch_);

	for (unsigned int i = 0; i < GUIMenuButtons_.size(); i++)
	{
		GUIMenuButtons_[i].drawText(system_.window);
	}

	menuPauseLabel_.drawText(system_.window);
}

void BattleState::killEnemy(int ID)
{
	glm::vec3 tmp = enemys_[ID]->getDetectionCir();
	explosions_.emplace_back(
		system_,
		glm::vec4(
			tmp.x - tmp.z / 2,
			tmp.y - tmp.z / 2,
			tmp.z,
			tmp.z
			)
		);

	//Show score feedback
	scoreFeedbacks_.emplace_back(
		system_,
		1499,
		glm::vec2(
			tmp.x,
			tmp.y
			)
		);

	delete (enemys_[ID]);
	enemys_[ID] = enemys_.back();
	enemys_.pop_back();
	score_->kills_++;
}

