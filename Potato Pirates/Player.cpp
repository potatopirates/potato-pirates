#include "Player.h"
#include <random>
#include <iostream>
Player::Player()
{
	/*Empty*/
}

Player::Player(System system,
	std::vector<Bullet>* bullets,
	Detection* playerDetection,
	std::vector<AbilityFirework> * fireworks)
	:
	system_(system),
	bullets_(bullets),
	fireDelay_(0),
	life_(3),
	startLife_(3),
	ammo_(100),
	maxAmmo_(100),
	detection_(playerDetection),
	speed_(5),
	abilityOne_(3),
	abilityMoonshine_(3),
	abilityPotatoCloud_(3),
	fireworks_(fireworks),
	firework_(nullptr),
	playerAnimation_("Avatar_Idle"),
	tutorial_(false),
	shootSoundDelay_(0),
	powerupAvailable_(false)
{
	//Set up player variables
	playerSize_ = glm::vec2(
		120,
		120);
	pos_ = glm::vec2(system_.cameraView->getSize().x / 16, system_.cameraView->getSize().y / 2 - playerSize_.y / 2);

	//Set up noise circle
	color_ = sf::Color(255, 255, 255, 255);
	noiseSize_ = glm::vec2(playerSize_.x * 1.75, playerSize_.y * 1.75);
	noiseRad_ = noiseSize_.x / 2;
	
	noiseHelper = 0;

	noiseAnimatedTexture_ = PotatoEngine::AnimatedTexture(
		system_.animationManager->getAnimation("NoiseCircle"));

	//Setup player hitbox
	hitBoxMargin_ = glm::vec4(
		playerSize_.x * -0.5,
		playerSize_.y * -0.45,
		playerSize_.x * 0.9,
		playerSize_.y * 0.90
		);
	
	//Set up abilities
	fireworkCooldown_ = 0;
	animatedTexture_ = PotatoEngine::AnimatedTexture(
		system_.animationManager->getAnimation(playerAnimation_));
	moonshine_ = AbilityMoonshine(system_);
	potatoCloud_ = AbilityPotatoCloud(system_);


	//Tutorial
	int checkLvl1Complete;
	system_.playerSaveData->get("lvlStatus", checkLvl1Complete);
	if (checkLvl1Complete <= 0)
	{
		tutorialTexture_ = system_.resourceManager->getTexture("resources/textures/tutorial_spritesheet.png");
		tutorialKeyCheck_['W'] = true;
		tutorialKeyCheck_['A'] = true;
		tutorialKeyCheck_['S'] = true;
		tutorialKeyCheck_['D'] = true;
		tutorialKeyCheck_['E'] = true;
		tutorialKeyCheck_[' '] = true;

		tutorialKeyCheckAlphaLvl_['W'] = 255;
		tutorialKeyCheckAlphaLvl_['A'] = 255;
		tutorialKeyCheckAlphaLvl_['S'] = 255;
		tutorialKeyCheckAlphaLvl_['D'] = 255;
		tutorialKeyCheckAlphaLvl_['E'] = 0;
		tutorialKeyCheckAlphaLvl_[' '] = 255;
		tutorial_ = true;
	}

	ActivateDeveloperFeatures();
	activateUpgrades();
}


Player::~Player()
{
	/*Empty*/
}

void Player::inputHandler(float deltaTime)
{
	sf::Vector2f helper =
		system_.window->mapPixelToCoords(sf::Vector2i(
			system_.window->getSize().x,
			system_.window->getSize().y
			));

	//Handle Movement control
	if (system_.inputManager->isKeyDown(sf::Keyboard::W) 
		&&
		getHitBox().y > system_.window->getSize().y / 9)
	{
		pos_.y -= deltaTime * speed_;
		playerAnimation_ = "Avatar_TileUP";
		animatedTexture_.setAnimation(system_.animationManager->getAnimation(playerAnimation_));
	}
	else if (system_.inputManager->isKeyDown(sf::Keyboard::S) 
		&&
		getHitBox().y + getHitBox().w 
		< system_.cameraView->getSize().y - system_.window->getSize().y / 9)
	{
		pos_.y += deltaTime * speed_;
		playerAnimation_ = "Avatar_TileDown";
		animatedTexture_.setAnimation(system_.animationManager->getAnimation(playerAnimation_));
	}
	else
	{
		playerAnimation_ = "Avatar_Idle";
		animatedTexture_.setAnimation(system_.animationManager->getAnimation(playerAnimation_));
	}

	if (system_.inputManager->isKeyDown(sf::Keyboard::D) &&
		pos_.x + playerSize_.x < helper.x - system_.cameraView->getSize().x / 13)
	{
		pos_.x += deltaTime * speed_;
	}
	else if (system_.inputManager->isKeyDown(sf::Keyboard::A) &&
		getHitBox().x > helper.x - 1920 + system_.window->getSize().x / 16)
	{
		pos_.x -= deltaTime * speed_;
	}

	//Handle shooting
	if (system_.inputManager->isKeyDown(sf::Keyboard::Space) && fireDelay_ <= 0 && ammo_ > 0)
	{
		//*detected_ = true;
		//Shoot
		fireDelay_ = 20;

		glm::vec2 bulletPos = pos_;
		bulletPos.y += 37.5;
		bulletPos.x += 75;



		std::mt19937 rng;
		rng.seed(std::random_device()());
		std::uniform_int_distribution<std::mt19937::result_type> dist3(0, 50);

		float tmp = dist3(rng);
		tmp -= 25;

		tmp /= 4;

		//Decrease ammo
		if (!developerFeaturesMap_["infinityAmmo"])
		{
			ammo_--;
		}
		


		bullets_->emplace_back(
			system_,
			bulletPos,
			glm::vec2(1, 0.01*tmp),
			BulletOwner::PLAYER,
			10
			);

		if (shootSoundDelay_ <= 0)
		{
			float pitchery = (0.05 + 1) + (((float)rand()) / (float)RAND_MAX) * (1.95 - (0.05 + 1));
			system_.audioManager->playSound("resources/sfx/shooting sound.ogg", pitchery, false, 0.2);
			shootSoundDelay_ = 5;
		}


		noiseHelper += 10;
	}


	//Handles abilities
	if (system_.inputManager->isKeyPressed(sf::Keyboard::E)
		&&
		powerupAvailable_
		)
	{
		if (!developerFeaturesMap_["infinityPowerup"])
		{
			powerupAvailable_ = false;
		}
		system_.inputManager->setKeyPressedStatus(sf::Keyboard::E, false);
		switch (powerup_)
		{
		case Powerup::FIREWORK:
			system_.audioManager->playSound("resources/sfx/firework.ogg");
			fireworks_->emplace_back(system_, 
				glm::vec2(pos_.x + 75,
					pos_.y + 37.5));
			firework_ = &fireworks_->back();
			break;

		case Powerup::MOONSHINE:
			moonshine_.setActive();
			break;
		
		case Powerup::POTATOCLOUD:
			potatoCloud_.setActive();
				break;
		}


		
	} else if (system_.inputManager->isKeyPressed(sf::Keyboard::E)
		&&
		firework_ != nullptr 
		&&
		firework_->isActive()
		)
	{
		system_.inputManager->setKeyPressedStatus(sf::Keyboard::E, false);
		firework_->detonate();
		firework_ = nullptr;
	}

	//Firework
	/*
	if ((system_.inputManager->isKeyPressed(sf::Keyboard::Num1)
		||
		system_.inputManager->isKeyPressed(sf::Keyboard::Numpad1))
		&&
		fireworkCooldown_ <= 0
		)
	{
		fireworkCooldown_ = 10;
		system_.inputManager->setKeyPressedStatus(sf::Keyboard::Num1, false);
		system_.inputManager->setKeyPressedStatus(sf::Keyboard::Numpad1, false);

		if ((abilityOne_ > 0 && firework_ == nullptr)
			||
			(abilityOne_ > 0 && !firework_->isActive()))
		{
			abilityOne_--;
			//fireworkActive_ = true;

			glm::vec2 bulletPos = pos_;
			bulletPos.y += 37.5;
			bulletPos.x += 75;

			fireworks_->emplace_back(system_, bulletPos);
			firework_ = &fireworks_->back();

		}
		else if (firework_ != nullptr && firework_->isActive())
		{
			//fireworkActive_ = false;
			firework_->detonate();
			firework_ = nullptr;
		}
	}
	
	//Moonshine
	if (system_.inputManager->isKeyPressed(sf::Keyboard::Num2)
		||
		system_.inputManager->isKeyPressed(sf::Keyboard::Numpad2))
	{
		system_.inputManager->setKeyPressedStatus(sf::Keyboard::Num2, false);
		system_.inputManager->setKeyPressedStatus(sf::Keyboard::Numpad2, false);

		if (abilityMoonshine_ > 0)
		{
			abilityMoonshine_--;
			moonshine_.setActive();
		}
	}
	
	//Potato Cloud
	if (system_.inputManager->isKeyPressed(sf::Keyboard::Num3)
		||
		system_.inputManager->isKeyPressed(sf::Keyboard::Numpad3))
	{
		system_.inputManager->setKeyPressedStatus(sf::Keyboard::Num3, false);
		system_.inputManager->setKeyPressedStatus(sf::Keyboard::Numpad3, false);

		if (abilityPotatoCloud_ > 0)
		{
			abilityPotatoCloud_--;
			potatoCloud_.setActive();
		}
	}*/

}

void Player::ActivateDeveloperFeatures()
{

	//developerFeaturesMap_
	int extraLife = 0;
	int extraSpeed = 0;
	int extraAmmo = 0;
	system_.settingsParser->get("infinityAmmo", developerFeaturesMap_["infinityAmmo"]);
	system_.settingsParser->get("invincible", developerFeaturesMap_["invincible"]);
	system_.settingsParser->get("infinityPowerup", developerFeaturesMap_["infinityPowerup"]);
	system_.settingsParser->get("extraLife", extraLife);
	system_.settingsParser->get("extraSpeed", extraSpeed);
	system_.settingsParser->get("extraAmmo", extraAmmo);
	
	
	life_ += extraLife;
	startLife_ += extraLife;
	speed_ += extraSpeed;
	ammo_ += extraAmmo;
	maxAmmo_ += extraAmmo;

}

void Player::activateUpgrades()
{
	int engine = 0, cargo = 0, armor = 0;

	system_.playerSaveData->get("Engine", engine);
	system_.playerSaveData->get("Cargo", cargo);
	system_.playerSaveData->get("Armor", armor);

	switch (engine)
	{
	case 3:
		speed_ += 1;
	case 2:
		speed_ += 1;
	case 1:
		speed_ += 1;
	}

	switch (cargo)
	{
	case 3:
		maxAmmo_ += 50;
		ammo_ += 50;
	case 2:
		maxAmmo_ += 30;
		ammo_ += 30;
	case 1:
		maxAmmo_ += 20;
		ammo_ += 20;
	}

	switch (armor)
	{
	case 3:
		startLife_ += 1;
		life_ += 1;
	case 2:
		startLife_ += 1;
		life_ += 1;
	case 1:
		startLife_ += 1;
		life_ += 1;
	}


}

void Player::update(float deltaTime)
{
	sf::Vector2f helper =
	system_.window->mapPixelToCoords(sf::Vector2i(
		system_.window->getSize().x,
		system_.window->getSize().y
		));

	if (!detection_->detected)
	{
		noiseAnimatedTexture_.update(deltaTime);
	}

	if (hitProtection)
	{
		hitProtectionTimer -= deltaTime;
		if (hitProtectionTimer <= 0)
		{
			hitProtection = false;
			hitProtectionTimer = 0;
		}
	}

	if (shootSoundDelay_ > 0)
	{
		shootSoundDelay_ -= deltaTime;
	}

	//handle input
	inputHandler(deltaTime);

	//Handle fire delay
	if (fireDelay_ > 0)
	{
		fireDelay_ -= 1 * deltaTime;
	}


	//Make sure you don't leave the screen
	if (getHitBox().x < helper.x - 1920)
	{
		pos_.x = helper.x - 1920 + hitBoxMargin_.x;
	}

	//Move the player with the camera
	pos_.x += deltaTime * 1;

	//Handle abilities
	//Firework
	if (fireworkCooldown_ > 0)
	{
		fireworkCooldown_ -= 1 * deltaTime;
	}
	if (firework_ != nullptr && !firework_->isActive())
	{
		firework_ = nullptr;
	}
	
	//Moonshine
	if (moonshine_.isActive())
	{
		moonshine_.update(deltaTime);
	}
	
	//Potato Cloud
	if (potatoCloud_.isActive())
	{
		fireDelay_ = 7;

		glm::vec2 bulletPos = pos_;
		bulletPos.y += 37.5;
		bulletPos.x += 75;



		std::mt19937 rng;
		rng.seed(std::random_device()());
		std::uniform_int_distribution<std::mt19937::result_type> dist3(0, 50);

		float tmp = dist3(rng);
		tmp -= 25;

		tmp /= 4;

		bullets_->emplace_back(
			system_,
			bulletPos,
			glm::vec2(1, 0.01*tmp),
			BulletOwner::PLAYER,
			10
			);
		bullets_->emplace_back(
			system_,
			bulletPos,
			glm::vec2(1, 0.1*tmp),
			BulletOwner::PLAYER,
			10
			);
		bullets_->emplace_back(
			system_,
			bulletPos,
			glm::vec2(1, -0.1*tmp),
			BulletOwner::PLAYER,
			10
			);

		potatoCloud_.update(deltaTime);
	}



	//Noise circle
	if (noiseHelper > 0)
	{
		noiseHelper -= 0.1;
	}

	//Update animations
	animatedTexture_.update(deltaTime);


	//Tutorial
	if (tutorial_)
	{
		if (system_.inputManager->isKeyDown(sf::Keyboard::W))
		{
			tutorialKeyCheck_['W'] = false;
		}
		if (system_.inputManager->isKeyDown(sf::Keyboard::A))
		{
			tutorialKeyCheck_['A'] = false;
		}
		if (system_.inputManager->isKeyDown(sf::Keyboard::S))
		{
			tutorialKeyCheck_['S'] = false;
		}
		if (system_.inputManager->isKeyDown(sf::Keyboard::D))
		{
			tutorialKeyCheck_['D'] = false;
		}
		if (system_.inputManager->isKeyDown(sf::Keyboard::E))
		{
			tutorialKeyCheck_['E'] = false;
		}
		if (system_.inputManager->isKeyDown(sf::Keyboard::Space))
		{
			tutorialKeyCheck_[' '] = false;
		}


		if (!tutorialKeyCheck_['W'])
		{
			tutorialKeyCheckAlphaLvl_['W'] -= 5 * deltaTime;
			
			if (tutorialKeyCheckAlphaLvl_['W'] < 0)
			{
				tutorialKeyCheckAlphaLvl_['W'] = 0;
			}
		}

		if (!tutorialKeyCheck_['A'])
		{
			tutorialKeyCheckAlphaLvl_['A'] -= 5 * deltaTime;
			
			if (tutorialKeyCheckAlphaLvl_['A'] < 0)
			{
				tutorialKeyCheckAlphaLvl_['A'] = 0;
			}
		}

		if (!tutorialKeyCheck_['S'])
		{
			tutorialKeyCheckAlphaLvl_['S'] -= 5 * deltaTime;

			if (tutorialKeyCheckAlphaLvl_['S'] < 0)
			{
				tutorialKeyCheckAlphaLvl_['S'] = 0;
			}
		}

		if (!tutorialKeyCheck_['D'])
		{
			tutorialKeyCheckAlphaLvl_['D'] -= 5 * deltaTime;

			if (tutorialKeyCheckAlphaLvl_['D'] < 0)
			{
				tutorialKeyCheckAlphaLvl_['D'] = 0;
			}
		}
		if (!tutorialKeyCheck_['E'])
		{
			tutorialKeyCheckAlphaLvl_['E'] -= 5 * deltaTime;
			if (tutorialKeyCheckAlphaLvl_['E'] < 0)
			{
				tutorialKeyCheckAlphaLvl_['E'] = 0;
			}
		}
		if (!tutorialKeyCheck_[' '])
		{
			tutorialKeyCheckAlphaLvl_[' '] -= 5 * deltaTime;
			if (tutorialKeyCheckAlphaLvl_[' '] < 0)
			{
				tutorialKeyCheckAlphaLvl_[' '] = 0;
			}
		}

		if (tutorialKeyCheckAlphaLvl_['W'] <= 0 &&
			tutorialKeyCheckAlphaLvl_['A'] <= 0 &&
			tutorialKeyCheckAlphaLvl_['S'] <= 0 &&
			tutorialKeyCheckAlphaLvl_['D'] <= 0 &&
			tutorialKeyCheckAlphaLvl_['E'] <= 0)
		{
			tutorial_;
		}
	}
}

void Player::draw(PotatoEngine::SpriteBatch * spriteBatch)
{

	//If player is not detected, draw the detection circle around the player
	if (!detection_->detected)
	{
		/*
		spriteBatch->draw(noiseTexture_,
			sf::FloatRect(
				pos_.x + playerSize_.x / 2 - noiseSize_.x / 2 - noiseHelper / 2,
				pos_.y + playerSize_.y / 2 - noiseSize_.y / 2 - noiseHelper / 2,
				noiseSize_.x + noiseHelper,
				noiseSize_.y + noiseHelper),
			uvRect_,
			sf::Color(0,0,255,255));*/

		noiseAnimatedTexture_.draw(
			spriteBatch,
			glm::vec4(
				pos_.x + playerSize_.x / 2 - noiseSize_.x / 2 - noiseHelper / 2,
				pos_.y + playerSize_.y / 2 - noiseSize_.y / 2 - noiseHelper / 2,
				noiseSize_.x + noiseHelper,
				noiseSize_.y + noiseHelper));
	}

	//Draw the player animation
	animatedTexture_.draw(spriteBatch,
		glm::vec4(
			pos_.x - playerSize_.x * 0.40,
			pos_.y,
			playerSize_.x * 1.40,
			playerSize_.y),
		sf::Color(
			255 - hitProtectionTimer * 4.25,
			255 - hitProtectionTimer * 4.25,
			255 - hitProtectionTimer * 4.25,
			255));

	//If tutorialis enabled, draw tutorial elements.
	if (tutorial_)
	{

		spriteBatch->draw(
			tutorialTexture_,
			sf::FloatRect(
				pos_.x + (playerSize_.x / 2) - 32,
				pos_.y - playerSize_.y,
				64,
				64),
			sf::IntRect(
				0,
				0,
				tutorialTexture_->getSize().x / 4,
				tutorialTexture_->getSize().y / 3),
			sf::Color(255, 255, 255, tutorialKeyCheckAlphaLvl_['W']));


		spriteBatch->draw(
			tutorialTexture_,
			sf::FloatRect(
				pos_.x - playerSize_.x,
				pos_.y + playerSize_.y / 2 - 32,
				64,
				64),
			sf::IntRect(
				tutorialTexture_->getSize().x / 4,
				0,
				tutorialTexture_->getSize().x / 4,
				tutorialTexture_->getSize().y / 3),
			sf::Color(255, 255, 255, tutorialKeyCheckAlphaLvl_['A']));



		spriteBatch->draw(
			tutorialTexture_,
			sf::FloatRect(
				pos_.x + (playerSize_.x / 2) - 32,
				pos_.y + playerSize_.y + 64,
				64,
				64),
			sf::IntRect(
				tutorialTexture_->getSize().x / 4 * 2,
				0,
				tutorialTexture_->getSize().x / 4,
				tutorialTexture_->getSize().y / 3),
			sf::Color(255, 255, 255, tutorialKeyCheckAlphaLvl_['S']));



		spriteBatch->draw(
			tutorialTexture_,
			sf::FloatRect(
				pos_.x + playerSize_.x + 64,
				pos_.y + playerSize_.y / 2 - 32,
				64,
				64),
			sf::IntRect(
				tutorialTexture_->getSize().x / 4 * 3,
				0,
				tutorialTexture_->getSize().x / 4,
				tutorialTexture_->getSize().y / 3),
			sf::Color(255, 255, 255, tutorialKeyCheckAlphaLvl_['D']));

		spriteBatch->draw(
			tutorialTexture_,
			sf::FloatRect(
				system_.window->mapPixelToCoords(sf::Vector2i(system_.window->getSize().x / 2, 0)).x,
				system_.window->mapPixelToCoords(sf::Vector2i(0, system_.window->getSize().y / 1.3)).y,
				64,
				64),
			sf::IntRect(
				0,
				tutorialTexture_->getSize().y / 3,
				tutorialTexture_->getSize().x / 4,
				tutorialTexture_->getSize().y / 3),
			sf::Color(255, 255, 255, tutorialKeyCheckAlphaLvl_['E']));
	
		spriteBatch->draw(
			tutorialTexture_,
			sf::FloatRect(
				system_.window->mapPixelToCoords(sf::Vector2i(
					system_.window->getSize().x / 2,
					0)).x - system_.window->getSize().x / 8,
				system_.window->mapPixelToCoords(sf::Vector2i(0, system_.window->getSize().y / 1.1)).y,
				system_.window->getSize().x / 4,
				system_.window->getSize().y / 16),
			sf::IntRect(
				0,
				tutorialTexture_->getSize().y / 3 * 2,
				tutorialTexture_->getSize().x,
				tutorialTexture_->getSize().y / 3),
			sf::Color(255, 255, 255, tutorialKeyCheckAlphaLvl_[' ']));
	
	}

	if (moonshine_.isActive())
	{
		moonshine_.draw(spriteBatch);
	}
}

void Player::addPickup(PickupType type)
{
	switch (type)
	{
	case PickupType::CARGO:
		system_.audioManager->playSound("resources/sfx/cargo.ogg");
		ammo_ += 25;
		if (ammo_ > maxAmmo_)
		{
			ammo_ = maxAmmo_;
		}

		break;
		
	case PickupType::FIREWORK:
		powerup_ = Powerup::FIREWORK;
		powerupAvailable_ = true;
		break;

	case PickupType::MOONSHINE:
		powerup_ = Powerup::MOONSHINE;
		powerupAvailable_ = true;
		break;

	case PickupType::POTATOCLOUD:
		powerup_ = Powerup::POTATOCLOUD;
		powerupAvailable_ = true;
		break;
	}


}

void Player::decreaseLife(unsigned char amount)
{
	if (!developerFeaturesMap_["invincible"] && !hitProtection)
	{
		life_ -= amount;
		hitProtection = true;
		hitProtectionTimer = 60;
		//Sound effect
		system_.audioManager->playSound("resources/sfx/avatar hit.ogg");
	}
}


//Setters ------------------------------------------------------
//Setters ------------------------------------------------------
//Setters ------------------------------------------------------

void Player::setPos(float x,float y)
{
	pos_.x = x;
	pos_.y = y;
}

void Player::setPos(glm::vec2 pos)
{
	pos_.x = pos.x;
	pos_.y = pos.y;
}

void Player::setLife(unsigned char life)
{
	if (!developerFeaturesMap_["invincible"])
	{
		life_ = life;
	}
}

void Player::setAmmo(unsigned short ammo)
{
	ammo_ = ammo;
}

//Getters ------------------------------------------------------
//Getters ------------------------------------------------------
//Getters ------------------------------------------------------

float Player::getX() const
{
	return pos_.x;
}

float Player::getY() const
{
	return pos_.y;
}

glm::vec2 Player::getPos() const
{
	return pos_;
}

char Player::getLife() const
{
	return life_;
}

char Player::getStartLife() const
{
	return startLife_;
}

short Player::getAmmo() const
{
	return ammo_;
}

short Player::getMaxAmmo() const
{
	return maxAmmo_;
}

int Player::getFireworks() const
{
	return abilityOne_;
}

glm::vec2 Player::getSize() const
{
	return  playerSize_;
}


glm::vec4 Player::getHitBox() const
{
	return glm::vec4(
		pos_.x - hitBoxMargin_.x,
		pos_.y - hitBoxMargin_.y,
		playerSize_.x - hitBoxMargin_.z,
		playerSize_.y - hitBoxMargin_.w);
}


glm::vec3 Player::getNoiseCir() const
{
	return glm::vec3(pos_.x + playerSize_.x / 2,
		pos_.y + playerSize_.y / 2,
		noiseRad_ + noiseHelper/2);
}

bool Player::getFireworkActive() const
{
	if (firework_ != nullptr)
	{
		return firework_->isActive();
	}
	return false;
}

bool Player::isMoonshineActive()
{
	return moonshine_.isActive();
}

int Player::getObtainedPowerUp()
{
	if (powerupAvailable_)
	{
		if (tutorialKeyCheck_['E'] == true)
		{
			tutorialKeyCheckAlphaLvl_['E'] = 255;

		}

		return powerup_;

	}
	return -1;
}


