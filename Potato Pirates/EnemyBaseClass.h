#ifndef POTATOPIRATES_ENEMYBASECLASS_H
#define POTATOPIRATES_ENEMYBASECLASS_H
#include <PotatoEngine\SpriteBatch.h>

#include "Bullet.h"
#include "glm\glm.hpp"
#include "System.h"
#include "Player.h"

enum MOVEMENTPATTERN_ID
{
	SLOW_SINOID,
	FAST_SINOID,
	LINEAR,
	HOMING
};

class EnemyBaseClass
{
public:
	//Constructors
	EnemyBaseClass();
	EnemyBaseClass(System system, glm::vec2 startPosition, Detection* playerDetection, float speed);
	//Deconstructors
	~EnemyBaseClass();

	virtual void fire(std::vector<Bullet>* bullets, Player* player) = 0;
	virtual void update(float deltaTime, Player* player) = 0;

	void draw(PotatoEngine::SpriteBatch* spriteBatch);

	glm::vec4 getHitBox();

	glm::vec3 getDetectionCir() const;

	void setLife(char life);
	char getLife() const;

protected:
	void movementPattern(float deltaTime, Player* player);
	float detectionRadious;
	System system_;
	glm::vec2 position_;
	glm::vec2 direction_;
	glm::vec2 size_;
	glm::vec4 hitBoxMargin_;
	MOVEMENTPATTERN_ID movementPatternID_;

	glm::vec2 detectionSize_;

	sf::Color color_;

	float hitColor_;


	PotatoEngine::AnimatedTexture animatedTexture_;
	sf::Texture* detectionTexture;
	float rotation;

	Detection* playerDetection_;

	char life_;
	float speed_;
};

#endif //POTATOPIRATES_ENEMYBASECLASS_H