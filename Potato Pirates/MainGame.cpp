#include "MainGame.h"
#include "BattleState.h"
#include "StateMenu.h"
#include "DefeatState.h"
#include "VictoryState.h"
#include "StateOptionsMenu.h"
#include "StateMissionSelect.h"
#include "StateNewGame.h"
#include "StateLoadGame.h"
#include "StateHighScore.h"
#include "StateCredits.h"
#include <PotatoEngine/Timing.h>
#include <PotatoEngine/SpriteBatch.h>



#include <windows.h>
#include <stdio.h>  /* defines FILENAME_MAX */
#include <Shlwapi.h>
#include <fstream>
#include <iostream>

#pragma comment(lib, "shlwapi.lib")

#include "ResourceLoader.h"
MainGame::MainGame() :
	cursor_(true)
{
	score_ = Score();
}

MainGame::~MainGame()
{
}

void MainGame::run()
{
	//Init all basic system
	loadConfig();
	init_systems();

	system_ = System(
		&gameState_,
		&window_, 
		&cameraView_,
		&textureProgram_,
		&inputManager_,
		&resourceManager_,
		&audioManager_,
		&collisionManager_,
		&animationManager_,
		&settingsParaser_,
		&playerSaveData_,
		&cursor_,
		&gameDictionary_);


	//Load resources
	ResourceLoader rl(system_);
	rl.loadAllResources();

	//Set gamestate
	gameState_ = GameStates::STATE_MENU;
	switchGameState();
	main_loop();
}

void MainGame::init_systems()
{
	//Create the main window
	//window_.create(sf::VideoMode(1600, 900), "potato pirates", sf::Style::Titlebar);

	int width = 0;
	int height = 0;
	bool fullscreen = false;
	bool borderless = false;
	float musicVolume = 0;
	float sfxVolume = 0;

	settingsParaser_.get("width", width);
	settingsParaser_.get("height", height);
	settingsParaser_.get("fullscreen", fullscreen);
	settingsParaser_.get("borderless", borderless);
	settingsParaser_.get("music", musicVolume);
	settingsParaser_.get("sfx", sfxVolume);

	sf::Uint32 winStyle;

	if (fullscreen)
	{
		winStyle = sf::Style::Fullscreen;
	}
	else if(borderless)
	{ 
		winStyle = sf::Style::None;	
	} 
	else
	{
		winStyle = sf::Style::Titlebar;
	}

	window_.create(sf::VideoMode(width, height), "potato pirates", winStyle);


	cameraView_.reset(sf::FloatRect(0, 0, 1920, 1080));

	// Set its target viewport to be half of the window
	cameraView_.setViewport(sf::FloatRect(0.f, 0.f, 1.0f, 1.0f));


	//Compile the non existing shaders

	//Initializing the audio manager
	audioManager_ = PotatoEngine::AudioManager(&resourceManager_);

	//Setup custom cursor
	window_.setMouseCursorVisible(false); //Hide default cursor
	cursorTexture_ = resourceManager_.getTexture("resources/textures/cursor.png"); //Load cursor texture

	//Set up sound
	audioManager_.setMusicVolume(musicVolume);
	audioManager_.setSoundVolume(sfxVolume);

}

void MainGame::loadConfig()
{

	char ownPth[MAX_PATH];
	// Will contain exe path
	HMODULE hModule = GetModuleHandle(NULL);
	
	if (hModule != NULL)
	{
		// When passing NULL to GetModuleHandle, it returns handle of exe itself
		GetModuleFileName(hModule, ownPth, (sizeof(ownPth)));
		PathRemoveFileSpec(ownPth);
		gameDictionary_ = ownPth;

		std::string settingsPath = gameDictionary_;
		settingsPath += "/settings.txt";

		if (!settingsParaser_.loadFromFile(settingsPath))
		{
			std::cout << "Error loading settings file!" << std::endl;
			createDefaultConfig();
			if (!settingsParaser_.loadFromFile(settingsPath))
			{
				//This should not happen :(
				return;
			}
		}

		//Check if highsscore file exist, elease create new
		PotatoEngine::SettingsParser tmp;
		std::string highPath = gameDictionary_;
		highPath += "/highscore.potato";
		if (!tmp.loadFromFile(highPath))
		{
			std::cout << "Error loading highscore file!" << std::endl;
			createDefaultHighscore();
		}
	}
	else
	{
		std::cout << "Module handle is NULL" << std::endl;
		//Should not happen, will happen if non window user..
	}
}

void MainGame::createDefaultConfig()
{
	std::string settingsPath = gameDictionary_;
	settingsPath += "/settings.txt";

	
	std::string defaultConfig = \
		"# Config options, do not change if you don't know what you are doing!" \
		"\n" \
		"\n#GameSave" \
		"\nlatestGame = NULL" \
		"\n" \
		"\n# Window screen size" \
		"\nwidth = 1600" \
		"\nheight = 900" \
		"\n" \
		"\n# video mode" \
		"\nfullscreen = FALSE" \
		"\nborderless = FALSE" \
		"\n" \
		"\n# Audio settings" \
		"\nmusic = 100.0" \
		"\nsfx = 100.0" \
		"\n" \
		"\n#Developer Features" \
		"\ninfinityPowerup = FALSE" \
		"\ninfinityAmmo = FALSE" \
		"\ninvincible = FALSE" \
		"\nextraLife = 0" \
		"\nextraSpeed = 0" \
		"\nextraAmmo = 0";
		
		
		
	std::ofstream out(settingsPath);
	out << defaultConfig;
	out.close();
	std::cout << "Created default settings file!" << std::endl;
}

void MainGame::createDefaultHighscore()
{
	std::string highscorePath = gameDictionary_;
	highscorePath += "/highscore.potato";


	std::string defaultHighscore = \
		"Rank_1_Name = Bob" \
		"\nRank_1_Score = 1337" \
		"\nRank_2_Name = Bob" \
		"\nRank_2_Score = 1337" \
		"\nRank_3_Name = Bob" \
		"\nRank_3_Score = 1337" \
		"\nRank_4_Name = Bob" \
		"\nRank_4_Score = 1337" \
		"\nRank_5_Name = Bob" \
		"\nRank_5_Score = 1337" \
		"\nRank_6_Name = Bob" \
		"\nRank_6_Score = 1337" \
		"\nRank_7_Name = Bob" \
		"\nRank_7_Score = 1337" \
		"\nRank_8_Name = Bob" \
		"\nRank_8_Score = 1337" \
		"\nRank_9_Name = Bob" \
		"\nRank_9_Score = 1337" \
		"\nRank_10_Name = Bob" \
		"\nRank_10_Score = 1337";



	std::ofstream out(highscorePath);
	out << defaultHighscore;
	out.close();
	std::cout << "Created default highscore file!" << std::endl;
}

void MainGame::switchGameState()
{
	if (currentState_ != nullptr)
	{
		delete currentState_;
	}

	switch (gameState_)
	{
	case STATE_MENU:
		currentState_ = new StateMenu(system_);
		break;
	case STATE_BATTLESTATE:
		currentState_ = new BattleState(system_, &score_);
		break;
	case STATE_MISSIONSELECT:
		currentState_ = new StateMissionSelect(system_);
		break;
	case STATE_GAMEOVER:
		currentState_ = new DefeatState(system_);
		break;
	case STATE_VICTORY:
		currentState_ = new VictoryState(system_, &score_);
		break;
	case STATE_NEWGAME:
		currentState_ = new StateNewGame(system_);
		break;
	case STATE_LOADGAME:
		currentState_ = new StateLoadGame(system_);
		break;
	case STATE_OPTIONS:
		currentState_ = new StateOptionsMenu(system_);
		break;
	case STATE_HIGHSCORE:
		currentState_ = new StateHighScore(system_);
		break;
	case STATE_CREDITS:
		currentState_ = new StateCredits(system_);
		break;
	default:
		currentState_ = new StateMenu(system_);
		break;

	}
	gameState_ = GameStates::STATE_NULL;
}

void MainGame::processInput()
{
	//Update the input manager before we handle input data.
	inputManager_.update();
	sf::Event e;
	while (window_.pollEvent(e))
	{
		switch (e.type)
		{
		case sf::Event::Closed:
			gameState_ = GameStates::STATE_EXIT;
			break;
		case sf::Event::MouseMoved:
			inputManager_.setMouseCoords(
				window_.mapPixelToCoords(sf::Vector2i(e.mouseMove.x, e.mouseMove.y)).x,
				window_.mapPixelToCoords(sf::Vector2i(e.mouseMove.x, e.mouseMove.y)).y);

			break;
		case sf::Event::KeyPressed:
			inputManager_.pressKey(e.key.code);
			break;
		case sf::Event::KeyReleased:
			inputManager_.releaseKey(e.key.code);
			break;
		case sf::Event::MouseButtonPressed:
			if (e.mouseButton.button == sf::Mouse::Left)
			{
				inputManager_.pressMouse(PotatoEngine::LEFT);
			}
			break;
		case sf::Event::MouseButtonReleased:
			if (e.mouseButton.button == sf::Mouse::Left)
			{
				inputManager_.releaseMouse(PotatoEngine::LEFT);
			}
			break;
		}
	}
}

void MainGame::main_loop()
{
	PotatoEngine::FpsLimiter fps_limiter;
	fps_limiter.setMaxFPS(120.0f);
	//Const's used for delta time, (To make sure the game don't runt faster/slower based on the users FPS)
	const float DESIRED_FPS = 60.0f;
	const float MS_PER_SECOND = 1000.0f;
	const float DESIRED_FRAMETIME = MS_PER_SECOND / DESIRED_FPS;
	const float MAX_DELTA_TIME = 1.0f;
	const int MAX_PHYSICS_STEPS = 6;

	float previousTicks = fps_limiter.getElapsedTime();

	while (gameState_ != GameStates::STATE_EXIT)
	{
		fps_limiter.begin();

		if (gameState_ != GameStates::STATE_NULL)
		{
			switchGameState();
		}

		float newTicks = fps_limiter.getElapsedTime();
		float frameTime = newTicks - previousTicks;
		previousTicks = newTicks;

		float totalDeltaTime = frameTime / DESIRED_FRAMETIME;

		processInput();

		//Update the audio manager
		system_.audioManager->update();

		//Update using delta time.
		int i = 0;
		while (totalDeltaTime > 0.0f && 
			i < MAX_PHYSICS_STEPS &&
			gameState_ == GameStates::STATE_NULL
			&& window_.hasFocus())
		{

			float deltaTime = std::min(totalDeltaTime, MAX_DELTA_TIME);
			currentState_->update(deltaTime);
			totalDeltaTime -= deltaTime;
			i++;
		}

		window_.clear(sf::Color::Black);

		//Update the view
		window_.setView(cameraView_);

		//Draw the current state
		currentState_->draw();

		//Draw cursor
		drawCursor();

		window_.display();

		//Fps limiter
		fps_ = fps_limiter.end();
	}
	window_.clear();
	window_.close();
}

void MainGame::drawCursor()
{
	if (cursor_)
	{
		spriteBatch_.begin();

		spriteBatch_.draw(cursorTexture_,
			sf::FloatRect(
				inputManager_.getMouseCoords().x,
				inputManager_.getMouseCoords().y,
				32,
				32),
			sf::IntRect(
				0,
				0,
				cursorTexture_->getSize().x,
				cursorTexture_->getSize().y),
			sf::Color(255,255,255,255));
		spriteBatch_.end();

		window_.draw(spriteBatch_);
	}
}


