#include "AbilityFirework.h"

#include <random>

AbilityFirework::AbilityFirework()
{
}

AbilityFirework::AbilityFirework(System system, glm::vec2 pos) :
	system_(system),
	pos_(pos),
	speed_(15)
{
	
	texture_ = system_.resourceManager->getTexture("resources/textures/rocket.png");
	uvRect_ = sf::IntRect(0, 0, texture_->getSize().x, texture_->getSize().y);

	test_ = system_.resourceManager->getTexture("resources/textures/bombCir.png");
	uvRect2_  = sf::IntRect(0, 0, test_->getSize().x, test_->getSize().y);

	color_ = sf::Color(255, 255, 255, 255);

	explosionSize_ = glm::vec2(system_.cameraView->getSize().x / 4);
	explosionRadious_ = explosionSize_.x / 2;
	explode_ = false;
	active_ = true;

	movementHelper_ = -60;
	direction_ = glm::vec2(1, 0);

	yOrgin = pos_.y;
	ySpeed_ = 0;

	animatedTexture_ = PotatoEngine::AnimatedTexture(
		system_.animationManager->getAnimation("Firework"));


	std::mt19937 rng;
	rng.seed(std::random_device()());
	std::uniform_int_distribution<std::mt19937::result_type> dist3(0, 1);
	goUp_ = dist3(rng);
}

AbilityFirework::~AbilityFirework()
{
}

bool AbilityFirework::update(float deltaTime)
{
	animatedTexture_.update(deltaTime);
	pos_.x += speed_ * deltaTime;
	pos_.y += ySpeed_ * deltaTime;

	disorientation_ = 100;
	if (goUp_)
	{
		movementHelper_ += deltaTime * 1;

		if (movementHelper_ <= 10)
		{
			if (ySpeed_ <= 1)
			{
				ySpeed_ += deltaTime * 0.1;
			}
		}
		else if (pos_.y <= yOrgin + disorientation_)
		{
			ySpeed_ = 1;
		}
		else
		{
			goUp_ = !goUp_;
		}
	}
	else
	{
		movementHelper_ -= deltaTime * 1;

		if (movementHelper_ >= -10)
		{
			if (ySpeed_ >= -1)
			{
				ySpeed_ -= deltaTime * 0.1;
			}
		}
		else if (pos_.y >= yOrgin - disorientation_)
		{
			ySpeed_ = -1;
		}
		else
		{
			goUp_ = !goUp_;
		}
	}
	
	if (pos_.x > system_.window->mapPixelToCoords(sf::Vector2i(system_.window->getSize())).x)
	{
		explode_ = true;
	}
	return explode_;
}

void AbilityFirework::draw(PotatoEngine::SpriteBatch * spriteBatch)
{
	//Circle around rocket???
	
	spriteBatch->draw(test_,
		sf::FloatRect(pos_.x - explosionRadious_,
			pos_.y - explosionRadious_,
			explosionSize_.x,
			explosionSize_.y),
		uvRect2_,
		sf::Color(255,255,255,127.5));
		
	float tmp = system_.cameraView->getSize().x / 16;
	animatedTexture_.draw(spriteBatch,
		glm::vec4(
			pos_.x - tmp/2 - tmp/4,
			pos_.y - tmp/2,
			tmp,
			tmp));
	/*
	spriteBatch->draw(texture_,
		sf::FloatRect(pos_.x - 12.5f, pos_.y - 12.5f, 25, 25),
		uvRect_,
		color_);*/
}

void AbilityFirework::detonate()
{
	explode_ = true;
}

glm::vec4 AbilityFirework::getHitBox() const
{
	float tmp = system_.cameraView->getSize().x / 16;
	return glm::vec4(
		pos_.x - tmp / 4 ,
		pos_.y - tmp / 8,
		tmp/2,
		tmp/4
		);
}

glm::vec3 AbilityFirework::getExplosionCir() const
{
	return glm::vec3(pos_.x, pos_.y, explosionRadious_);
}

