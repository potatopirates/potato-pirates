#include "VictoryState.h"
#include <PotatoEngine\Converter.h>

VictoryState::VictoryState(System system, Score* score) :
	system_(system),
	score_(score),
	blackScreen_(255)
{
	//Music
	if (system_.audioManager->getLatestMusicPlayed() != "resources/music/Feelin Good radio.ogg")
	{
		system_.audioManager->playMusic("resources/music/Feelin Good radio.ogg", true);
	}

	// Initialize the state buttons.
	initializeButtons();

	// Set up the background.
	backgroundImage_ = PotatoEngine::GUIImage(system_.resourceManager, glm::vec2(0),
		glm::vec2(system_.cameraView->getSize().x, system_.cameraView->getSize().y),
		"resources/textures/victoryBG.png", glm::vec4(0.0f));


	for (int i = 0; i < 6; i++)
	{
		if (i == 0)
		{
			label_ = "Kills: ";
		}
		else if (i == 1)
		{
			label_ = "Evasions: ";
		}
		else if (i == 2)
		{
			label_ = "Pick-Ups: ";
		}
		else if (i == 3)
		{
			label_ = "Life: ";
		}
		else if (i == 4)
		{
			label_ = "Ammo: ";
		}
		else if (i == 5)
		{
			label_ = "Total Score: ";
		}
		scoreLabels_.push_back(PotatoEngine::GUILabel(system_.resourceManager,
			glm::vec2(
				system_.window->mapPixelToCoords(sf::Vector2i(system_.window->getSize().x / 4, 0)).x,
					system_.window->mapPixelToCoords(sf::Vector2i(0, system_.window->getSize().y / 9 * i)).y),
				label_,
				"resources/fonts/StardosStencil-Bold.ttf",
				system_.window->mapCoordsToPixel(sf::Vector2f(0, 1080)).y / 18
				));
	}
	for (int i = 0; i < 6; i++)
	{
		victoryCounts_.push_back(PotatoEngine::GUILabel(system_.resourceManager,
			glm::vec2(
				system_.window->mapPixelToCoords(sf::Vector2i(system_.window->getSize().x / 4, 0)).x,
				system_.window->mapPixelToCoords(sf::Vector2i(0, system_.window->getSize().y / 9 * i)).y),
			"",
			"resources/fonts/StardosStencil-Bold.ttf",
			system_.window->mapCoordsToPixel(sf::Vector2f(0, 1080)).y / 18
				));
	}
	system_.resetView();

	*system_.cursor = true;

	//Check new highscore
	checkHighScore();

	//Awards!
	int currentLvl = 0;
	int money = 0;
	int currentStatus = 0;

	system_.playerSaveData->get("currentLvl", currentLvl);
	system_.playerSaveData->get("Money", money);
	system_.playerSaveData->get("lvlStatus", currentStatus);


	switch (currentLvl)
	{
	case 1: money += 100; break;
	case 2: money += 200; break;
	case 3: money += 300; break;
	case 4: money += 400; break;
	}

	if (currentStatus < currentLvl)
	{
		currentStatus = currentLvl;
	}

	system_.playerSaveData->set("lvlStatus", currentStatus);
	system_.playerSaveData->set("Money", money);
	system_.playerSaveData->saveToFile();

}


VictoryState::~VictoryState()
{
}

void VictoryState::update(float deltaTime)
{
	if (blackScreen_ > 0)
	{
		blackScreen_ -= 4 * deltaTime;
	}

	for (unsigned int i = 0; i < GUIButtons_.size(); i++)
	{
		GUIButtons_[i].update(system_.inputManager, deltaTime);
	}
}

void VictoryState::draw()
{
	spriteBatch_.begin();

	// Draw the menu background.
	backgroundImage_.draw(&spriteBatch_);

	// Draw all the GUI objects.
	for (unsigned int i = 0; i < GUIButtons_.size(); i++)
	{
		GUIButtons_[i].draw(&spriteBatch_);
	}

	if (blackScreen_ > 0)
	{
		spriteBatch_.draw(
			system_.resourceManager->getTexture("resources/textures/hitboxArea.png"),
			sf::FloatRect(
				0,
				0,
				system_.cameraView->getSize().x,
				system_.cameraView->getSize().y),
			sf::IntRect(0, 0, 0, 0),
			sf::Color(0, 0, 0, static_cast<sf::Uint8>(blackScreen_)));
	}


	spriteBatch_.end();
	system_.window->draw(spriteBatch_);
	for (unsigned int i = 0; i < GUIButtons_.size(); i++)
	{
		GUIButtons_[i].drawText(system_.window);
	}

	drawText(system_.window, 0);

}

void VictoryState::drawText(sf::RenderWindow * window, float alphaRemove)
{
	for (int i = 0; i < scoreLabels_.size(); i++)
	{
		if (i == 5)
		{
			scoreLabels_[i].setPos(glm::vec2(
				system_.window->mapPixelToCoords(sf::Vector2i(system_.window->getSize().x / 14, 0)).x,
				system_.window->mapPixelToCoords(sf::Vector2i(0, system_.window->getSize().y / 18 * (i + 6))).y));
			scoreLabels_[i].setColor((sf::Color(0, 0, 0, 255 - alphaRemove)));
			scoreLabels_[i].drawText(window);

		}
		else
		{
			scoreLabels_[i].setPos(glm::vec2(
				system_.window->mapPixelToCoords(sf::Vector2i(system_.window->getSize().x / 14, 0)).x,
				system_.window->mapPixelToCoords(sf::Vector2i(0, system_.window->getSize().y / 18 * (i + 5))).y));
			scoreLabels_[i].setColor((sf::Color(0, 0, 0, 255 - alphaRemove)));
			scoreLabels_[i].drawText(window);
		}
	}

	for (int i = 0; i < victoryCounts_.size(); i++)
	{
		if (i == 5)
		{
			victoryCounts_[i].setText(std::to_string(score_->getVictoryScore()));
			victoryCounts_[i].setPos(glm::vec2(
				system_.window->mapPixelToCoords(sf::Vector2i(system_.window->getSize().x / 3, 0)).x,
				system_.window->mapPixelToCoords(sf::Vector2i(0, system_.window->getSize().y / 18 * (i + 6))).y));
			victoryCounts_[i].setColor((sf::Color(0, 0, 0, 255 - alphaRemove)));
			victoryCounts_[i].drawText(window);

		}
		else if (i == 0)
		{
			victoryCounts_[i].setText(std::to_string(score_->kills_));
			victoryCounts_[i].setPos(glm::vec2(
				system_.window->mapPixelToCoords(sf::Vector2i(system_.window->getSize().x / 3, 0)).x,
				system_.window->mapPixelToCoords(sf::Vector2i(0, system_.window->getSize().y / 18 * (i + 5))).y));
			victoryCounts_[i].setColor((sf::Color(0, 0, 0, 255 - alphaRemove)));
			victoryCounts_[i].drawText(window);
		}
		else if (i == 1)
		{
			victoryCounts_[i].setText(std::to_string(score_->evasions_));
			victoryCounts_[i].setPos(glm::vec2(
				system_.window->mapPixelToCoords(sf::Vector2i(system_.window->getSize().x / 3, 0)).x,
				system_.window->mapPixelToCoords(sf::Vector2i(0, system_.window->getSize().y / 18 * (i + 5))).y));
			victoryCounts_[i].setColor((sf::Color(0, 0, 0, 255 - alphaRemove)));
			victoryCounts_[i].drawText(window);
		}
		else if (i == 2)
		{
			victoryCounts_[i].setText(std::to_string(score_->pickUps_));
			victoryCounts_[i].setPos(glm::vec2(
				system_.window->mapPixelToCoords(sf::Vector2i(system_.window->getSize().x / 3, 0)).x,
				system_.window->mapPixelToCoords(sf::Vector2i(0, system_.window->getSize().y / 18 * (i + 5))).y));
			victoryCounts_[i].setColor((sf::Color(0, 0, 0, 255 - alphaRemove)));
			victoryCounts_[i].drawText(window);
		}
		else if (i == 3)
		{
			victoryCounts_[i].setText(std::to_string(score_->life_));
			victoryCounts_[i].setPos(glm::vec2(
				system_.window->mapPixelToCoords(sf::Vector2i(system_.window->getSize().x / 3, 0)).x,
				system_.window->mapPixelToCoords(sf::Vector2i(0, system_.window->getSize().y / 18 * (i + 5))).y));
			victoryCounts_[i].setColor((sf::Color(0, 0, 0, 255 - alphaRemove)));
			victoryCounts_[i].drawText(window);
		}
		else if (i == 4)
		{
			victoryCounts_[i].setText(std::to_string(score_->ammo_));
			victoryCounts_[i].setPos(glm::vec2(
				system_.window->mapPixelToCoords(sf::Vector2i(system_.window->getSize().x / 3, 0)).x,
				system_.window->mapPixelToCoords(sf::Vector2i(0, system_.window->getSize().y / 18 * (i + 5))).y));
			victoryCounts_[i].setColor((sf::Color(0, 0, 0, 255 - alphaRemove)));
			victoryCounts_[i].drawText(window);
		}

	}

}

void VictoryState::initializeButtons()
{
	float xHelper = static_cast<float>(system_.cameraView->getSize().x / 5.0f);
	float yHelper = static_cast<float>(system_.cameraView->getSize().y / 10.0f);

	float xSizeHelper = static_cast<float>(
		system_.cameraView->getSize().x / 2.0f - xHelper / 2);


	glm::vec2 margin(
		system_.cameraView->getSize().x / 2 - xHelper - 50,
		150);
	std::string imagePath = "resources/textures/Mainbutton_spritesheet.png";
	std::string fontPath = "resources/fonts/StardosStencil-Regular.ttf";
	unsigned int fontSize = 50;

	GUIButtons_.push_back(PotatoEngine::GUIButton(
		system_.resourceManager,
		glm::vec2(
			xSizeHelper,
			yHelper * 4.8) + margin,
		glm::vec2(xHelper + xHelper / 2, yHelper),
		imagePath,
		"Restart",
		fontSize,
		fontPath,
		[=](PotatoEngine::GUIButton& object, float deltaTime)
	{
		*system_.gameState = GameStates::STATE_BATTLESTATE;
		system_.audioManager->playSound("resources/sfx/button_click.ogg");
	}));

	GUIButtons_.push_back(PotatoEngine::GUIButton(
		system_.resourceManager,
		glm::vec2(
			xSizeHelper,
			yHelper * 6.0) + margin,
		glm::vec2(xHelper + xHelper / 2, yHelper),
		imagePath,
		"Mission Select",
		fontSize,
		fontPath,
		[=](PotatoEngine::GUIButton& object, float deltaTime)
	{
		*system_.gameState = GameStates::STATE_MISSIONSELECT;
		system_.audioManager->playSound("resources/sfx/button_click.ogg");
	}));

	GUIButtons_.push_back(PotatoEngine::GUIButton(
		system_.resourceManager,
		glm::vec2(
			xSizeHelper,
			yHelper * 7.2) + margin,
		glm::vec2(xHelper + xHelper / 2, yHelper),
		imagePath,
		"Exit to Menu",
		fontSize,
		fontPath,
		[=](PotatoEngine::GUIButton& object, float deltaTime)
	{
		*system_.gameState = GameStates::STATE_MENU;
		system_.audioManager->playSound("resources/sfx/button_click.ogg");
	}));
}



void VictoryState::checkHighScore()
{
	//read into highScoreList_
	std::string scorePath = *system_.gameDictionary;
	scorePath += "/highscore.potato";

	PotatoEngine::SettingsParser scoreParser_;
	scoreParser_.loadFromFile(scorePath);

	std::string name;
	int score;
	std::string rs;
	std::string rn;

	std::string playerName = system_.playerSaveData->getFileName();

	playerName = PotatoEngine::base_name(playerName);
	playerName = PotatoEngine::remove_extension(playerName);

	int playerScore = score_->getVictoryScore();

	int i;
	for (i = 10; i > 0; i--)
	{
		rn = "Rank_";
		rn += static_cast<std::ostringstream*>
			(&(std::ostringstream() << i))->str();
		rs = rn;
		rn += "_Name";
		rs += "_Score";
		scoreParser_.get(rn, name);
		scoreParser_.get(rs, score);

		if (playerScore <= score)
		{
			break;
		}
	}


	if (i < 10)
	{
		for (int j = 10; j > (i+1); j--)
		{
			//Get
			rn = "Rank_";
			rn += static_cast<std::ostringstream*>
				(&(std::ostringstream() << j-1))->str();
			rs = rn;
			rn += "_Name";
			rs += "_Score";
			scoreParser_.get(rn, name);
			scoreParser_.get(rs, score);

			//Replace
			rn = "Rank_";
			rn += static_cast<std::ostringstream*>
				(&(std::ostringstream() << j))->str();
			rs = rn;
			rn += "_Name";
			rs += "_Score";
			scoreParser_.set(rn, name);
			scoreParser_.set(rs, score);
		}

		//Replace with player

		std::string rs;
		std::string rn = "Rank_";
		rn += static_cast<std::ostringstream*>
			(&(std::ostringstream() << i+1))->str();
		rs = rn;
		rn += "_Name";
		rs += "_Score";

		scoreParser_.set(rn, playerName);
		scoreParser_.set(rs, playerScore);

		scoreParser_.saveToFile();
	}
}
