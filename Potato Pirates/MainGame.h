#ifndef POTATOPIRATES_MAINGAME_H
#define POTATOPIRATES_MAINGAME_H
#include "System.h"
#include "Score.h"

class MainGame
{
public:
	MainGame();
	~MainGame();
	
	void run();
private:

	void init_systems();
	void loadConfig();
	void createDefaultConfig();
	void createDefaultHighscore();

	void switchGameState();
	void processInput();

	void main_loop();

	void drawCursor();


	//Member variables
	System system_;										///Keeps pointer to all system variables. (tex m_window)
	GameStates gameState_;								///Gamestate enum, keeps gamestate info
	sf::RenderWindow window_;							///The game window
	sf::View cameraView_;								///Camera view... :)
	PotatoEngine::GLSLProgram textureProgram_;			///The shader program
	PotatoEngine::InputManager inputManager_;			///Handles input
	PotatoEngine::ResourceManager resourceManager_;		///Resource manager.
	PotatoEngine::AudioManager audioManager_;			///Manages sound effects and music.
	PotatoEngine::CollisionManager collisionManager_;	///Handles all collision checks
	PotatoEngine::AnimationManager animationManager_;
	GameState *currentState_ = nullptr;					///Gamesate structor

	PotatoEngine::SettingsParser settingsParaser_;
	PotatoEngine::SettingsParser playerSaveData_;
	float fps_;										///Fps of the game.

	std::string gameDictionary_;

	bool cursor_;
	sf::Texture* cursorTexture_;
	PotatoEngine::SpriteBatch spriteBatch_;
	Score score_;

};

#endif // POTATOPIRATES_MAINGAME_H