#ifndef POTATOPIRATES_STATELOADGAME_H
#define POTATOPIRATES_STATELOADGAME_H

#include "GameState.h"
#include "System.h"

#include "PotatoEngine\SpriteBatch.h"
#include "PotatoEngine\GUI.h"

class StateLoadGame :
	public GameState
{
public:
	//Constructors
	StateLoadGame(System system);
	//Deconstructor
	~StateLoadGame();


	/// <summary>
	/// Updates the state
	/// </summary>
	///<param name="deltaTime">Time used for physics calculating.</param>
	/// <returns></returns>
	void update(float deltaTime);

	/// <summary>Draw the state on the system window</summary>
	void draw();

private:
	void initilizeGUI();

	//Varuables
	System system_;

	PotatoEngine::SpriteBatch spriteBatch_;

	//GUI Elements
	std::vector<PotatoEngine::GUIButton> GUIButtons_;
	PotatoEngine::GUIVerticalSlider slider_;
	float valueHelper_;

	//Background
	PotatoEngine::GUIImage backgroundImage_;
};

#endif // POTATOPIRATES_STATELOADGAME_H