#include "Score.h"
#include <iostream>


Score::Score() :
	killScore_(0),
	evasionScore_(0),
	pickUpScore_(0),
	totalScore_(0),
	kills_(0),
	evasions_(0),
	pickUps_(0),
	ammoScore_(0),
	ammo_(0),
	lifeScore_(0),
	life_(0)
{
}


Score::~Score()
{
}
/*
void Score::setScore(int * evasionScore, int * killScore, int * pickUpScore)
{
	evasionScore_ = evasionScore;
	killScore_ = killScore;
	pickUpScore_ = pickUpScore;

	totalScore_ = *evasionScore_ + *killScore_ + *pickUpScore_;
}*/

void Score::update(float deltaTime)
{
	evasionScore_ = evasions_ * 997;
	killScore_ = kills_ * 1499;
	pickUpScore_ = pickUps_ * 1048;
	
	totalScore_ = evasionScore_ + killScore_ + pickUpScore_;
	
	lifeScore_ = life_ * 1980;

	ammoScore_ = (1 + (static_cast<float>(ammo_) / 100.0f));


	endScore_ = (totalScore_ + lifeScore_) * ammoScore_;
	
}

void Score::resetScore()
{
	evasions_ = 0;
	kills_ = 0;
	pickUps_ = 0;
	life_ = 0;
	ammo_ = 0;
}

int Score::getScore()
{
	return totalScore_;
}

int Score::getVictoryScore()
{

	return endScore_;
}
