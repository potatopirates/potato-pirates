#ifndef POTATOENGINE_TEXTURECACHE_H
#define POTATOENGINE_TEXTURECACHE_H
#include <SFML/Graphics.hpp>

namespace PotatoEngine
{
	class TextureCache
	{
	public:
		//Constructors
		TextureCache();
		//Deconstructor
		~TextureCache();

		/// <summary>
		/// Returns a pointer to a texture stored in a map based on the texture file path.
		/// If texture is not inside the map it will laod it from the file path.
		/// </summary>
		///<param name="texturePath">The file location of the texture</param>
		/// <returns></returns>
		sf::Texture* getTexture(std::string texturePath);

	private:
		std::map<std::string, sf::Texture> textureMap_;			///Map of all cached textures.
	};
}
#endif // POTATOENGINE_TEXTURECACHE_H