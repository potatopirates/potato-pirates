#ifndef POTATOPIRATES_ABILITYPOTATOCLOUD_H
#define POTATOPIRATES_ABILITYPOTATOCLOUD_H

#include "System.h"

class AbilityPotatoCloud
{
public:
	//Constructors
	AbilityPotatoCloud();
	AbilityPotatoCloud(System system);
	//Deconstructors
	~AbilityPotatoCloud();

	void update(float deltaTime);
	bool isActive();
	void setDuration(float duration);
	void setActive();

private:
	System system_;
	float duration_;
	bool isActive_;
};

#endif //POTATOPIRATES_ABILITYPOTATOCLOUD_H