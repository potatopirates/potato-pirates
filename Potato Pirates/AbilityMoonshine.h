#ifndef POTATOPIRATES_ABILITYMOONSHINE_H
#define POTATOPIRATES_ABILITYMOONSHINE_H

#include "System.h"

class AbilityMoonshine
{
public:
	//Constructors
	AbilityMoonshine();
	AbilityMoonshine(System system);
	//Deconstructors
	~AbilityMoonshine();

	///<summary>Updates the duration of the ability</summary>
	void update(float deltaTime);

	///<summary>Draws the effect of the ability on screen</summary>
	void draw(PotatoEngine::SpriteBatch* spriteBatch);

	///<summary>Returns whether the ability is active or not</summary>
	bool isActive();
	//Setters
	///<summary>Sets the duration of the ability</summary>
	void setDuration(float duration);

	///<summary>Sets the ability active</summary>
	void setActive();
private:
	System system_;

	PotatoEngine::AnimatedTexture moonshineEffect_;
	sf::Color effectColor_;
	bool updateAnimation_;
	float duration_;
	bool isActive_;
};

#endif //POTATOPIRATES_ABILITYMOONSHINE_H