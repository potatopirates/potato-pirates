#ifndef POTATOPIRATES_STATECREDITS_H
#define POTATOPIRATES_STATECREDITS_H
#include "GameState.h"
#include "System.h"
#include "PotatoEngine\GUI.h"

class StateCredits : public GameState
{
public:
	//Constructors
	StateCredits();
	StateCredits(System system);
	//Deconstructors
	~StateCredits();

	/// <summary>
	/// Updates the state
	/// </summary>
	///<param name="deltaTime">Time used for physics calculating.</param>
	/// <returns></returns>
	void update(float deltaTime);

	/// <summary>Draw the state on the system window</summary>
	void draw();
private:
	void initilizeButtons();
	//Variables
	System system_;

	PotatoEngine::SpriteBatch spriteBatch_;
	std::vector<PotatoEngine::GUIButton> GUIButtons_;
	//Background
	sf::Texture* background_;

	float movingBG_;
	float moveWaiter_;

};

#endif //POTATOPIRATES_STATECREDITS_H