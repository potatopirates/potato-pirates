#include "ResourceManager.h"


namespace PotatoEngine
{
	ResourceManager::ResourceManager()
	{
		/*Empty*/
	}


	ResourceManager::~ResourceManager()
	{
		/*Empty*/
	}

	sf::Texture* ResourceManager::getTexture(std::string texturePath)
	{
		return textureCache_.getTexture(texturePath);
	}

	sf::SoundBuffer* ResourceManager::getSoundBuffer(std::string soundPath)
	{
		return soundBufferCache_.getSoundBuffer(soundPath);
	}
}