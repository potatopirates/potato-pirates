#include "StateLoadGame.h"

#include <windows.h>
#include <string>
#include <vector>

StateLoadGame::StateLoadGame(System system) :
	system_(system)
{
	initilizeGUI();

	//Setup background
	backgroundImage_ = PotatoEngine::GUIImage(system_.resourceManager,
		glm::vec2(0),
		glm::vec2(system_.cameraView->getSize().x, system_.cameraView->getSize().y),
		"resources/textures/loadgameBG.png", glm::vec4(0.0f));

	float xHelper = static_cast<float>(system_.cameraView->getSize().x / 5.0f);
	float yHelper = static_cast<float>(system_.cameraView->getSize().y / 20.0f);

	float xSizeHelper = static_cast<float>(
		system_.cameraView->getSize().x * 0.63);

	slider_ = PotatoEngine::GUIVerticalSlider(system_.resourceManager,
		glm::vec2(
			system_.cameraView->getSize().x * 0.96,
			system_.cameraView->getSize().y * 0.05),
		glm::vec2(
			system_.cameraView->getSize().x * 0.02,
			system_.cameraView->getSize().y * 0.9),
		"resources/textures/verticalSlider_spritesheet.png",
		0,
		GUIButtons_.size() - 1,
		0);
	valueHelper_ = 0;
}


StateLoadGame::~StateLoadGame()
{
	/*Empty*/
}

void StateLoadGame::update(float deltaTime)
{
	for (unsigned int i = 0; i < GUIButtons_.size(); i++)
	{
		GUIButtons_[i].update(system_.inputManager, deltaTime);
	//	GUIButtons_[i].setPos(GUIButtons_[i].getPos() + glm::vec2(0,-1));
	}

	slider_.update(system_.inputManager, deltaTime);

	if (valueHelper_ != slider_.getValue())
	{
		valueHelper_ = slider_.getValue();

		float yHelper = static_cast<float>(system_.cameraView->getSize().y / 10.0f);

		for (unsigned int i = 1; i < GUIButtons_.size()-1; i++)
		{
			GUIButtons_[i].update(system_.inputManager, deltaTime);

			GUIButtons_[i].setPos(glm::vec2(
				system_.cameraView->getSize().x * 0.585,
				50 + i * 10 + yHelper * i - (valueHelper_ * yHelper + 100)));
		}
	}

}

void StateLoadGame::draw()
{
	spriteBatch_.begin();

	//Draw the menu background
	backgroundImage_.draw(&spriteBatch_);

	//Draw all the gui menu objects.
	for (unsigned int i = 0; i < GUIButtons_.size(); i++)
	{
		GUIButtons_[i].draw(&spriteBatch_);
	}

	slider_.draw(&spriteBatch_);
	//Draw on screen
	spriteBatch_.end();
	system_.window->draw(spriteBatch_);

	//Draw text
	for (unsigned int i = 0; i < GUIButtons_.size(); i++)
	{
		GUIButtons_[i].drawText(system_.window);
	}
}


void StateLoadGame::initilizeGUI()
{
	float xHelper = static_cast<float>(system_.cameraView->getSize().x / 5.0f);
	float yHelper = static_cast<float>(system_.cameraView->getSize().y / 10.0f);

	float xSizeHelper = static_cast<float>(
		system_.cameraView->getSize().x / 2.0f - xHelper / 2);


	std::string imagePath = "resources/textures/Mainbutton_spritesheet.png";
	std::string fontPath = "resources/fonts/StardosStencil-Regular.ttf";
	unsigned int fontSize = 50;

	//Back
	GUIButtons_.push_back(PotatoEngine::GUIButton(
		system_.resourceManager,
		glm::vec2(
			system_.cameraView->getSize().x * 0.05,
			system_.cameraView->getSize().y * 0.83),
		glm::vec2(xHelper + xHelper / 2, yHelper),
		imagePath,
		"Back",
		fontSize,
		fontPath,
		[=](PotatoEngine::GUIButton& object, float deltaTime)
	{
		*system_.gameState = GameStates::STATE_MENU;
		system_.audioManager->playSound("resources/sfx/button_click.ogg");
	}));



	std::vector<std::string> files;
	

	HANDLE hFind;
	WIN32_FIND_DATA data;

	std::string tmp = *system_.gameDictionary;
	tmp += "\\saves\\*.potato";
	hFind = FindFirstFile(tmp.c_str(), &data);
	if (hFind != INVALID_HANDLE_VALUE) {
		do {
			printf("%s\n", data.cFileName);

			std::string tmp = data.cFileName;
			size_t lastindex = tmp.find_last_of(".");
			std::string rawname = tmp.substr(0, lastindex);

			files.emplace_back(rawname);

		} while (FindNextFile(hFind, &data));
		FindClose(hFind);
	}


	int count = files.size();

	for (int i = 0; i < files.size(); i++)
	{
		GUIButtons_.push_back(PotatoEngine::GUIButton(
			system_.resourceManager,
			glm::vec2(
				system_.cameraView->getSize().x * 0.585,
				50 + i * 10 + yHelper * i),
			glm::vec2(
				xHelper + xHelper / 2,
				yHelper),
			imagePath,
			files[i],
			fontSize,
			fontPath,
			[=](PotatoEngine::GUIButton& object, float deltaTime)
		{

			std::string loadPath = *system_.gameDictionary;
			loadPath += "//saves//";
			loadPath += files[i];
			loadPath += ".potato";

			system_.playerSaveData->loadFromFile(loadPath);

			//Update continue
			std::string settingsPath = *system_.gameDictionary;
			settingsPath += "/settings.txt";

			PotatoEngine::SettingsParser tmp;
			tmp.loadFromFile(settingsPath);
			tmp.set("latestGame", files[i]);
			tmp.saveToFile();

			system_.settingsParser->set("latestGame", files[i]);

			*system_.gameState = GameStates::STATE_MISSIONSELECT;
			system_.audioManager->playSound("resources/sfx/button_click.ogg");

		}));
	}

}
