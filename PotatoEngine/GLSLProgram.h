#ifndef POTATOENGINE_GLSLPROGRAM_H
#define POTATOENGINE_GLSLPROGRAM_H
#include <string>
#include <SFML/Graphics.hpp>

namespace PotatoEngine
{
	class GLSLProgram
	{
		
	public:
		//Constructors
		GLSLProgram();
		//Deconstructors
		~GLSLProgram();

		void compileShaders(													///Compile our shaders
			const std::string &vertexShaderFilePath,
			const std::string &fragmentShaderFilepath);

		void use();
		void unuse();


		sf::Shader* getShader();

	private:
		sf::Shader shader_;
	};
}
#endif // POTATOENGINE_GLSLPROGRAM_H