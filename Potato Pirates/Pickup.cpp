#include "Pickup.h"

#include <PotatoEngine\Converter.h>

Pickup::Pickup()
{
	/*Empty*/
}

Pickup::Pickup(System system, 
	glm::vec2 pos, 
	glm::vec2 size,
	PickupType pickupType) 
	:
	system_(system),
	dest_(glm::vec4(pos.x, pos.y, size.x, size.y)),
	pickupType_(pickupType),
	color_(255,255,255,255)
{
	//Set up texture
	switch (pickupType_)
	{
	case CARGO: 
		animatedPickup_ = PotatoEngine::AnimatedTexture(system_.animationManager->getAnimation("CargoBalloon"));
		break;
	
	case FIREWORK: 		
		animatedPickup_ = PotatoEngine::AnimatedTexture(system_.animationManager->getAnimation("FWBalloon"));
		break;

	case MOONSHINE: 
		animatedPickup_ = PotatoEngine::AnimatedTexture(system_.animationManager->getAnimation("MoonshineBalloon"));
		break;

	case POTATOCLOUD:
		animatedPickup_ = PotatoEngine::AnimatedTexture(system_.animationManager->getAnimation("PotatoCloudBalloon"));
		break;
	}

}


Pickup::~Pickup()
{
	/*Empty*/
}

bool Pickup::leftWindow()
{
	if ((dest_.x + dest_.z) < system_.window->mapPixelToCoords(sf::Vector2i(0,0)).x)
	{
		return true;
	}
	return false;
}


void Pickup::draw(PotatoEngine::SpriteBatch * spriteBatch)
{
	animatedPickup_.draw(spriteBatch,
		dest_);
}

void Pickup::update(float deltaTime)
{
	animatedPickup_.update(deltaTime);
}

PickupType Pickup::getType() const
{
	return pickupType_;
}

glm::vec4 Pickup::getHitBox() const
{
	return dest_;
}
