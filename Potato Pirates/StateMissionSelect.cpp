#include "StateMissionSelect.h"
#include <sstream> 


StateMissionSelect::StateMissionSelect(System system) :
	system_(system)
{

	//Music
	if (system_.audioManager->getLatestMusicPlayed() != "resources/music/Off to Osaka.ogg")
	{
		system_.audioManager->playMusic("resources/music/Off to Osaka.ogg", true);
	}


	system_.resetView();

	//Initilize the menu buttons
	initilizeMap();
	initilizeButtons();
	initilizeShop();
	
	//Set up background
	backgroundImage_ = PotatoEngine::GUIImage(system_.resourceManager,
		glm::vec2(0),
		glm::vec2(system_.cameraView->getSize().x, system_.cameraView->getSize().y),
		"resources/textures/missionSelectBG.png", glm::vec4(0.0f));
	
	hudImage_ = PotatoEngine::GUIImage(system_.resourceManager,
		glm::vec2(0),
		glm::vec2(system_.cameraView->getSize().x, system_.cameraView->getSize().y),
		"resources/textures/missionSelectHUD.png", glm::vec4(0.0f));


	shopBackground_ = PotatoEngine::GUIImage(system_.resourceManager,
		glm::vec2(
			system_.cameraView->getSize().x * 0.75,
			0),
		glm::vec2(
			system_.cameraView->getSize().x * 0.25,
			system_.cameraView->getSize().y),
		"resources/textures/shop_background.png", glm::vec4(0.0f));


	*system_.cursor = true;
}


StateMissionSelect::~StateMissionSelect()
{
}

void StateMissionSelect::update(float deltaTime)
{
	for (unsigned int i = 0; i < GUIButtons_.size(); i++)
	{
		GUIButtons_[i].update(system_.inputManager, deltaTime);
	}
}

void StateMissionSelect::draw()
{
	spriteBatch_.begin();

	//Draw the menu background
	backgroundImage_.draw(&spriteBatch_);

	shopBackground_.draw(&spriteBatch_);

	//Draw all the gui menu objects.
	for (unsigned int i = 0; i < GUIButtons_.size(); i++)
	{
		GUIButtons_[i].draw(&spriteBatch_);
	}

	for (unsigned int i = 0; i < upgradeStars_.size(); i++)
	{
		upgradeStars_[i].draw(&spriteBatch_);
	}

	hudImage_.draw(&spriteBatch_);

	spriteBatch_.end();

	system_.window->draw(spriteBatch_);

	for (unsigned int i = 0; i < GUIButtons_.size(); i++)
	{
		GUIButtons_[i].drawText(system_.window);
	}

	moneyLabel_.drawText(system_.window);
}

void StateMissionSelect::initilizeMap()
{
	float xSizeHelper = static_cast<float>((system_.cameraView->getSize().x * 0.375));
	float ySizeHelper = static_cast<float>(system_.cameraView->getSize().y / 2);

	float lvlStatus;
	system_.playerSaveData->get("lvlStatus", lvlStatus);

	GUIButtons_.push_back(PotatoEngine::GUIButton(
		system_.resourceManager,
		glm::vec2(0, 0),
		glm::vec2(
			xSizeHelper,
			ySizeHelper),
		"resources/textures/missionSelectScreen_lvl1.png",
		"",
		150,
		"resources/fonts/game_over.ttf",
		[=](PotatoEngine::GUIButton& object, float deltaTime)
	{
		system_.playerSaveData->set("currentLvl",1);
		*system_.gameState = GameStates::STATE_BATTLESTATE;
		system_.audioManager->playSound("resources/sfx/button_click.ogg");
	}));

	GUIButtons_.push_back(PotatoEngine::GUIButton(
		system_.resourceManager,
		glm::vec2(xSizeHelper, 0),
		glm::vec2(xSizeHelper, ySizeHelper),
		"resources/textures/missionSelectScreen_lvl2.png",
		"",
		150,
		"resources/fonts/game_over.ttf",
		[=](PotatoEngine::GUIButton& object, float deltaTime)
	{
		system_.playerSaveData->set("currentLvl", 2);
		*system_.gameState = GameStates::STATE_BATTLESTATE;
		system_.audioManager->playSound("resources/sfx/button_click.ogg");
	},
		(lvlStatus >= 1)
		));

	GUIButtons_.push_back(PotatoEngine::GUIButton(
		system_.resourceManager,
		glm::vec2(0, ySizeHelper),
		glm::vec2(xSizeHelper, ySizeHelper),
		"resources/textures/missionSelectScreen_lvl3.png",
		"",
		150,
		"resources/fonts/game_over.ttf",
		[=](PotatoEngine::GUIButton& object, float deltaTime)
	{
		system_.playerSaveData->set("currentLvl", 3);
		*system_.gameState = GameStates::STATE_BATTLESTATE;
		system_.audioManager->playSound("resources/sfx/button_click.ogg");
	},
		(lvlStatus >= 2)
		));

	GUIButtons_.push_back(PotatoEngine::GUIButton(
		system_.resourceManager,
		glm::vec2(xSizeHelper, ySizeHelper),
		glm::vec2(xSizeHelper, ySizeHelper),
		"resources/textures/missionSelectScreen_lvl4.png",
		"",
		150,
		"resources/fonts/game_over.ttf",
		[=](PotatoEngine::GUIButton& object, float deltaTime)
	{
		system_.playerSaveData->set("currentLvl", 4);
		*system_.gameState = GameStates::STATE_BATTLESTATE;
		system_.audioManager->playSound("resources/sfx/button_click.ogg");
	},
		(lvlStatus >= 3)
		));
}

void StateMissionSelect::initilizeButtons()
{
	float xHelper = static_cast<float>(system_.cameraView->getSize().x / 5.0f);
	float yHelper = static_cast<float>(system_.cameraView->getSize().y / 10.0f);

	float xSizeHelper = static_cast<float>(system_.cameraView->getSize().x * 0.2);


	std::string imagePath = "resources/textures/Mainbutton_spritesheet.png";
	std::string fontPath = "resources/fonts/StardosStencil-Regular.ttf";
	unsigned int fontSize = 50;

	GUIButtons_.push_back(PotatoEngine::GUIButton(
		system_.resourceManager,
		glm::vec2(
			system_.cameraView->getSize().x * 0.75,
			system_.cameraView->getSize().y * 0.89),
		glm::vec2(
			system_.cameraView->getSize().x * 0.25,
			yHelper),
		imagePath,
		"Quit to menu",
		fontSize,
		fontPath,
		[=](PotatoEngine::GUIButton& object, float deltaTime)
	{
		*system_.gameState = GameStates::STATE_MENU;
		system_.audioManager->playSound("resources/sfx/button_click.ogg");
	}));


}

void StateMissionSelect::initilizeShop()
{
	float xHelper = static_cast<float>(system_.cameraView->getSize().x / 5.0f);
	float yHelper = static_cast<float>(system_.cameraView->getSize().y / 10.0f);

	float xSizeHelper = static_cast<float>(system_.cameraView->getSize().x * 0.2);


	std::string imagePath = "resources/textures/shopbutton_spritesheet.png";
	std::string fontPath = "resources/fonts/StardosStencil-Regular.ttf";
	unsigned int fontSize = 50;

	sf::Vector2u starSize = system_.resourceManager->getTexture(
		"resources/textures/star_spritesheet.png")->getSize();


	int engine = 0;
	int cargo = 0;
	int armor = 0;
	system_.playerSaveData->get("Engine", engine);
	system_.playerSaveData->get("Cargo", cargo);
	system_.playerSaveData->get("Armor", armor);

	//static_cast<std::string>((100 + engine * 50));


	std::string engineText = "";
	if (engine < 3)
	{
		engineText = static_cast<std::ostringstream*>
			(&(std::ostringstream() << (engine + 1) * 100))->str() + \
			"$ - Buy!";
	}

	std::string armorText = "";
	if (armor < 3)
	{
		armorText = static_cast<std::ostringstream*>
			(&(std::ostringstream() << (armor + 1) * 100))->str() + \
			"$ - Buy!";
	}

	std::string cargoText = "";
	if (cargo < 3)
	{
		cargoText = static_cast<std::ostringstream*>
			(&(std::ostringstream() << (cargo + 1) * 100))->str() + \
			"$ - Buy!";
	}

	int money;
	system_.playerSaveData->get("Money", money);

	//Engine
	GUIButtons_.push_back(PotatoEngine::GUIButton(
		system_.resourceManager,
		glm::vec2(
			system_.cameraView->getSize().x * 0.77,
			system_.cameraView->getSize().y * 0.37),
		glm::vec2(
			system_.cameraView->getSize().x * 0.21,
			yHelper/2),
		imagePath,
		engineText,
		fontSize,
		fontPath,
		[=](PotatoEngine::GUIButton& object, float deltaTime)
	{
		int money;
		int engineLvl;
		system_.playerSaveData->get("Money", money);
		system_.playerSaveData->get("Engine", engineLvl);

		if (money >= (engineLvl + 1) * 100)
		{
			money -= (engineLvl + 1) * 100;
			system_.playerSaveData->set("Money", money);

			engineLvl++;
			system_.playerSaveData->set("Engine", engineLvl);

			system_.audioManager->playSound("resources/sfx/pickup sound.ogg");
			system_.playerSaveData->saveToFile();

			*system_.gameState = GameStates::STATE_MISSIONSELECT;
			system_.audioManager->playSound("resources/sfx/button_click.ogg");
		}
	},
		engine < 3 && ((engine + 1) * 100) <= money));
	
	//Create stars
	//upgradeStars_

	
	glm::vec4 star1 = glm::vec4(0,0,starSize.x / 2,starSize.y);
	glm::vec4 star2 = glm::vec4(0,0,starSize.x / 2,starSize.y);
	glm::vec4 star3 = glm::vec4(0,0,starSize.x / 2,starSize.y);
	switch (engine)
	{
	case 3:		
		star3 = glm::vec4(starSize.x / 2,0,starSize.x / 2,starSize.y);
	case 2:
		star2 = glm::vec4(starSize.x / 2,0,starSize.x / 2,starSize.y);
	case 1:
		star1 = glm::vec4(starSize.x / 2,0,starSize.x / 2,starSize.y);
	}
	
	upgradeStars_.emplace_back(
		system_.resourceManager,
		glm::vec2(
			system_.cameraView->getSize().x * 0.86,
			system_.cameraView->getSize().y * 0.21),
		glm::vec2(
			system_.cameraView->getSize().x * 0.04,
			system_.cameraView->getSize().y * 0.07),
		"resources/textures/star_spritesheet.png",
		star1);

	upgradeStars_.emplace_back(
		system_.resourceManager,
		glm::vec2(
			system_.cameraView->getSize().x * 0.90,
			system_.cameraView->getSize().y * 0.21),
		glm::vec2(
			system_.cameraView->getSize().x * 0.04,
			system_.cameraView->getSize().y * 0.07),
		"resources/textures/star_spritesheet.png",
		star2);

	upgradeStars_.emplace_back(
		system_.resourceManager,
		glm::vec2(
			system_.cameraView->getSize().x * 0.94,
			system_.cameraView->getSize().y * 0.21),
		glm::vec2(
			system_.cameraView->getSize().x * 0.04,
			system_.cameraView->getSize().y * 0.07),
		"resources/textures/star_spritesheet.png",
		star3);

	//Armor
	GUIButtons_.push_back(PotatoEngine::GUIButton(
		system_.resourceManager,
		glm::vec2(
			system_.cameraView->getSize().x * 0.77,
			system_.cameraView->getSize().y * 0.58),
		glm::vec2(
			system_.cameraView->getSize().x * 0.21,
			yHelper / 2),
		imagePath,
		armorText,
		fontSize,
		fontPath,
		[=](PotatoEngine::GUIButton& object, float deltaTime)
	{
		int money;
		int armorLvl;
		system_.playerSaveData->get("Money", money);
		system_.playerSaveData->get("Armor", armorLvl);

		if (money >= (armorLvl + 1) * 100)
		{
			money -= (armorLvl + 1) * 100;
			system_.playerSaveData->set("Money", money);

			armorLvl++;
			system_.playerSaveData->set("Armor", armorLvl);

			system_.audioManager->playSound("resources/sfx/pickup sound.ogg");
			system_.playerSaveData->saveToFile();

			*system_.gameState = GameStates::STATE_MISSIONSELECT;
			system_.audioManager->playSound("resources/sfx/button_click.ogg");
		}
	},
		armor < 3 && ((armor + 1) * 100) <= money));

	//Upgrade star
	star1 = glm::vec4(0, 0, starSize.x / 2, starSize.y);
	star2 = glm::vec4(0, 0, starSize.x / 2, starSize.y);
	star3 = glm::vec4(0, 0, starSize.x / 2, starSize.y);
	switch (armor)
	{
	case 3:
		star3 = glm::vec4(starSize.x / 2, 0, starSize.x / 2, starSize.y);
	case 2:
		star2 = glm::vec4(starSize.x / 2, 0, starSize.x / 2, starSize.y);
	case 1:
		star1 = glm::vec4(starSize.x / 2, 0, starSize.x / 2, starSize.y);
	}

	upgradeStars_.emplace_back(
		system_.resourceManager,
		glm::vec2(
			system_.cameraView->getSize().x * 0.86,
			system_.cameraView->getSize().y * 0.42),
		glm::vec2(
			system_.cameraView->getSize().x * 0.04,
			system_.cameraView->getSize().y * 0.07),
		"resources/textures/star_spritesheet.png",
		star1);

	upgradeStars_.emplace_back(
		system_.resourceManager,
		glm::vec2(
			system_.cameraView->getSize().x * 0.90,
			system_.cameraView->getSize().y * 0.42),
		glm::vec2(
			system_.cameraView->getSize().x * 0.04,
			system_.cameraView->getSize().y * 0.07),
		"resources/textures/star_spritesheet.png",
		star2);

	upgradeStars_.emplace_back(
		system_.resourceManager,
		glm::vec2(
			system_.cameraView->getSize().x * 0.94,
			system_.cameraView->getSize().y * 0.42),
		glm::vec2(
			system_.cameraView->getSize().x * 0.04,
			system_.cameraView->getSize().y * 0.07),
		"resources/textures/star_spritesheet.png",
		star3);



	//Cargo
	GUIButtons_.push_back(PotatoEngine::GUIButton(
		system_.resourceManager,
		glm::vec2(
			system_.cameraView->getSize().x * 0.77,
			system_.cameraView->getSize().y * 0.80),
		glm::vec2(
			system_.cameraView->getSize().x * 0.21,
			yHelper / 2),
		imagePath,
		cargoText,
		fontSize,
		fontPath,
		[=](PotatoEngine::GUIButton& object, float deltaTime)
	{
		int money;
		int cargoLvl;
		system_.playerSaveData->get("Money", money);
		system_.playerSaveData->get("Cargo", cargoLvl);

		if (money >= (cargoLvl + 1) * 100)
		{
			money -= (cargoLvl + 1) * 100;
			system_.playerSaveData->set("Money", money);

			cargoLvl++;
			system_.playerSaveData->set("Cargo", cargoLvl);

			system_.audioManager->playSound("resources/sfx/pickup sound.ogg");
			system_.playerSaveData->saveToFile();

			*system_.gameState = GameStates::STATE_MISSIONSELECT;
			system_.audioManager->playSound("resources/sfx/button_click.ogg");
		}
	},
		cargo < 3 && ((cargo + 1) * 100) <= money));

	//Upgrade stars
	star1 = glm::vec4(0, 0, starSize.x / 2, starSize.y);
	star2 = glm::vec4(0, 0, starSize.x / 2, starSize.y);
	star3 = glm::vec4(0, 0, starSize.x / 2, starSize.y);
	switch (cargo)
	{
	case 3:
		star3 = glm::vec4(starSize.x / 2, 0, starSize.x / 2, starSize.y);
	case 2:
		star2 = glm::vec4(starSize.x / 2, 0, starSize.x / 2, starSize.y);
	case 1:
		star1 = glm::vec4(starSize.x / 2, 0, starSize.x / 2, starSize.y);
	}

	upgradeStars_.emplace_back(
		system_.resourceManager,
		glm::vec2(
			system_.cameraView->getSize().x * 0.86,
			system_.cameraView->getSize().y * 0.625),
		glm::vec2(
			system_.cameraView->getSize().x * 0.04,
			system_.cameraView->getSize().y * 0.07),
		"resources/textures/star_spritesheet.png",
		star1);

	upgradeStars_.emplace_back(
		system_.resourceManager,
		glm::vec2(
			system_.cameraView->getSize().x * 0.90,
			system_.cameraView->getSize().y * 0.625),
		glm::vec2(
			system_.cameraView->getSize().x * 0.04,
			system_.cameraView->getSize().y * 0.07),
		"resources/textures/star_spritesheet.png",
		star2);

	upgradeStars_.emplace_back(
		system_.resourceManager,
		glm::vec2(
			system_.cameraView->getSize().x * 0.94,
			system_.cameraView->getSize().y * 0.625),
		glm::vec2(
			system_.cameraView->getSize().x * 0.04,
			system_.cameraView->getSize().y * 0.07),
		"resources/textures/star_spritesheet.png",
		star3);



	std::string moneyText;
	moneyText = static_cast<std::ostringstream*>
		(&(std::ostringstream() << money))->str() + \
		"$";


	moneyLabel_ = PotatoEngine::GUILabel(
		system_.resourceManager,
		glm::vec2(
			system_.cameraView->getSize().x * 0.88,
			system_.cameraView->getSize().y * 0.07),
		moneyText,
		"resources/fonts/StardosStencil-Bold.ttf",
		system_.cameraView->getSize().y * 0.07);

	moneyLabel_.setOrigin(
		PotatoEngine::StylePos::X_MIDDLE | PotatoEngine::StylePos::Y_BOTTOM);

	moneyLabel_.setColor(sf::Color(0, 0, 0, 255));


}
