#ifndef POTATOENGINE_AUDIOMANAGER_H
#define POTATOENGINE_AUDIOMANAGER_H
#include <glm/glm.hpp>
#include <SFML/Audio.hpp>
#include "ResourceManager.h"
#include <vector>

namespace PotatoEngine
{
	class AudioManager
	{
	public:
		//Constructors
		AudioManager();
		AudioManager(ResourceManager* resourceManager);
		
		//Deconstructors
		~AudioManager();

		//Public functions.

		/// <summary>
		/// Plays a selected sound effect.
		/// </summary>
		/// <param name="filepath"> Filepath to the sound</param>
		/// <param name="pitch"> Sets the pitch of the sound, default value 1</param>
		/// <returns></returns>
		void playSound(std::string filepath, float pitch = 1.0f, bool loop = false, float volumeModifer = 1);
	
		/// <summary>
		/// Plays a selected music track.
		/// </summary>
		/// <param name="filepath"> Filepath to the music file.</param>
		/// <returns></returns>
		void playMusic(std::string filepath, bool loop);
		
		/// <summary>
		/// Stops the playback of the music file.
		/// </summary>
		/// <returns></returns>
		void stopMusic();
		
		/// <summary>
		/// Pauses the playback of the music track.
		/// </summary>
		/// <returns></returns>
		void pauseMusic();
		
		void stopAllSounds();

		/// <summary>
		/// Update function for the audiomanager.
		/// </summary>
		/// <returns></returns>
		void update();


		//Setters
		/// <summary>
		/// Sets the volume of the sound.
		/// </summary>
		/// <param name="soundVolume">The new value for the sound volume.</param>
		/// <returns></returns>
		void setSoundVolume(float soundVolume);

		/// <summary>
		/// Sets the volume of the music.
		/// </summary>
		/// <param name="musicVolume">The new value for the music volume.</param>
		/// <returns></returns>
		void setMusicVolume(float musicVolume);

		//Getters
		/// <summary>
		/// Returns the value for the sound volume.
		/// </summary>
		float getSoundVolume();

		/// <summary> Returns the value for the music volume. </summary>
		float getMusicVolume();


		/// <summary>Returns the path to the latest played music. </summary>
		std::string getLatestMusicPlayed();

	private:
		float soundVolume_;
		ResourceManager* resourceManager_;
		std::vector<sf::Sound> activeSound_;
		sf::Music* music_;

		std::string latestMusicPlayed_;
	};
}
#endif	// !POTATOENGINE_AUDIOMANAGER_H