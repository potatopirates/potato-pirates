#ifndef POTATOPIRATES_ENEMYHG_H
#define POTATOPIRATES_ENEMYHG_H

#include "EnemyBaseClass.h"
class EnemyHG :
	public EnemyBaseClass
{
public:
	//Constructors
	EnemyHG();
	EnemyHG(System system, glm::vec2 startPosition, Detection* playerDetection);
	//Deconstructors
	~EnemyHG();

	void fire(std::vector<Bullet>* bullets, Player* player) override;
	void update(float deltaTime, Player* player) override;

private:
	float fireDelay_;

	glm::vec2 bulletDirection_;
};

#endif // POTATOPIRATES_ENEMYHG_H