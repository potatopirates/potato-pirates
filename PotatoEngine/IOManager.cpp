#include "IOManager.h"
#include <fstream>
#include <string>
#include <streambuf>

namespace PotatoEngine
{

	IOManager::IOManager()
	{
	}


	IOManager::~IOManager()
	{
	}

	bool IOManager::readFileToBuffer(std::string filePath, std::vector<unsigned char>& buffer)
	{
		std::ifstream file(filePath, std::ios::binary);
		if (file.fail())
		{
			perror(filePath.c_str());
			return false;
		}

		//seek to the end
		file.seekg(0, std::ios::end);

		//Get the file size
		int fileSize = (int)file.tellg();

		//seek to the beginning
		file.seekg(0, std::ios::beg);

		//Discard any header bytes that might be present from size.
		// (Probablly not needed)
		fileSize -= (int)file.tellg();

		//Resizethe buffer to match the file size.
		buffer.resize(fileSize);

		//Read the data into the buffer
		file.read((char *)&(buffer[0]), fileSize);

		//Close the stream
		file.close();

		return true;
	}

	bool IOManager::readFileToString(std::string filePath, std::string & output)
	{

		std::ifstream file("file.txt");

		if (file.fail())
		{
			perror(filePath.c_str());
			return false;
		}

		std::string str;

		file.seekg(0, std::ios::end);
		str.reserve(file.tellg());
		file.seekg(0, std::ios::beg);

		str.assign((std::istreambuf_iterator<char>(file)),
			std::istreambuf_iterator<char>());

		output = str;
		return true;
	}
}