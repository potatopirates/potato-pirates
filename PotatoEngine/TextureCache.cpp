#include "TextureCache.h"


namespace PotatoEngine
{
	TextureCache::TextureCache()
	{
		/*Empty*/
	}


	TextureCache::~TextureCache()
	{
		/*Empty*/
	}

	sf::Texture * TextureCache::getTexture(std::string texturePath)
	{
		//Lookup the texture and see if its in the map
		std::map<std::string, sf::Texture>::iterator mit = textureMap_.find(texturePath);

		//Check if its not in the map
		if (mit == textureMap_.end())
		{

			//Create the texture
			sf::Texture newTexture;
			//And load it fromthe file path
			newTexture.loadFromFile(texturePath);

			//Insert the texture into the map
			textureMap_.insert(make_pair(texturePath, newTexture));

			//Return the loaded texture.
			return &textureMap_.at(texturePath);
		}


		//Return the texture that is in the map.
		return &mit->second;
	}
}