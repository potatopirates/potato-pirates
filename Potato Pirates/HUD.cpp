#include "HUD.h"
#include <iostream>
#include <sstream> 
#include <math.h>

HUD::HUD()
{
}

HUD::HUD(System system, Player* player, Detection* playerDetection, Score* score) :
	system_(system),
	player_(player),
	playerDetection_(playerDetection),
	score_(score),
	displayScore_(0),
	detectedAlarmShown_(120)
{
	playerHealth_ = player_->getLife();
	playerAmmo_ = player_->getAmmo();
	displayScore_ = score_->getScore();

	color_ = sf::Color(255, 255, 255, 255);
	//ICE HUD
	ICEHUDTexture_ = system_.resourceManager->getTexture("resources/textures/ICEHUD_spritesheet.png");


	glm::vec2 tmp;
	glm::vec2 size = glm::vec2(
		player_->getSize().x * 1.75,
		player_->getSize().y * 1.75);
	tmp = glm::vec2(
		player_->getNoiseCir().x - size.x / 2,
		player_->getNoiseCir().y - size.y / 2
		);


	ICEHUDAmmoLabel_ = PotatoEngine::GUILabel(
		system_.resourceManager,
		glm::vec2(
			tmp.x + size.x / 2,
			tmp.y + size.x * 0.69		
			),
		static_cast<std::ostringstream*>(&(std::ostringstream() << player_->getAmmo()))->str(),
		"resources/fonts/StardosStencil-Bold.ttf",
		size.y * 0.19
		);
	ICEHUDAmmoLabel_.setOrigin(PotatoEngine::X_MIDDLE | PotatoEngine::Y_TOP);

	ICEHUDScore_ = PotatoEngine::GUILabel(
		system_.resourceManager,
		glm::vec2(
			system_.window->mapPixelToCoords(sf::Vector2i(system_.window->getSize().x/2, 0)).x,
			25
			),
		"0",
		"resources/fonts/StardosStencil-Bold.ttf",
		system_.window->getSize().y / 18
		);

	
	ICEHUDScore_.setOrigin(PotatoEngine::X_RIGHT | PotatoEngine::Y_MIDDLE);



	ICEHUDScoreOutLine_ = PotatoEngine::GUILabel(
		system_.resourceManager,
		glm::vec2(
			system_.window->mapPixelToCoords(sf::Vector2i(system_.window->getSize().x / 2, 0)).x,
			25
			),
		"0",
		"resources/fonts/StardosStencil-Outline.ttf",
		system_.window->getSize().y / 18
		);
	ICEHUDScoreOutLine_.setOrigin(PotatoEngine::X_RIGHT | PotatoEngine::Y_MIDDLE);

	//Sprites for power ups
	powerUpSize_ = glm::vec2(200, 200);
	//Firework obtained
	fireworksAnimatedButton_ = PotatoEngine::AnimatedTexture(system_.animationManager->getAnimation("FireworkButton"));
	//Firework active
	fireworksActiveAnimatedButton_ = PotatoEngine::AnimatedTexture(system_.animationManager->getAnimation("FireworkActive"));
	//Moonshine
	moonshineAnimatedButton_ = PotatoEngine::AnimatedTexture(system_.animationManager->getAnimation("Moonshine"));
	//Potato cloud
	potatoCloudAnimatedButton_ = PotatoEngine::AnimatedTexture(system_.animationManager->getAnimation("PotatoCloud"));
	//Alerted alarm
	alertedLabel_ = PotatoEngine::GUILabel(
		system_.resourceManager,
		glm::vec2(
			system_.window->mapPixelToCoords(sf::Vector2i(system_.window->getSize().x / 2, 0)).x,
			system_.window->mapPixelToCoords(sf::Vector2i(0, system_.window->getSize().y / 2)).y),
		"WARNING!",
		"resources/fonts/StardosStencil-Bold.ttf",
		system_.cameraView->getSize().y / 9);
	alertedCounter_ = PotatoEngine::GUILabel(
		system_.resourceManager,
		glm::vec2(
			system_.window->mapPixelToCoords(sf::Vector2i(system_.window->getSize().x / 2, 0)).x,
			system_.window->mapPixelToCoords(sf::Vector2i(0, system_.window->getSize().y / 2)).y),
		"",
		"resources/fonts/StardosStencil-Bold.ttf",
		system_.cameraView->getSize().y / 9);
	//Detected alarm
	detectedLabel_ = PotatoEngine::GUILabel(
		system_.resourceManager,
		glm::vec2(
			system_.window->mapPixelToCoords(sf::Vector2i(system_.window->getSize().x / 2, 0)).x,
			system_.window->mapPixelToCoords(sf::Vector2i(0, system_.window->getSize().y / 2)).y),
		"DETECTED!",
		"resources/fonts/StardosStencil-Bold.ttf",
		system_.cameraView->getSize().y / 9);
	detectedLabelOutline_ = PotatoEngine::GUILabel(
		system_.resourceManager,
		glm::vec2(
			system_.window->mapPixelToCoords(sf::Vector2i(system_.window->getSize().x / 2, 0)).x,
			system_.window->mapPixelToCoords(sf::Vector2i(0, system_.window->getSize().y / 2)).y),
		"DETECTED!",
		"resources/fonts/StardosStencil-Outline.ttf",
		system_.cameraView->getSize().y / 9);

}

HUD::~HUD()
{
}

void HUD::update(float deltaTime)
{

	if (displayScore_ != score_->getScore())
	{
		displayScore_ = score_->getScore();

		ICEHUDScore_.setText(std::to_string(displayScore_));
		ICEHUDScoreOutLine_.setText(std::to_string(displayScore_));

	}

	playerHealth_ = player_->getLife();

	if (playerAmmo_ != player_->getAmmo())
	{
		playerAmmo_ = player_->getAmmo();
		ICEHUDAmmoLabel_.setText(std::to_string(playerAmmo_));
	}
	if (playerDetection_->detected && detectedAlarmShown_ > 0)
	{
		detectedAlarmShown_ -= 1 * deltaTime;
	}
	moonshineAnimatedButton_.update(deltaTime);
	fireworksActiveAnimatedButton_.update(deltaTime);
	fireworksAnimatedButton_.update(deltaTime);
	potatoCloudAnimatedButton_.update(deltaTime);
}

void HUD::draw(PotatoEngine::SpriteBatch * spriteBatch)
{
	glm::vec2 tmp;

	glm::vec2 size = glm::vec2(
		player_->getSize().x * 1.75,
		player_->getSize().y * 1.75);

	tmp = glm::vec2(
		player_->getNoiseCir().x - size.x / 2,
		player_->getNoiseCir().y - size.y / 2
		);


	//Draw RAM
	spriteBatch->draw(ICEHUDTexture_,
		sf::FloatRect(
			tmp.x,
			tmp.y,
			size.x,
			size.y),
		sf::IntRect(
			0,
			0,
			ICEHUDTexture_->getSize().x,
			ICEHUDTexture_->getSize().y / 2),
		sf::Color(255, 255, 255, 255 / 1.25));

	//Draw hp

	float lifeRemove =size.x / static_cast<float>(player_->getStartLife());
	lifeRemove *= static_cast<float>(player_->getStartLife() - player_->getLife());

	float lifeRemove2 = static_cast<float>(ICEHUDTexture_->getSize().x) / static_cast<float>(player_->getStartLife());
	lifeRemove2 *= static_cast<float>(player_->getStartLife() - player_->getLife());

	spriteBatch->draw(ICEHUDTexture_,
		sf::FloatRect(
			tmp.x,
			tmp.y,
			size.x - lifeRemove,
			size.y/2),
		sf::IntRect(
			0,
			ICEHUDTexture_->getSize().y / 2,
			ICEHUDTexture_->getSize().x - lifeRemove2,
			ICEHUDTexture_->getSize().y / 4),
		sf::Color(255, 255, 255, 255));

	//Draw ammo
	
	float ammoRemove = size.x / static_cast<float>(player_->getMaxAmmo());
	ammoRemove *= static_cast<float>(player_->getMaxAmmo() - player_->getAmmo());

	float ammoRemove2 = static_cast<float>(ICEHUDTexture_->getSize().x) / static_cast<float>(player_->getMaxAmmo());
	ammoRemove2 *= static_cast<float>(player_->getMaxAmmo() - player_->getAmmo());

	spriteBatch->draw(ICEHUDTexture_,
		sf::FloatRect(
			tmp.x,
			tmp.y + size.y / 2,
			size.x - ammoRemove,
			size.y / 2),
		sf::IntRect(
			0,
			ICEHUDTexture_->getSize().y / 2 + ICEHUDTexture_->getSize().y / 4,
			ICEHUDTexture_->getSize().x - ammoRemove2,
			ICEHUDTexture_->getSize().y / 4),
		sf::Color(255, 255, 255, 255));

	//Draw Power up

	switch (player_->getObtainedPowerUp())
	{
	case 0:
		fireworksAnimatedButton_.draw(spriteBatch,
			glm::vec4(system_.window->mapPixelToCoords(sf::Vector2i(system_.window->getSize().x / 2, 0)).x - powerUpSize_.x / 2,
				system_.window->mapPixelToCoords(sf::Vector2i(0, system_.window->getSize().y)).y - powerUpSize_.y,
				powerUpSize_.x,
				powerUpSize_.y),
			sf::Color(255, 255, 255, 200));
		break;
	case 1:
		moonshineAnimatedButton_.draw(spriteBatch,
			glm::vec4(system_.window->mapPixelToCoords(sf::Vector2i(system_.window->getSize().x / 2, 0)).x - powerUpSize_.x / 2,
				system_.window->mapPixelToCoords(sf::Vector2i(0, system_.window->getSize().y)).y - powerUpSize_.y,
				powerUpSize_.x,
				powerUpSize_.y),
			sf::Color(255, 255, 255, 200));
		break;
	case 2:
		potatoCloudAnimatedButton_.draw(spriteBatch,
			glm::vec4(system_.window->mapPixelToCoords(sf::Vector2i(system_.window->getSize().x / 2, 0)).x - powerUpSize_.x / 2,
				system_.window->mapPixelToCoords(sf::Vector2i(0, system_.window->getSize().y)).y - powerUpSize_.y,
				powerUpSize_.x,
				powerUpSize_.y),
			sf::Color(255, 255, 255, 200));
		break;
	default:
		break;
	}
	if (player_->getFireworkActive() && player_->getObtainedPowerUp() == -1)
	{
		fireworksActiveAnimatedButton_.draw(spriteBatch,
			glm::vec4(system_.window->mapPixelToCoords(sf::Vector2i(system_.window->getSize().x / 2, 0)).x - powerUpSize_.x / 2,
				system_.window->mapPixelToCoords(sf::Vector2i(0, system_.window->getSize().y)).y - powerUpSize_.y,
				powerUpSize_.x,
				powerUpSize_.y),
			sf::Color(255, 255, 255, 200));
	}

}


void HUD::drawText(sf::RenderWindow * window, float alphaRemove)
{
		glm::vec2 icetmp;

		glm::vec2 size = glm::vec2(
			player_->getSize().x * 1.75,
			player_->getSize().y * 1.75);
		icetmp = glm::vec2(
			player_->getNoiseCir().x - size.x / 2,
			player_->getNoiseCir().y - size.y / 2
			);

		ICEHUDAmmoLabel_.setPos(glm::vec2(
			icetmp.x + size.x / 2,
			icetmp.y + size.x * 0.75
			));

		ICEHUDAmmoLabel_.setColor(sf::Color(255, 255,255, 255- alphaRemove));
		
		ICEHUDAmmoLabel_.setOrigin(PotatoEngine::X_MIDDLE | PotatoEngine::Y_TOP);
		ICEHUDAmmoLabel_.drawText(window);

		
		//Score
		ICEHUDScore_.setPos(glm::vec2(
			system_.window->mapPixelToCoords(sf::Vector2i(system_.window->getSize().x / 2, 0)).x,
			system_.window->mapPixelToCoords(sf::Vector2i(0, 50)).y
			));

		ICEHUDScoreOutLine_.setPos(glm::vec2(
			system_.window->mapPixelToCoords(sf::Vector2i(system_.window->getSize().x / 2, 0)).x,
			system_.window->mapPixelToCoords(sf::Vector2i(0, 50)).y

			));


		ICEHUDScoreOutLine_.setColor(sf::Color(0, 0, 0, 255 - alphaRemove));
		ICEHUDScoreOutLine_.drawText(window);

		ICEHUDScore_.setColor(sf::Color(237, 237, 38, 255 - alphaRemove));
		ICEHUDScore_.drawText(window);

		//Alert
		if (playerDetection_->alerted && playerDetection_->alertedCounter > 0)
		{
			alertedLabel_.setOrigin(PotatoEngine::X_MIDDLE | PotatoEngine::Y_MIDDLE);
			alertedLabel_.setPos(glm::vec2(
				system_.window->mapPixelToCoords(sf::Vector2i(system_.window->getSize().x / 2, 0)).x,
				system_.window->mapPixelToCoords(sf::Vector2i(0, system_.window->getSize().y / 4)).y));
			alertedLabel_.setColor(sf::Color(255, 255, 0, 175 - alphaRemove));
			alertedLabel_.drawText(window);

			alertedCounter_.setOrigin(PotatoEngine::X_MIDDLE | PotatoEngine::Y_MIDDLE);
			alertedCounter_.setText(std::to_string(static_cast<int>(std::ceil(playerDetection_->alertedCounter / 10))));
			alertedCounter_.setPos(glm::vec2(
				system_.window->mapPixelToCoords(sf::Vector2i(system_.window->getSize().x / 2, 0)).x,
				system_.window->mapPixelToCoords(sf::Vector2i(0, system_.window->getSize().y / 2)).y));
			alertedCounter_.setColor(sf::Color(255, 255, 0, 175 - alphaRemove));
			alertedCounter_.drawText(window);


		}

		//Detection
		if (playerDetection_->detected && detectedAlarmShown_ > 0)
		{
			detectedLabelOutline_.setOrigin(PotatoEngine::X_MIDDLE | PotatoEngine::Y_MIDDLE);
			detectedLabelOutline_.setPos(glm::vec2(
				system_.window->mapPixelToCoords(sf::Vector2i(system_.window->getSize().x / 2, 0)).x,
				system_.window->mapPixelToCoords(sf::Vector2i(0, system_.window->getSize().y / 4)).y));
			detectedLabelOutline_.setColor(sf::Color(0, 0, 0, 25 - alphaRemove));
			detectedLabelOutline_.drawText(window);

			detectedLabel_.setOrigin(PotatoEngine::X_MIDDLE | PotatoEngine::Y_MIDDLE);
			detectedLabel_.setPos(glm::vec2(
				system_.window->mapPixelToCoords(sf::Vector2i(system_.window->getSize().x / 2, 0)).x,
				system_.window->mapPixelToCoords(sf::Vector2i(0, system_.window->getSize().y / 4)).y));
			detectedLabel_.setColor(sf::Color(255, 0, 0, 255 - alphaRemove));
			detectedLabel_.drawText(window);
		}
}
