#ifndef POTATOPIRATES_PLAYER_H
#define POTATOPIRATES_PLAYER_H
#include "System.h"
#include <glm/glm.hpp>
#include <PotatoEngine\SpriteBatch.h>
#include "AbilityFirework.h"
#include "Bullet.h"
#include "AbilityMoonshine.h"
#include "AbilityPotatoCloud.h"
#include "Pickup.h"

struct Detection
{
	bool detected;
	bool alerted;
	float alertedCounter;
};

class Player
{

	enum Powerup
	{
		FIREWORK,
		MOONSHINE,
		POTATOCLOUD
	};


public:
	//Constructors
	Player();
	Player(System system, 
		std::vector<Bullet>* bullets,
		Detection* playerDetection, std::vector<AbilityFirework>* fireworks);
	
	//Deconstructor
	~Player();

	//Player functions
	/// <summary>Update the player, should be called each frame.</summary>
	void update(float deltaTime);

	void draw(PotatoEngine::SpriteBatch* spriteBatch);

	void addPickup(PickupType type);

	void decreaseLife(unsigned char amount);

	//Setters ------------------------------------------------------
	//Setters ------------------------------------------------------
	//Setters ------------------------------------------------------

	/// <summary>
	/// Sets the position using two floats.
	/// </summary>
	///<param name="x">The x coordinate.</param>
	///<param name="y">The y coordinate.</param>
	/// <returns></returns>
	void setPos(float x, float y);

	/// <summary> Sets the position using a vec2 variable.</summary>
	void setPos(glm::vec2 pos);

	void setLife(unsigned char life);

	void setAmmo(unsigned short ammo);


	//Getters ------------------------------------------------------
	//Getters ------------------------------------------------------
	//Getters ------------------------------------------------------
	/// <summary>Returns the x coordinate in float format. </summary>
	float getX() const;

	/// <summary>Returns the y coordinate in float format. </summary>
	float getY() const;

	/// <summary>Returns the position in glm::vec2 format. </summary>
	glm::vec2 getPos() const;

	char getLife() const;

	char getStartLife() const;

	short getAmmo() const;
	short getMaxAmmo() const;

	int getFireworks() const;

	glm::vec2 getSize() const;

	glm::vec4 getHitBox() const;

	glm::vec3 getNoiseCir() const;

	//Abilitys
	bool getFireworkActive() const;

	bool isMoonshineActive();
	int getObtainedPowerUp();
private:
	
	//Functions
	void inputHandler(float deltaTime);
	void ActivateDeveloperFeatures();
	void activateUpgrades();

	//variables
	System system_;

	//Textures /Animations
	PotatoEngine::AnimatedTexture noiseAnimatedTexture_;
	PotatoEngine::AnimatedTexture animatedTexture_;
	std::string playerAnimation_;

	//Area stats
	glm::vec2 pos_;
	glm::vec2 playerSize_;
	glm::vec2 noiseSize_;
	glm::vec4 hitBoxMargin_;


	//Abilities
	int abilityOne_;
	float fireworkCooldown_;

	AbilityFirework* firework_;
	std::vector<AbilityFirework>* fireworks_;

	int abilityMoonshine_;
	AbilityMoonshine moonshine_;

	int abilityPotatoCloud_;
	AbilityPotatoCloud potatoCloud_;

	Powerup powerup_;
	bool powerupAvailable_;

	//Draw variables
	sf::Color color_;

	//Bullet handling
	std::vector<Bullet>* bullets_;
	char fireDelay_;


	//Life
	//Life is a char = 0 to 255
	//Ammo is a unsigned short = 0 to 65, 535
	char life_;
	char startLife_;
	bool hitProtection = false;
	float hitProtectionTimer = 0;

	//Cargo/Ammo
	short ammo_; 
	short maxAmmo_;
	
	//Movement speed
	short speed_;

	//Detection / Stealth
	Detection* detection_;
	float noiseRad_;
	//noiseHelper what increase the circle when the player shoot
	float noiseHelper; 


	//Tutorial objects (used for the level 1 tutorial)
	sf::Texture* tutorialTexture_;
	std::map<char, bool> tutorialKeyCheck_;
	std::map<char, int> tutorialKeyCheckAlphaLvl_;
	bool tutorial_;

	//Sound helper
	float shootSoundDelay_;


	//Developer features
	std::map<std::string, bool> developerFeaturesMap_;
};



#endif //POTATOPIRATES_PLAYER_H