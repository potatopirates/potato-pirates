#ifndef POTATOPIRATES_EXPLOSION_H
#define POTATOPIRATES_EXPLOSION_H
#include <glm\glm.hpp>
#include "System.h"

class Explosion
{
public:
	//Constructors
	Explosion();
	Explosion(System system, glm::vec4 destRect);

	//Deconstructors
	~Explosion();

	void draw(PotatoEngine::SpriteBatch* spriteBatch);

	bool update(float deltaTime);

private:

	System system_;

	glm::vec4 destRect_;

	PotatoEngine::AnimatedTexture animatedTexture_;


};

#endif // POTATOPIRATES_EXPLOSION_H