#include "EnemyScout.h"



EnemyScout::EnemyScout()
{
}

EnemyScout::EnemyScout(System system, glm::vec2 startPosition, Detection * playerDetection) :
	EnemyBaseClass(system, startPosition, playerDetection, 3)
{
	movementPatternID_ = LINEAR;
	animatedTexture_ = PotatoEngine::AnimatedTexture(system_.animationManager->getAnimation("Scout"));

	size_ = glm::vec2(
		system_.cameraView->getSize().x / 16,
		system_.cameraView->getSize().y / 9);
	size_.y *= 1.4;

	detectionSize_ = glm::vec2(size_.x * 4, size_.x * 4);
	detectionRadious = detectionSize_.x / 2;

	life_ = 1;

	//Setup hitbox
	hitBoxMargin_ = glm::vec4(
		size_.x * -0.4,
		size_.y * -0.1,
		size_.x * 0.7,
		size_.y * 0.2
		);
}


EnemyScout::~EnemyScout()
{
}

void EnemyScout::fire(std::vector<Bullet>* bullets, Player* player)
{
	/*Does not fire*/
}

void EnemyScout::update(float deltaTime, Player* player)
{
	//Move enemy plane
	movementPattern(deltaTime, player);
}
