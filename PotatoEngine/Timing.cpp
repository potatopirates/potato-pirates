#include "Timing.h"
namespace PotatoEngine
{
	///Public------------------------------------------------------------------
	//Constructor
	FpsLimiter::FpsLimiter()
	{

	}

	//Init the FpsLimiter
	void FpsLimiter::init(float maxFPS)
	{
		setMaxFPS(maxFPS);
		clock_.restart();
	}

	//Sets the current FPS the game will be locked to
	void FpsLimiter::setMaxFPS(float maxFPS)
	{
		maxFPS_ = maxFPS;
	}

	void FpsLimiter::begin()
	{
		start_ticks_ = clock_.getElapsedTime().asMilliseconds();
	}

	//end() returns the current FPS and "delay/limit" the fps
	float FpsLimiter::end()
	{
		//Calculate the fps
		calculateFPS();

		float frameTicks = static_cast<float>( clock_.getElapsedTime().asMilliseconds() - start_ticks_);
		//Limit the FPS to the max FPS
		if (1000.0f / maxFPS_ > frameTicks)
		{
			
			sf::sleep( sf::milliseconds( static_cast<sf::Int32>( 1000.0f / maxFPS_ - frameTicks)));
		}

		//Return the current FPS
		return fps_;
	}

	//Getters
	float FpsLimiter::getElapsedTime() const
	{
		return static_cast<float>( clock_.getElapsedTime().asMilliseconds());
	}

	///Private-----------------------------------------------------------------

	void FpsLimiter::calculateFPS()
	{
		//Number of frames to average
		static const int NUM_SAMPLES = 10;

		static float frameTimes[NUM_SAMPLES];
		static int currentFrame = 0;
		//The ticks of the previous tick. at start get current.
		static float prevTicks = static_cast<float>( clock_.getElapsedTime().asMilliseconds());

		//Current ticks
		float currentTicks = static_cast<float>( clock_.getElapsedTime().asMilliseconds());

		//Calculate the number of ticks for this frame
		frame_time_ = currentTicks - prevTicks;
		frameTimes[currentFrame % NUM_SAMPLES] = frame_time_;

		//Set previous tick to current tick.
		prevTicks = currentTicks;

		//Number of frames to average
		int count;

		currentFrame++;
		if (currentFrame < NUM_SAMPLES)
			count = currentFrame;
		else
			count = NUM_SAMPLES;

		//Average all the frame times
		float frameTimeAverage = 0;
		for (int i = 0; i < count; i++)
			frameTimeAverage += frameTimes[i];

		frameTimeAverage /= count;

		//Calculate FPS
		if (frameTimeAverage > 0)
			fps_ = 1000.0f / frameTimeAverage;
		else
			fps_ = 60.0f; //Faking. its not 60 fps. But we tell them it is.
	}



}