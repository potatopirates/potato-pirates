#ifndef POTATOENGINE_ANIMATIONMANAGER_H
#define POTATOENGINE_ANIMATIONMANAGER_H
#include <glm\glm.hpp>
#include "ResourceManager.h"
#include "SpriteBatch.h"
#include <unordered_map>

namespace PotatoEngine
{
	struct Animation
	{
	public:
		//Constructors
		Animation(sf::Texture* texture, float animationSpeed);
		Animation();

		std::vector<sf::IntRect> frams_; //vector of uvrects : )
		sf::Texture* spriteSheet_;

		float animationSpeed_;
	};

	class AnimatedTexture
	{
	public:
		AnimatedTexture();
		AnimatedTexture(Animation* animation);

		bool update(float deltaTime);
		void draw(SpriteBatch* spritebatch, glm::vec4 destRect, sf::Color color = sf::Color(255,255,255,255));

		void setAnimation(Animation* animation);


		void setCurrentFrame(unsigned int frame);
		unsigned int getTotalFrams() const;

	private:
		Animation* animation_;

		float currentTick_;
		int currentFrame_;
	};

	class AnimationManager
	{
	public:
		//Constructors
		AnimationManager();
		
		//Deconstructors
		~AnimationManager();

		/// <summary>
		/// Create an animation that will be stored inside the animation manager
		/// </summary>
		///<param name="animationName">The name of the animation</param>
		///<param name="animationTexture">The texture the animation will use</param>
		///<param name="animationSpeed">The speed of the animation.</param>
		/// <returns></returns>
		bool createAnimation(std::string animationName,
			sf::Texture* animationTexture,
			float animationSpeed = 60);

		/// <summary>
		/// Add a new frame to the animation
		/// </summary>
		///<param name="animationName">The name of the animation you wish to add a frame to</param>
		///<param name="uvRect">The cordinate/uvrect of the frame in the spirtesheet</param>
		/// <returns></returns>
		void addFrameToAnimation(std::string animationName, glm::vec4 uvRect);
		
		/// <summary>
		/// Get an pointer to an animation
		/// </summary>
		///<param name="animationName">The name of the animation</param>
		/// <returns>Animation sprite</returns>
		Animation* getAnimation(std::string animation);


	private:
		std::map<std::string, Animation> animationMap_;
	};
}

#endif	//POTATOENGINE_ANIMATIONMANAGER_H