#include "DefeatState.h"



DefeatState::DefeatState(System system) :
	system_(system)
{
	initializeButtons();

	backgroundImage_ = PotatoEngine::GUIImage(system_.resourceManager,
		glm::vec2(0),
		glm::vec2(system_.cameraView->getSize().x, system_.cameraView->getSize().y),
		"resources/textures/gameoverBG.png", glm::vec4(0.0f));

	gameOver_ = 255;

	system_.resetView();
	*system_.cursor = true;
}

DefeatState::~DefeatState()
{
}



void DefeatState::update(float deltaTime)
{
	if (gameOver_ > 0)
	{
		gameOver_ -= 4 * deltaTime;
	}

	for (unsigned int i = 0; i < GUIButtons_.size(); i++)
	{
		GUIButtons_[i].update(system_.inputManager, deltaTime);
	}
}

void DefeatState::draw()
{
	spriteBatch_.begin();

	backgroundImage_.draw(&spriteBatch_);
	
	for (unsigned int i = 0; i < GUIButtons_.size(); i++)
	{
		GUIButtons_[i].draw(&spriteBatch_);
	}

	if (gameOver_ > 0)
	{
		spriteBatch_.draw(
			system_.resourceManager->getTexture("resources/textures/hitboxArea.png"),
			sf::FloatRect(
				0,
				0,
				system_.cameraView->getSize().x,
				system_.cameraView->getSize().y),
			sf::IntRect(0, 0, 0, 0),
			sf::Color(0, 0, 0, static_cast<sf::Uint8>(gameOver_)));
	}

	spriteBatch_.end();

	system_.window->draw(spriteBatch_);

	for (unsigned int i = 0; i < GUIButtons_.size(); i++)
	{
		GUIButtons_[i].drawText(system_.window);
	}
}

void DefeatState::initializeButtons()
{
	float xHelper = static_cast<float>(system_.cameraView->getSize().x / 5.0f);
	float yHelper = static_cast<float>(system_.cameraView->getSize().y / 10.0f);

	float xSizeHelper = static_cast<float>(
		system_.cameraView->getSize().x / 2.0f - xHelper / 2);



	glm::vec2 margin(
		system_.cameraView->getSize().x / 2 - xHelper - 50,
		150);
	std::string imagePath = "resources/textures/Mainbutton_spritesheet.png";
	std::string fontPath = "resources/fonts/StardosStencil-Regular.ttf";
	unsigned int fontSize = 50;

	GUIButtons_.push_back(PotatoEngine::GUIButton(
		system_.resourceManager,
		glm::vec2(
			xSizeHelper,
			yHelper * 4.8) + margin,
		glm::vec2(xHelper + xHelper / 2, yHelper),
		imagePath,
		"Restart",
		fontSize,
		fontPath,
		[=](PotatoEngine::GUIButton& object, float deltaTime)
	{
		*system_.gameState = GameStates::STATE_BATTLESTATE;
	}));
	GUIButtons_.push_back(PotatoEngine::GUIButton(
		system_.resourceManager,
		glm::vec2(
			xSizeHelper,
			yHelper * 6.0) + margin,
		glm::vec2(xHelper + xHelper / 2, yHelper),
		imagePath,
		"Mission select",
		fontSize,
		fontPath,
		[=](PotatoEngine::GUIButton& object, float deltaTime)
	{
		*system_.gameState = GameStates::STATE_MISSIONSELECT;
		system_.audioManager->playSound("resources/sfx/button_click.ogg");
	}));
	GUIButtons_.push_back(PotatoEngine::GUIButton(
		system_.resourceManager,
		glm::vec2(
			xSizeHelper,
			yHelper * 7.2) + margin,
		glm::vec2(xHelper + xHelper / 2, yHelper),
		imagePath,
		"Exit to Menu",
		fontSize,
		fontPath,
		[=](PotatoEngine::GUIButton& object, float deltaTime)
	{
		*system_.gameState = GameStates::STATE_MENU;
		system_.audioManager->playSound("resources/sfx/button_click.ogg");
	}));
}