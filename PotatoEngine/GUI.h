#ifndef POTATOENGINE_GUI_H
#define POTATOENGINE_GUI_H

#include <glm\glm.hpp>
#include <functional>
#include <memory>

#include "SpriteBatch.h"
#include "InputManager.h"
#include "ResourceManager.h"

namespace PotatoEngine
{	
	/// <summary>
	/// The default GUI object. Is empty and does, nothing, use the children classes instead.
	/// </summary>
	/// <returns></returns>
	class GUIObject
	{
	public:
		/// <summary> Draws the GUI Object. </summary>
		///<param name="spriteBatch">The spritebatch the gui object will be drawn one.</param>
		/// <returns></returns>
		virtual void draw(SpriteBatch * spriteBatch) = 0;

		//Getters
		/// <summary> Returns the position of the gui object</summary>
		glm::vec2 getPos() const { return position_; }
		/// <summary> Returns the size of the object.</summary>
		glm::vec2 getSize() const { return size_; }

		//Setters
		/// <summary> Set the position of the GUI Object</summary>
		void setPos(glm::vec2 position) { position_ = position; }
		/// <summary> Set the size of the GUI Object</summary>
		void setSize(glm::vec2 size) { size_ = size; }

	private:

	protected:
		glm::vec2 position_;
		glm::vec2 size_;
	};

	///<summary>Tmp function used by diffrent gui objects.</summary>
	inline void defaultButtonAction(float deltaTime)
	{
		/*Empty*/
	}

	/// <summary>
	/// Call update each frame for handling diffrent states. Draw should be called before drawText.
	///the spritebatch sent into draw must be rendered before you run drawText. Else it will be rendered on the text
	/// </summary>
	/// <returns></returns>
	class GUIButton : public GUIObject
	{
	public:
		//Constructors
		GUIButton();
		GUIButton(ResourceManager* resourceManager,
			glm::vec2 pos,
			glm::vec2 size,
			std::string imagePath,
			std::string text = "",
			unsigned int fontSize = 30,
			std::string textFontPath = "",
			std::function<void(GUIButton&, float)> pressFunc = defaultButtonAction,
			bool enabled = true); 
		///Texture is seperated on the Y-axi, bottom = normal, mid = however, top = pressed.


			
		//Deconstructor
		~GUIButton();

		/// <summary> Update the button, checking with the inputmangaer if the mouse is over and/or pressing the button.</summary>
		void update(InputManager* inputManager, float deltatime);
		/// <summary> Draw the button onto a spritebatch.</summary>
		void draw(SpriteBatch* spriteBatch) override;
		/// <summary> Draw the text onto the window screen, should be called afterthe spritebatch has been rendered.</summary>
		void drawText(sf::RenderWindow* window);

		void runFunction(float deltatime);
			
		void setEnabled(bool enabled);

		void setPos(glm::vec2 position);



	private:
		char status_ = 0; //0 = nothing, 1 = hover, 2 = click
		char statusHelper_;
		bool enabled_ = true;

		std::function<void(GUIButton&, float)> pressFunc_;		//Function
		sf::Texture* buttonImage_;

		std::shared_ptr<sf::Font> font_;
		std::shared_ptr<sf::Text> buttonText_;

	};

	/// <summary>
	/// GUI Label, used to write text on the screen
	/// </summary>
	/// <returns></returns>
	
	enum StylePos
	{
		X_LEFT,
		X_MIDDLE,
		X_RIGHT,
		Y_TOP,
		Y_MIDDLE,
		Y_BOTTOM

	};
	class GUILabel : public GUIObject
	{
	public:
		//Constructor
		GUILabel();
		GUILabel(ResourceManager* resourceManager, 
			glm::vec2 pos,
			std::string text,
			std::string fontPath,
			int fontSize = 64);
		//Deconstructor
		~GUILabel();
		/// <summary>DON'T USE! USE drawText() INSTEAD!!!!!!!</summary>
		void draw(SpriteBatch* spriteBatch) override;

		/// <summary> Draw the text onto the window screen, should for the most part be called after your other drawing has been drawn and rendered.</summary>
		void drawText(sf::RenderWindow* window);

		//Setters
		/// <summary>
		/// Change the font of the label
		/// </summary>
		///<param name="size">The new character size.</param>
		/// <returns></returns>
		void setFontSize(int size);
		void setText(std::string text);

		void setOrigin(unsigned char data = 0);

		void setColor(sf::Color color);

		//Getters
		/// <summary> Returns the character size </summary>
		int getFontSize() const;
		std::string getText() const;

	protected:
		
		std::shared_ptr<sf::Font> font_;
		std::shared_ptr<sf::Text> text_;




	};
	
	/// <summary>
	/// GUI Image, used to print out images. Can currently not be moved or resized after creation.
	/// </summary>
	/// <returns></returns>
	class GUIImage : public GUIObject
	{
	public:
		//Constructors
		GUIImage() { /*Empty*/ };
		GUIImage(ResourceManager* resourceManager,
			glm::vec2 pos,
			glm::vec2 size,
			std::string imagePath,
			glm::vec4 uvRect = glm::vec4(0.0f),
			sf::Color color = sf::Color(255,255,255,255));
		
		//Decontructor
		~GUIImage() { /*Empty*/ };
		/// <summary> Draw the image on a spritebatch </summary>
		void draw(SpriteBatch * spriteBatch) override;

		//Setters
		/// <summary>
		/// Change the color of the image.
		/// </summary>
		///<param name="color">The new color, for exempel sf::Color(255,255,255,255)</param>
		/// <returns></returns>
		void setColor(sf::Color color);

		void setUVRect(sf::IntRect uvRect);

		//Getters
		/// <summary> Returns the color</summary>
		sf::Color getColor() const;

		sf::IntRect getUVRect() const;

	protected:
		sf::Texture* image_;
		sf::FloatRect destRect_;
		sf::IntRect uvRect_;
		sf::Color color_;
	};
	
	/// <summary>
	/// GUI Slider, it does what it says, and its amazing. It slides left, it slides right. What more do you need in life
	/// </summary>
	/// <returns></returns>
	class GUISlider :
		public GUIObject
	{
	public:
		//Constructor
		GUISlider();
		GUISlider(ResourceManager* resourceManager,
			glm::vec2 pos,
			glm::vec2 size,
			std::string imagePath,
			float minValue = 0,
			float maxValue = 100,
			float startingValue = 0);
		//Deconstructor
		~GUISlider();


		/// <summary> Update the button, checking with the inputmangaer if the mouse is over and/or pressing the button.</summary>
		void update(InputManager* inputManager, float deltatime);
		/// <summary> Draw the button onto a spritebatch.</summary>
		void draw(SpriteBatch* spriteBatch) override;

		void setValue(float value);
		void setMinValue(float minValue);
		void setMaxValue(float maxValue);

		float getValue();
		float getMinValue();
		float getMaxValue();

	private:
		float minValue_;
		float maxValue_;
		float value_;


		bool draging_;

		int status_;
		sf::Texture* sliderImage_;
	};
	
	/// <summary>
	/// A simple textbox
	/// </summary>
	/// <returns></returns>
	class GUITextBox :
		public GUIObject
	{
	public:
		//Constructors
		GUITextBox();
		GUITextBox(ResourceManager* resourceManager,
			glm::vec2 pos,
			glm::vec2 size,
			std::string imagePath,
			std::string fontPath,
			std::string startText = "Something"
			);
		//Deconstructor
		~GUITextBox();

		/// <summary> Update the button, checking with the inputmangaer if the mouse is over and/or pressing the button.</summary>
		void update(InputManager* inputManager, float deltatime);
		/// <summary> Draw the button onto a spritebatch.</summary>
		void draw(SpriteBatch* spriteBatch) override;

		/// <summary> Draw the text onto the window screen, should for the most part be called after your other drawing has been drawn and rendered.</summary>
		void drawText(sf::RenderWindow* window);

		std::string getText() const;

		void setInputing(bool inputing);

		bool wasEnterPressed() const;

	private:
		sf::Texture* textBoxTexture_;
		std::shared_ptr<sf::Font> font_;
		std::shared_ptr<sf::Text> text_;

		bool inputing_;
		int maxCharacters_;

		//Print//Inputing helping
		float counter_;
		bool printLine_;
		float spamHelper_;

		bool enterPressed_;

		void inputHandler(InputManager* inputManager);


	};


	class GUINumerBox :
		public GUIObject
	{
	public:
		//Constructors
		GUINumerBox();
		GUINumerBox(ResourceManager* resourceManager,
			glm::vec2 pos,
			glm::vec2 size,
			std::string imagePath,
			std::string fontPath,
			int startNumber = 0,
			int minNumber = 0,
			int maxNumber = 100,
			sf::Color textColor = sf::Color(255,255,255,255)
			);
		//Deconstructor
		~GUINumerBox();

		/// <summary> Update the button, checking with the inputmangaer if the mouse is over and/or pressing the button.</summary>
		void update(InputManager* inputManager, float deltatime);
		/// <summary> Draw the button onto a spritebatch.</summary>
		void draw(SpriteBatch* spriteBatch) override;

		/// <summary> Draw the text onto the window screen, should for the most part be called after your other drawing has been drawn and rendered.</summary>
		void drawText(sf::RenderWindow* window);
		
		void setNumber(int number);
		
		int getNumber() const;
		
	

	private:
		bool printBackground_;
		sf::Texture* textBoxTexture_;
		std::shared_ptr<sf::Font> font_;
		std::shared_ptr<sf::Text> text_;

		bool inputing_;
		int maxCharacters_;

		//Print//Inputing helping
		float counter_;
		bool printLine_;
		float spamHelper_;


		//Number info
		int number_;
		int minNumber_;
		int maxNumber_;

		void inputHandler(InputManager* inputManager);


	};



	/// <summary>
	/// A siimple checkbox, is ither off or on. yes
	/// </summary>
	/// <returns></returns>
	class GUICheckBox : GUIObject
	{
	public:
		//Constructors
		GUICheckBox();
		GUICheckBox(ResourceManager* resourceManager,
			glm::vec2 pos,
			glm::vec2 size,
			std::string imagePath,
			bool enabled = true);
		///Texture is seperated on the Y-axi, bottom = normal, mid = however, top = pressed.

		//Deconstructor
		~GUICheckBox();

		/// <summary> Update the button, checking with the inputmangaer if the mouse is over and/or pressing the button.</summary>
		void update(InputManager* inputManager, float deltatime);

		/// <summary> Draw the button onto a spritebatch.</summary>
		void draw(SpriteBatch* spriteBatch) override;

		void setEnabled(bool enabled);

		bool isEnabled();


	private:
		char status_ = 0; //0 = nothing, 1 = hover, 2 = click
		char statusHelper_;
		
		bool mouseHover = false;
		bool enabled_ = true;
		sf::Texture* checkBoxImage_;

	};


	/// <summary>
	/// GUI Slider, it does what it says, and its amazing. It slides left, it slides right. What more do you need in life
	/// </summary>
	/// <returns></returns>
	class GUIVerticalSlider :
		public GUIObject
	{
	public:
		//Constructor
		GUIVerticalSlider();
		GUIVerticalSlider(ResourceManager* resourceManager,
			glm::vec2 pos,
			glm::vec2 size,
			std::string imagePath,
			float minValue = 0,
			float maxValue = 100,
			float startingValue = 0);
		//Deconstructor
		~GUIVerticalSlider();


		/// <summary> Update the button, checking with the inputmangaer if the mouse is over and/or pressing the button.</summary>
		void update(InputManager* inputManager, float deltatime);
		/// <summary> Draw the button onto a spritebatch.</summary>
		void draw(SpriteBatch* spriteBatch) override;

		void setValue(float value);
		void setMinValue(float minValue);
		void setMaxValue(float maxValue);

		float getValue();
		float getMinValue();
		float getMaxValue();

	private:
		float minValue_;
		float maxValue_;
		float value_;


		bool draging_;

		int status_;
		sf::Texture* sliderImage_;
	};

}
#endif // POTATOENGINE_GUI_H