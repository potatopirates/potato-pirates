#include "StateHighScore.h"

#include <sstream> 


StateHighScore::StateHighScore()
{
}

StateHighScore::StateHighScore(System system) :
	system_(system)
{
	//Inititalize the state buttons.
	initilizeButtons();
	openHighScore();

	//Set up the background
	backgroundImage_ = PotatoEngine::GUIImage(system_.resourceManager, glm::vec2(0),
		glm::vec2(system_.cameraView->getSize().x, system_.cameraView->getSize().y),
	"resources/textures/highscoreBG.png", glm::vec4(0.0f));

	system_.resetView();
	*system_.cursor = true;

}


StateHighScore::~StateHighScore()
{
}

void StateHighScore::update(float deltaTime)
{
	for (unsigned int i = 0; i < GUIButtons_.size(); i++)
	{
		GUIButtons_[i].update(system_.inputManager, deltaTime);
	}
}

void StateHighScore::draw()
{
	spriteBatch_.begin();

	//Draw the background.
	backgroundImage_.draw(&spriteBatch_);

	//Draw the GUI objects
	for (unsigned int i = 0; i < GUIButtons_.size(); i++)
	{
		GUIButtons_[i].draw(&spriteBatch_);
	}

	spriteBatch_.end();
	system_.window->draw(spriteBatch_);
	for (unsigned int i = 0; i < GUIButtons_.size(); i++)
	{
		GUIButtons_[i].drawText(system_.window);
	}

	for (unsigned int i = 0; i < highscoreLabels_.size(); i++)
	{
		highscoreLabels_[i].drawText(system_.window);
	}
	
}

void StateHighScore::initilizeButtons()
{
	float xHelper = static_cast<float>(system_.cameraView->getSize().x / 5.0f);
	float yHelper = static_cast<float>(system_.cameraView->getSize().y / 10.0f);

	float xSizeHelper = static_cast<float>(
		system_.cameraView->getSize().x / 2.0f - xHelper / 2);


	glm::vec2 margin(
		system_.cameraView->getSize().x / 2 - xHelper - 50,
		150);
	std::string imagePath = "resources/textures/Mainbutton_spritesheet.png";
	std::string fontPath = "resources/fonts/StardosStencil-Regular.ttf";
	unsigned int fontSize = 50;

	GUIButtons_.push_back(PotatoEngine::GUIButton(
		system_.resourceManager,
		glm::vec2(
			system_.cameraView->getSize().x * 0.05,
			system_.cameraView->getSize().y * 0.83),
		glm::vec2(xHelper + xHelper / 2, yHelper),
		imagePath,
		"Back",
		fontSize,
		fontPath,
		[=](PotatoEngine::GUIButton& object, float deltaTime)
	{
		*system_.gameState = GameStates::STATE_MENU;
		system_.audioManager->playSound("resources/sfx/button_click.ogg");
	}));

}

void StateHighScore::openHighScore()
{
	//read into highScoreList_
	std::string scorePath = *system_.gameDictionary;
	scorePath += "/highscore.potato";

	PotatoEngine::SettingsParser scoreParser_;
	scoreParser_.loadFromFile(scorePath);
	std::string name;
	int score;

	scoreParser_.get("Rank_1_Name", name);
	scoreParser_.get("Rank_1_Score", score);
	highScoreList_.emplace_back(name, score);

	for (int i = 1; i < 11; i++)
	{
		std::string rs;
		std::string rn = "Rank_";
		rn += static_cast<std::ostringstream*>
			(&(std::ostringstream() << i))->str();
		rs = rn;
		rn += "_Name";
		rs += "_Score";
		scoreParser_.get(rn, name);
		scoreParser_.get(rs, score);
		highScoreList_.emplace_back(name, score);
		
		if (i < 6)
		{
			std::string scoreName = static_cast<std::ostringstream*>
				(&(std::ostringstream() <<i))->str();

			scoreName += ": ";
			scoreName += name;
			scoreName += " - ";
			scoreName += static_cast<std::ostringstream*>
				(&(std::ostringstream() << score))->str();

			sf::Vector2f tmp = system_.window->mapPixelToCoords(sf::Vector2i(
				system_.window->getSize().x * 0.14, 
				system_.window->getSize().y * 0.25 + 
				(system_.window->getSize().y * 0.08) * (i - 1)));
			

			highscoreLabels_.emplace_back(PotatoEngine::GUILabel(
				system_.resourceManager,
				glm::vec2(
					tmp.x,		
					tmp.y
					),
				scoreName,
				"resources/fonts/StardosStencil-Regular.ttf",
				system_.window->getSize().y * 0.05
				));

			highscoreLabels_[i-1].setColor(sf::Color(0, 0, 0, 255));
		}
		else
		{
			std::string scoreName = static_cast<std::ostringstream*>
				(&(std::ostringstream() << i))->str();

			scoreName += ": ";
			scoreName += name;
			scoreName += " - ";
			scoreName += static_cast<std::ostringstream*>
				(&(std::ostringstream() << score))->str();

			sf::Vector2f tmp = system_.window->mapPixelToCoords(sf::Vector2i(
				system_.window->getSize().x * 0.55,
				system_.window->getSize().y * 0.25 +
				(system_.window->getSize().y * 0.08) * (i - 6)));

			highscoreLabels_.emplace_back(PotatoEngine::GUILabel(
				system_.resourceManager,
				glm::vec2(
					tmp.x,
					tmp.y
					),
				scoreName,
				"resources/fonts/StardosStencil-Regular.ttf",
				system_.window->getSize().y * 0.05
				));




			highscoreLabels_[i - 1].setColor(sf::Color(0, 0, 0, 255));
		}
	}




}
