#include "ScoreFeedback.h"



ScoreFeedback::ScoreFeedback(System system,
	int score,
	glm::vec2 pos)
	:
	system_(system),
	position_(pos),
	score_(score),
	alpha_(255)
{
	GUILabel_ = PotatoEngine::GUILabel(
		system_.resourceManager,
		position_,
		std::to_string(score),
		"resources/fonts/StardosStencil-Regular.ttf",
		40
		);

	GUILabel_.setOrigin(
		PotatoEngine::StylePos::X_MIDDLE |
		PotatoEngine::StylePos::Y_MIDDLE);
}


ScoreFeedback::~ScoreFeedback()
{
	/*Empty*/
}

bool ScoreFeedback::update(float deltaTime)
{
	GUILabel_.setPos(GUILabel_.getPos() + glm::vec2(0,-1));
	alpha_ -= deltaTime * 5;
	
	return alpha_ < 0;
}

void ScoreFeedback::draw(sf::RenderWindow* window)
{
	GUILabel_.setColor(sf::Color(
		255, //R
		255, //G
		255, //B
		static_cast<sf::Uint8>(alpha_))); //A

	GUILabel_.drawText(window);

}
