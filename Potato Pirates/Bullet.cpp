#include "Bullet.h"



Bullet::Bullet() :
	speed_(10)
{
}

Bullet::Bullet(System system, 
	glm::vec2 position, 
	glm::vec2 direction, 
	BulletOwner bulletOwner,
	float bulletSpeed)
	:
	speed_(bulletSpeed),
	system_(system),
	direction_(direction),
	bulletOwner_(bulletOwner),
	position_(position)
{
	if (bulletOwner_ == BulletOwner::PLAYER)
	{
		animatedTexture_ = system_.animationManager->getAnimation("Bullet");
	}
	else
	{
		animatedTexture_ = system_.animationManager->getAnimation("EnemyBullet");
	}
	
	size_ = glm::vec2(
		system_.cameraView->getSize().x / 64, 
		system_.cameraView->getSize().y / 36);

	hitBoxMargin_ = glm::vec4(0);
}


Bullet::~Bullet()
{
}
#include <iostream>
bool Bullet::update(float deltaTime)
{
	position_ += direction_ * speed_ * deltaTime;
	animatedTexture_.update(deltaTime);
	
	if (position_.x > system_.window->mapPixelToCoords(sf::Vector2i(system_.window->getSize())).x
		||
		position_.x < system_.window->mapPixelToCoords(sf::Vector2i(0,0)).x - bulletWidth_
		||
		position_.y < 0
		||
		position_.y > system_.window->mapPixelToCoords(sf::Vector2i(system_.window->getSize())).y)
	{
		return true;
	}
	return false;
}

void Bullet::draw(PotatoEngine::SpriteBatch* spriteBatch)
{
	animatedTexture_.draw(spriteBatch,
		glm::vec4(
			position_.x,
			position_.y,
			size_.x,
			size_.y
			));

}


bool Bullet::collideWithObject(glm::vec4 objectPS)
{

	return system_.collisionManager->AABB(objectPS,
		glm::vec4(position_.x, position_.y, bulletWidth_, bulletWidth_));
}


int Bullet::getDamage() const
{
	return damage_;
}

BulletOwner Bullet::getOwner() const
{
	return bulletOwner_;
}

glm::vec4 Bullet::getHitBox() const
{
	if (bulletOwner_ == PLAYER)
	{
		return glm::vec4(
			position_.x,
			position_.y,
			size_.x,
			size_.y);
	}

	return glm::vec4(
		position_.x + size_.x / 4,
		position_.y + size_.y / 4,
		size_.x / 2,
		size_.y / 2);
}
