#include "AbilityPotatoCloud.h"



AbilityPotatoCloud::AbilityPotatoCloud()
{
}

AbilityPotatoCloud::AbilityPotatoCloud(System system) :
	system_(system),
	isActive_(false),
	duration_(0)
{
}


AbilityPotatoCloud::~AbilityPotatoCloud()
{
}

void AbilityPotatoCloud::update(float deltaTime)
{
	if (isActive() && duration_ > 0)
	{
		duration_ -= 1 * deltaTime;
	}
	else
	{
		isActive_ = false;
	}
}

bool AbilityPotatoCloud::isActive()
{
	return isActive_;
}

void AbilityPotatoCloud::setDuration(float duration)
{
	duration_ = duration;
}

void AbilityPotatoCloud::setActive()
{
	system_.audioManager->playSound("resources/sfx/enemiedead.ogg", 0.5, 0, 0.3);
	isActive_ = true;
	setDuration(1);
}
