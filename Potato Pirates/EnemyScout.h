#ifndef POTATOPIRATES_ENEMYSCOUT_H
#define POTATOPIRATES_ENEMYSCOUT_H

#include "EnemyBaseClass.h"
class EnemyScout :
	public EnemyBaseClass
{
public:
	//Constructors
	EnemyScout();
	EnemyScout(System system, glm::vec2 startPosition, Detection* playerDetection);
	
	//Deconstructors
	~EnemyScout();

	void fire(std::vector<Bullet>* bullets, Player* player) override;
	void update(float deltaTime, Player* player) override;



};

#endif // POTATOPIRATES_ENEMYSCOUT_H