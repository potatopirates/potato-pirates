#include "EnemyBaseClass.h"
#include <PotatoEngine/Converter.h>

EnemyBaseClass::EnemyBaseClass()
{
}

EnemyBaseClass::EnemyBaseClass(System system, glm::vec2 startPosition, Detection* playerDetection, float speed) :
	system_(system),
	position_(startPosition),
	color_(255, 255, 255, 255),
	playerDetection_(playerDetection),
	speed_(speed),
	hitColor_(255),
	direction_(0, -1),
	rotation(0)
{
	
	detectionTexture = system_.resourceManager->getTexture("resources/textures/radar.png");


}


EnemyBaseClass::~EnemyBaseClass()
{
}

void EnemyBaseClass::movementPattern(float deltaTime, Player* player)
{
	animatedTexture_.update(deltaTime);

	if (!playerDetection_->detected)
	{
		rotation += deltaTime * 5;
	}




	switch (movementPatternID_)
	{
	case SLOW_SINOID:
	{
		position_.x -= speed_ / 2  * deltaTime;
		position_.y -= sin(system_.cameraView->getSize().x * direction_.y);

		if (position_.y < 0)
		{
			direction_.y = 1;
		}
		else if (position_.y > system_.cameraView->getSize().y - size_.y)
		{
			direction_.y = -1;
		}
	}
	break;

	case FAST_SINOID:
	{
		position_.x -= speed_ / 2  * deltaTime;
		position_.y -= 2 * sin(system_.cameraView->getSize().x * direction_.y);

		if (position_.y < 0)
		{
			direction_.y = 1;
		}
		else if (position_.y > system_.cameraView->getSize().y - size_.y)
		{
			direction_.y = -1;
		}
	}
	break;

	case LINEAR:
	{
		if (direction_.y == -1)
		{
			direction_.y = (rand() % 10) * 0.1 * pow(-1, rand() % 2 + 1);
		}
		position_.x -= speed_ * deltaTime;
		position_.y -= direction_.y * deltaTime;
		if (position_.y < 0 || position_.y > system_.cameraView->getSize().y - size_.y)
		{
			direction_.y *= -1;
		}
		

	}
	break;

	case HOMING:
	{
		direction_ = glm::normalize(player->getPos() - position_);
		position_.x -= speed_ * deltaTime;
		position_.y += direction_.y * deltaTime * speed_;
		player->getPos();
	}
	break;

	default:
		break;
	}
}

void EnemyBaseClass::draw(PotatoEngine::SpriteBatch * spriteBatch)
{

	
	//Draw detection circle
	if (!playerDetection_->detected)
	{
		spriteBatch->draw(
			detectionTexture,
			sf::Vector2f(
				position_.x + size_.x / 2,
				position_.y + size_.y / 2),
			sf::IntRect(
				0,
				0,
				detectionTexture->getSize().x,
				detectionTexture->getSize().y),
			sf::Color(255, 255, 255, 255),
			sf::Vector2f(
				1 * (detectionSize_.x / detectionTexture->getSize().x ),
				1 * (detectionSize_.y / detectionTexture->getSize().y)),
			sf::Vector2f(
				detectionTexture->getSize().x/2,
				detectionTexture->getSize().y/2),
			rotation);
	}

	//Draw plane
	animatedTexture_.draw(spriteBatch,
		glm::vec4(
			position_.x,
			position_.y,
			size_.x,
			size_.y),
		color_);

}

glm::vec4 EnemyBaseClass::getHitBox()
{
	return glm::vec4(
		position_.x - hitBoxMargin_.x,
		position_.y - hitBoxMargin_.y,
		size_.x - hitBoxMargin_.z,
		size_.y - hitBoxMargin_.w);
}

glm::vec3 EnemyBaseClass::getDetectionCir() const
{
	return glm::vec3(position_.x + size_.x / 2,
		position_.y + size_.y / 2,
		detectionRadious);
}

void EnemyBaseClass::setLife(char life)
{
	life_ = life;
	hitColor_ = 0;
	if (life > 0)
	{
		system_.audioManager->playSound("resources/sfx/enemiehit.ogg");
	}
	else if (life <= 0)
	{
		system_.audioManager->playSound("resources/sfx/enemiedead.ogg", PotatoEngine::getRandomNumber(1, 5), false, 0.2);
	}
}

char EnemyBaseClass::getLife() const
{
	return life_;
}
