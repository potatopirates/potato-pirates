#ifndef POTATOPIRATES_HUD_H
#define POTATOPIRATES_HUD_H
#include "System.h"
#include <PotatoEngine\SpriteBatch.h>
#include <PotatoEngine\GUI.h>
#include "Score.h"
#include "Player.h"



class HUD
{
public:
	// Constructors
	HUD();
	HUD(System system, Player* player, Detection* playerDetection, Score* score);
	// Deconstructors
	~HUD();

	///<summary>
	///Updates the HUD.
	///</summary>
	///<param name="playerHealth">The health of the player.</param>
	///<param name="playerAmmo">The ammo of the player.</param>
	///<param name="fireworks">The fireworks available to the player.</param>
	void update(float deltaTime);

	///<summary>
	///Draws the HUD using the spritebatch
	///</summary>

	void draw(PotatoEngine::SpriteBatch* spriteBatch);

	void drawText(sf::RenderWindow* window, float alphaRemove = 0);
private:
	System system_;
	Player* player_;
	Score* score_;

	int displayScore_;
	int playerHealth_;
	int playerAmmo_;
	bool* isDetected_;
	sf::Color color_;
	//Detection elements
	Detection* playerDetection_;
	//Alerted
	PotatoEngine::GUILabel alertedLabel_;
	PotatoEngine::GUILabel alertedCounter_;
	//Detected
	int detectedAlarmShown_;
	PotatoEngine::GUILabel detectedLabel_;
	PotatoEngine::GUILabel detectedLabelOutline_;
	//ICEHUD
	sf::Texture* ICEHUDTexture_;
	PotatoEngine::GUILabel ICEHUDAmmoLabel_;

	PotatoEngine::GUILabel ICEHUDScore_;
	PotatoEngine::GUILabel ICEHUDScoreOutLine_;

	//Power ups
	glm::vec2 powerUpSize_;
	//Firework
	PotatoEngine::AnimatedTexture fireworksAnimatedButton_;
	PotatoEngine::AnimatedTexture fireworksActiveAnimatedButton_;
	//Moonshine
	PotatoEngine::AnimatedTexture moonshineAnimatedButton_;
	//Potato cloud
	PotatoEngine::AnimatedTexture potatoCloudAnimatedButton_;
};

#endif // POTATOPIRATES_HUD_H