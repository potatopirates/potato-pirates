#ifndef POTATOPIRATES_VICTORYSTATE_H
#define POTATOPIRATES_VICTORYSTATE_H
#include <PotatoEngine\SpriteBatch.h>
#include <PotatoEngine\GUI.h>
#include "GameState.h"
#include "System.h"
#include "Score.h"

class VictoryState:
	public GameState
{
public:
	// Constructors.
	VictoryState(System system, Score* score);

	// Deconstructors.
	~VictoryState();

	/// <summary>
	/// Updates the state.
	/// </summary>
	/// <param name="deltaTime">Time used for physics calculations.</param>
	/// <returns></returns>
	void update(float deltaTime);

	/// <summary>Draw the state on the system window.</summary>
	void draw();

	///<summary>Draws text on the victory screen</summary>
	void drawText(sf::RenderWindow* window, float alphaRemove = 0);

private:
	/// <summary>Initializes the buttons available in the state.</summary>
	void initializeButtons();
	void checkHighScore();

	// Variables.
	System system_;
	PotatoEngine::SpriteBatch spriteBatch_;
	Score* score_;
	// All state objects.
	std::vector<PotatoEngine::GUIButton> GUIButtons_;

	// Background.
	PotatoEngine::GUIImage backgroundImage_;
	std::vector<PotatoEngine::GUILabel> scoreLabels_;
	std::string label_;
	std::vector<PotatoEngine::GUILabel> victoryCounts_;

	float blackScreen_;
};

#endif // POTATOPIRATES_VICTORYSTATE_H