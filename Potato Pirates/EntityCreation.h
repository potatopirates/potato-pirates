#ifndef POTATOPIRATES_ENEMYCREATION_H
#define POTATOPIRATES_ENEMYCREATION_H

#include "PotatoEngine\SpriteBatch.h"
#include "System.h"
#include "EnemyBaseClass.h"
#include "TMXLoader\TMXLoader.h"

//#include "Player.h"

class EntityCreation
{
public:
	//Constructors
	EntityCreation();
	EntityCreation(System system, Detection* playerDetection, std::vector<EnemyBaseClass*>* enemies, std::vector<Pickup>* pickUps);

	//Deconstructors
	~EntityCreation();

	///<summary>Updates the wavenumber to be spawned and the delay timer between the waves. Calls the enemy spawn function</summary>
	void update(float deltaTime);

	///<summary>
	///Loads the enemy-wave information from a file.
	///</summary>
	///<param name ="filepath">The filepath of the file to load.</param>
	///<returns></returns>
	void loadWaves(std::string filepath);

	///<summary>
	///Creates the waves of enemies.
	///</summary>
	///<param name="waveNumber">The number of the wave to be created.</param>
	///<returns></returns>
	void waveCreation(int waveNumber);

private:
	System system_;

	std::vector<EnemyBaseClass*>* enemies_;
	std::vector<std::vector<std::string>> levelData_;
	std::vector<Pickup>* pickUps_;

	Detection* playerDetection_;
	int waveNumber_;
	float waveDelay_;
};

#endif //POTATOPIRATES_ENEMYCREATION_H