#ifndef POTATOPIRATES_DEFEATSTATE_H
#define POTATOPIRATES_DEFEATSTATE_H
#include <PotatoEngine\SpriteBatch.h>
#include <PotatoEngine\GUI.h>
#include "GameState.h"
#include "System.h"

class DefeatState :
	public GameState
{
public:
	// Constructors.
	DefeatState(System system);

	// Deconstructors.
	~DefeatState();

	/// <summary>
	/// Updates the state.
	/// </summary>
	/// <param name="deltaTime">Time used for physics calculations.</param>
	/// <returns></returns>
	void update(float deltaTime);

	/// <summary>Draw the state on the system window.</summary>
	void draw();
private:

	/// <summary>Initializes the buttons available in the state.</summary>
	void initializeButtons();

	// Variables.
	System system_;
	PotatoEngine::SpriteBatch spriteBatch_;
	
	// All selectable objects.
	std::vector<PotatoEngine::GUIButton> GUIButtons_;

	// Background image.
	PotatoEngine::GUIImage backgroundImage_;


	float gameOver_;
};

#endif POTATOPIRATES_DEFEATSTATE_H // POTATOPIRATES_DEFEATSTATE_H