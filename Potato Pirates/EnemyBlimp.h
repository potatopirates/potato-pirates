#ifndef POTATOPIRATES_ENEMYBLIMP_H
#define POTATOPIRATES_ENEMYBLIMP_H

#include "EnemyBaseClass.h"
class EnemyBlimp :
	public EnemyBaseClass
{
public:
	//Constructors
	EnemyBlimp();
	EnemyBlimp(System system, glm::vec2 startPosition, Detection* playerDetection);
	//Deconstructors
	~EnemyBlimp();

	void fire(std::vector<Bullet>* bullets, Player* player) override;
	void update(float deltaTime, Player* player) override;

private:
	float fireDelay_;
	glm::vec2 bulletDirection_;
};

#endif //POTATOPIRATES_ENEMYBLIMP_H