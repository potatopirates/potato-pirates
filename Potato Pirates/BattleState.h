#ifndef POTATOPIRATES_BATTLESTATE_H
#define POTATOPIRATES_BATTLESTATE_H
#include <PotatoEngine/SpriteBatch.h>
#include <glm\glm.hpp>
#include "GameState.h"
#include "System.h"
#include "Player.h"
#include "Bullet.h"
#include "HUD.h"
#include "Explosion.h"
#include "EnemyBaseClass.h"
#include "EntityCreation.h"
#include "Score.h"
#include "Pickup.h"
#include "ScoreFeedback.h"


#include "TMXLoader\TMXLoader.h"

class BattleState :
	public GameState
{
public:
	BattleState(System system, Score* score);
	~BattleState();

	// Main loop functions
	void update(float deltaTime);
	void draw();

private:
	//Functions
	void initilizeMenuButtons();

	void drawHitBoxs();

	void menuhandler(float deltaTime);
	void drawMenu();

	void killEnemy(int ID);

	//variables
	System system_;
	sf::View camera_;
	PotatoEngine::SpriteBatch spriteBatch_;
	
	bool gamePaused_;

	//Level laoding :)
	TMXLoader* loader;
	int levelNumber_;
	
	sf::Texture* testLevelTexture_;


	sf::Texture* cloudBackgroundTexture_;

	Player player_;

	HUD hud_;

	//Score handler
	Score* score_;
	std::vector<ScoreFeedback> scoreFeedbacks_;

	EntityCreation entityCreation_;

	std::vector<Explosion> explosions_;

	std::vector<Bullet> bullets_;
	std::vector<AbilityFirework> fireworks_;


	//AbilityFirework firework_;
	bool activeBomb();

	std::vector<Pickup> pickups_;

	//Enemys
	std::vector<EnemyBaseClass*> enemys_;

	float lvlProgress_;
	float cloudProgress_;

	//bool playerDetected_;

	Detection playerDetection_;

	bool drawHitBox_;


	float blackScreen_;
	



	//Menu
	std::vector<PotatoEngine::GUIButton> GUIMenuButtons_;
	PotatoEngine::GUIImage menuBackground_;
	PotatoEngine::GUILabel menuPauseLabel_;

	//TMP

	std::vector<std::string> levelData_;
	PotatoEngine::AudioManager propeler;
};

#endif // POTATOPIRATES_BATTLESTATE_H