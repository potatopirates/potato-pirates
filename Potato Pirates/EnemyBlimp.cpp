#include "EnemyBlimp.h"
#include <glm\gtx\rotate_vector.hpp>



EnemyBlimp::EnemyBlimp()
{
}

EnemyBlimp::EnemyBlimp(System system, glm::vec2 startPosition, Detection* playerDetection) :
	EnemyBaseClass(system, startPosition, playerDetection, 1)
{
	movementPatternID_ = LINEAR;
	animatedTexture_ = PotatoEngine::AnimatedTexture(system_.animationManager->getAnimation("Blimp"));

	size_ = glm::vec2(
		system_.cameraView->getSize().x / 8,
		system_.cameraView->getSize().y / 9);

	detectionSize_ = glm::vec2(size_.x * 2.3, size_.x * 2.3);
	detectionRadious = detectionSize_.x / 2;

	//Set life
	life_ = 5;
	//Setup hitbox
	hitBoxMargin_ = glm::vec4(
		size_.x * -0.3,
		0,
		size_.x * 0.55,
		0
		);
}


EnemyBlimp::~EnemyBlimp()
{
}

void EnemyBlimp::fire(std::vector<Bullet>* bullets, Player* player)
{
	if (playerDetection_->detected && fireDelay_ <= 0)
	{
		movementPatternID_ = HOMING;

		glm::vec2 bulletPos = position_;
		bulletPos.y += 37.5;
		bulletPos.x += size_.x / 3;
		bulletDirection_ = glm::vec2(
			-1,
			-1);


		for (int i = 0; i < 21; i++)
		{

			bullets->emplace_back(
				system_,
				bulletPos,
				bulletDirection_,
				BulletOwner::ENEMY
				);
			bulletDirection_ = glm::rotate(bulletDirection_, (glm::mediump_float)(360 / 20));
		}
		fireDelay_ = 120;
	}
}

void EnemyBlimp::update(float deltaTime, Player* player)
{

	if (hitColor_ < 255)
	{
		hitColor_ += 10 * deltaTime;
		color_.r = hitColor_;
		color_.g = hitColor_;
		color_.b = hitColor_;
	}
	else if (hitColor_ > 255)
	{
		hitColor_ = 255;
		color_.r = hitColor_;
		color_.g = hitColor_;
		color_.b = hitColor_;
	}


	if (fireDelay_ > 0)
	{
		fireDelay_ -= 1 * deltaTime;
	}

	//Move enemy plane
	movementPattern(deltaTime, player);
}
