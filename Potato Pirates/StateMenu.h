#ifndef POTATOPIRATES_STATEMENU_H
#define POTATOPIRATES_STATEMENU_H
#include "GameState.h"
#include "System.h"
#include <PotatoEngine/SpriteBatch.h>
#include <PotatoEngine\GUI.h>

class StateMenu : public GameState
{
public:
	//Constructors
	StateMenu(System system);
	//Deconstructor
	~StateMenu();

	/// <summary>
	/// Updates the state
	/// </summary>
	///<param name="deltaTime">Time used for physics calculating.</param>
	/// <returns></returns>
	void update(float deltaTime);

	/// <summary>Draw the state on the system window</summary>
	void draw();

private:
	void initilizeMenuButtons();

	//Variables
	System system_;

	PotatoEngine::SpriteBatch menuSpriteBatch_;

	std::vector<PotatoEngine::GUIButton> GUIMenuButtons_; //All menu objects
	//Background
	PotatoEngine::GUIImage backgroundImage_;
};

#endif // POTATOPIRATES_STATEMENU_H