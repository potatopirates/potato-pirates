#include "Camera2D.h"
#include <iostream>

//#include "CollisionManager.h"


namespace PotatoEngine
{
	Camera2D::Camera2D() 
		:
		position_(0.0f, 0.0f),
		camera_matrix_(1.0f),
		ortho_matrix_(1.0f),
		scale_(1.0f),
		needs_matrix_update_(true),
		screen_width_(500),
		screen_height_(500)
	{

	}


	Camera2D::~Camera2D()
	{
	}

	void Camera2D::init(int screenWidth, int screenHeight, CollisionManager *collisionmanager)
	{
		screen_width_ = screenWidth;
		screen_height_ = screenHeight;
		collision_manager_ = collisionmanager;
		//Builds orthoMatrix
		ortho_matrix_ = glm::ortho(0.0f, (float)screen_width_, 0.0f, (float)screen_height_);
	}

	//Update the camera matrix, (if needed)
	void Camera2D::update()
	{
		//Does only update if our positionor scale has changed.
		if (needs_matrix_update_)
		{
			//Camera Translation
			glm::vec3 translate(-position_.x + screen_width_ / 2, -position_.y + screen_height_ / 2, 0.0f);
			camera_matrix_ = glm::translate(ortho_matrix_, translate);

			//Camera Scale
			glm::vec3 scale(scale_, scale_, 0.0f);
			camera_matrix_ = glm::scale(glm::mat4(1.0f), scale) * camera_matrix_;


			needs_matrix_update_ = false;
		}
	}

	glm::vec2 Camera2D::convertScreenToWorld(glm::vec2 screenCoords)
	{
		//Invert Y direction
		screenCoords.y = screen_height_ - screenCoords.y;

		// Make it so 0 is the center
		screenCoords -= glm::vec2(screen_width_ / 2, screen_height_ / 2);
		//Scale the coordinates
		screenCoords /= scale_;
		//Translate with the camera position
		screenCoords += position_;
		//Returns the converted coords
		return screenCoords;
	}

	//return true if box is inside camera view.
	bool Camera2D::isBoxInView(const glm::vec2 & position, const glm::vec2 & dimensions)
	{

		glm::vec2 scaledScreenDimensions = glm::vec2(screen_width_, screen_height_) / (scale_);
		const float MIN_DISTANCE_X = dimensions.x / 2.0f + scaledScreenDimensions.x / 2.0f;
		const float MIN_DISTANCE_Y = dimensions.y / 2.0f + scaledScreenDimensions.y / 2.0f;

		glm::vec2 center_pos = position + dimensions / 2.0f;
		glm::vec2 center_camera_pos = position_;
		glm::vec2 distVec = center_pos - center_camera_pos;

		float xDepth = MIN_DISTANCE_X - abs(distVec.x);
		float yDepth = MIN_DISTANCE_Y - abs(distVec.y);

		if (xDepth > 0 && yDepth > 0)
		{
			return true;
		}
		return false;


		float tmp = scale_;
		scale_ = 2;

		//Create a temp collisionManager.
		//CollisionManager cm;
		glm::vec4 camera_box;

		glm::vec2 bottom_right = convertScreenToWorld(glm::vec2(screen_width_, screen_height_));
		glm::vec2 top_left = convertScreenToWorld(glm::vec2(0, 0));

		camera_box.x = position_.x - screen_width_ * scale_;
		camera_box.y = position_.y - screen_height_ * scale_;
		camera_box.w = screen_width_ / scale_;
		camera_box.z = screen_height_ / scale_;

		camera_box.x = position_.x - top_left.x * scale_;
		camera_box.y = position_.y - top_left.y * scale_;
		camera_box.w = bottom_right.x / scale_;
		camera_box.z = bottom_right.y / scale_;


		scale_ = tmp;
		//Check if the object colide with the camera object (aka inside view)
		if (collision_manager_->AABB(glm::vec4(position, dimensions), camera_box))
		{
			return true;
		}

		return false;
	}

	bool Camera2D::isBoxInView(const glm::vec4 & destRect)
	{
		//Just runs the isBoxinView code as its the same mathematic. 
		return isBoxInView(glm::vec2(destRect.x, destRect.y), glm::vec2(destRect.w, destRect.z));
	}

}