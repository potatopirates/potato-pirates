#ifndef POTATOPIRATES_GAMESTATE_H
#define POTATOPIRATES_GAMESTATE_H
enum GameStates
{
	STATE_NULL,
	STATE_MENU,
	STATE_NEWGAME,
	STATE_LOADGAME,
	STATE_HIGHSCORE,
	STATE_OPTIONS,
	STATE_CREDITS,
	STATE_BATTLESTATE,
	STATE_EXIT,
	STATE_GAMEOVER,
	STATE_VICTORY,
	STATE_MISSIONSELECT
};


//Gmae state base class
class GameState
{
public:

	virtual ~GameState() {};

	virtual void update(float deltaTime) = 0;
	virtual void draw() = 0;

};
#endif // POTATOPIRATES_GAMESTATE_H