#ifndef POTATOENGINE_INPUTMANAGER_H
#define POTATOENGINE_INPUTMANAGER_H
#include <unordered_map>
#include <glm/glm.hpp>

namespace PotatoEngine
{

	enum MouseKey
	{
		LEFT,
		RIGHT
	};

	/// <summary>
	///void pressKey() should be called whenever a key is pressed.
	///void releaseKey() should be called whenever a key is released.
	///void update() should be called whenever a frame or state is updated.
	///This is to make sure previouskey is correctly sorted.
	/// </summary>
	/// <returns></returns>
	class InputManager
	{
	public:
		//Constructors
		InputManager();

		//Deconstructors
		~InputManager();

		//Public functions

		/// <summary>
		/// Update the inputmanager, needs to be called first each update frame.
		///( Update m_previousKeyMap with m_keymap )
		/// </summary>
		/// <returns></returns>
		void update();
		
		/// <summary>
		/// Press the key, set in the data map that the key is down.
		/// </summary>
		///<param name="keyID">The key to change. exempel sf::Keyboard::Space</param>
		/// <returns></returns>
		void pressKey(unsigned int keyID);

		/// <summary>
		/// Release the key, set in the data map that the key is released.
		/// </summary>
		///<param name="keyID">The key to change. exempel sf::Keyboard::Space</param>
		/// <returns></returns>
		void releaseKey(unsigned int keyID);

		/// <summary>
		/// Returns true if the key is down.
		/// </summary>
		///<param name="keyID">The key to check for. exempel sf::Keyboard::Space</param>
		/// <returns></returns>
		bool isKeyDown(unsigned int keyID);
		
		/// <summary>
		/// Returns true if the key was pressed this frame but not the frame before.
		/// </summary>
		///<param name="keyID">The key to check for. exempel sf::Keyboard::Space</param>
		/// <returns></returns>
		bool isKeyPressed(unsigned int keyID);


		bool isAnyKeyDown();
		bool isAnyKeyPressed();

		void setKeyPressedStatus(int keyID, bool value);

		void pressMouse(MouseKey mouseKey = LEFT);
		void releaseMouse(MouseKey mouseKey = LEFT);
		bool isMouseDown(MouseKey mouseKey = LEFT);
		bool isMousePressed(MouseKey mouseKey = LEFT);


		//Setters
		/// <summary>
		/// Set the stored mouse cordinations to the inputed ints.
		/// </summary>
		///<param name="x">Mouse position in x direction</param>
		///<param name="y">Mouse position in y direction</param>
		/// <returns></returns>
		void setMouseCoords(int x, int y);

		/// <summary>
		/// Set the stored mouse cordinations to the inputed glm::vec2.
		/// </summary>
		///<param name="pos">Mouse position in a glm::vec2</param>
		/// <returns></returns>
		void setMouseCoords(glm::vec2 pos);

		//Getters
		/// <summary>
		/// Returns the mouse cordination in a glm::vec2 format.
		/// </summary>
		/// <returns></returns>
		glm::vec2 getMouseCoords() const { return mouseCoords_; }


		int getLatestKeyIDPressed();

		char getLatestKeyChar();

	private:
		bool wasKeyDown(unsigned int keyID);							///Returns was keyID was for value in m_previousKeyMap (if it was down last frame)
		bool wasMouseDown(MouseKey mouseKey);
		//Variables	
		std::unordered_map<unsigned int, bool> keyMap_;					///main keymap, used for checking if a key is down or not
		std::unordered_map<unsigned int, bool> previousKeyMap_;			///secondary keymap, used for checking if they key was pressed previous frame or not.
		glm::vec2 mouseCoords_;											///stores the cordination where the mouse is located on the screen.

		std::unordered_map<MouseKey, bool> mouseMap_;
		std::unordered_map<MouseKey, bool> previousMouseMap_;


		int latestKey_;
	};

}

#endif // POTATOENGINE_INPUTMANAGER_H