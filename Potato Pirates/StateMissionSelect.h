#ifndef POTATOPIRATES_STATEMISSIONSELECT_H
#define POTATOPIRATES_STATEMISSIONSELECT_H
#include "GameState.h"
#include "System.h"

#include <PotatoEngine\GUI.h>

class StateMissionSelect :
	public GameState
{
public:
	//Constructors
	StateMissionSelect(System system);
	//Deconstructor
	~StateMissionSelect();

	/// <summary>
	/// Updates the state
	/// </summary>
	///<param name="deltaTime">Time used for physics calculating.</param>
	/// <returns></returns>
	void update(float deltaTime);

	/// <summary>Draw the state on the system window</summary>
	void draw();

private:
	void initilizeMap();
	void initilizeButtons();
	void initilizeShop();

	System system_;

	PotatoEngine::SpriteBatch spriteBatch_;

	//All buttons
	std::vector<PotatoEngine::GUIButton> GUIButtons_;
	
	//Background
	PotatoEngine::GUIImage backgroundImage_;
	PotatoEngine::GUIImage hudImage_;

	PotatoEngine::GUIImage shopBackground_;

	std::vector<PotatoEngine::GUIImage> upgradeStars_;

	PotatoEngine::GUILabel moneyLabel_;

};

#endif // POTATOPIRATES_STATEMISSIONSELECT_H
