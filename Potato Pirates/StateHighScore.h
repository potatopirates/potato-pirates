#ifndef POTATOPIRATES_STATEHIGHSCORE_H
#define POTATOPIRATES_STATEHIGHSCORE_H
#include "GameState.h"
#include "System.h"

#include <PotatoEngine\GUI.h>

struct highScore
{
	highScore(std::string name, unsigned int score) :
		name_(name),
		score_(score)
	{
		/*Empty*/
	}
	std::string name_;
	unsigned int score_;
};
class StateHighScore : public GameState
{
public:
	//Constructors
	StateHighScore();
	StateHighScore(System system);
	//Deconstructors
	~StateHighScore();

	/// <summary>
	/// Updates the state
	/// </summary>
	///<param name="deltaTime">Time used for physics calculating.</param>
	/// <returns></returns>
	void update(float deltaTime);

	/// <summary>Draw the state on the system window</summary>
	void draw();
private:
	void initilizeButtons();
	void openHighScore();

	void loadHighScore();

	//Variables
	System system_;

	PotatoEngine::SpriteBatch spriteBatch_;

	std::vector<PotatoEngine::GUIButton> GUIButtons_;
	std::vector<PotatoEngine::GUISlider> GUISliders_;

	std::vector<PotatoEngine::GUICheckBox> GUICheckBoxs_;


	std::vector<PotatoEngine::GUILabel> highscoreLabels_;

	std::vector<highScore> highScoreList_;


	//Background
	PotatoEngine::GUIImage backgroundImage_;

};

#endif //POTATOPIRATES_STATEHIGHSCORE_H