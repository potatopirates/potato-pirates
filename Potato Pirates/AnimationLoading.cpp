#include "ResourceLoader.h"

void ResourceLoader::loadAnimations()
{
	sf::Texture* avatarTexture = system_.resourceManager->getTexture("resources/textures/avatar_spritesheet.png");

	system_.animationManager->createAnimation("Avatar_Idle", avatarTexture, 6);
	system_.animationManager->createAnimation("Avatar_TileDown", avatarTexture, 6);
	system_.animationManager->createAnimation("Avatar_TileUP", avatarTexture, 6);

	//Add Idle
	for (int y = 0; y < 2; y++)
	{
		for (int x = 0; x < 3; x++)
		{
			system_.animationManager->addFrameToAnimation("Avatar_Idle",
				glm::vec4(
					avatarTexture->getSize().x / 3 * x,
					avatarTexture->getSize().y / 6 * y,
					avatarTexture->getSize().x / 3,
					avatarTexture->getSize().y / 6
					));
		}
	}
	
	//Tile Down
	for (int y = 2; y < 4; y++)
	{
		for (int x = 0; x < 3; x++)
		{
			system_.animationManager->addFrameToAnimation("Avatar_TileDown",
				glm::vec4(
					avatarTexture->getSize().x / 3 * x,
					avatarTexture->getSize().y / 6 * y,
					avatarTexture->getSize().x / 3,
					avatarTexture->getSize().y / 6
					));
		}
	}

	//Tile up
	for (int y = 4; y < 6; y++)
	{
		for (int x = 0 ; x < 3; x++)
		{
			system_.animationManager->addFrameToAnimation("Avatar_TileUP",
				glm::vec4(
					avatarTexture->getSize().x / 3 * x,
					avatarTexture->getSize().y / 6 * y,
					avatarTexture->getSize().x / 3,
					avatarTexture->getSize().y / 6
					));
		}
	}
	//Firework
	sf::Texture* fireworkTexture = system_.resourceManager->getTexture("resources/textures/firework_spritesheet.png");
	system_.animationManager->createAnimation("Firework", fireworkTexture, 5);

	for (int y = 0; y < 2; y++)
	{
		for (int x = 0; x < 3; x++)
		{
			system_.animationManager->addFrameToAnimation("Firework",
				glm::vec4(
					fireworkTexture->getSize().x / 3 * x,
					fireworkTexture->getSize().y / 2 * y,
					fireworkTexture->getSize().x / 3,
					fireworkTexture->getSize().y / 2
					));
		}
	}
	
	//Explosion
	sf::Texture* explosionTexture = system_.resourceManager->getTexture("resources/textures/explosion_spritesheet.png");
	system_.animationManager->createAnimation("Explosion", explosionTexture, 6);

	for (int y = 0; y < 2; y++)
	{
		for (int x = 0; x < 4; x++)
		{
			system_.animationManager->addFrameToAnimation("Explosion",
				glm::vec4(
					explosionTexture->getSize().x / 4 * x,
					explosionTexture->getSize().y / 2 * y,
					explosionTexture->getSize().x / 4,
					explosionTexture->getSize().y / 2
					));
		}
	}


	//Bullets

	sf::Texture* bulletTexture = system_.resourceManager->getTexture("resources/textures/bullet_spritesheet.png");
	system_.animationManager->createAnimation("Bullet", bulletTexture, 6);

	for (int y = 0; y < 2; y++)
	{
		for (int x = 0; x < 2; x++)
		{
			system_.animationManager->addFrameToAnimation("Bullet",
				glm::vec4(
					bulletTexture->getSize().x / 2 * x,
					bulletTexture->getSize().y / 2 * y,
					bulletTexture->getSize().x / 2,
					bulletTexture->getSize().y / 2
					));
		}
	}

	sf::Texture* enemeyBulletTexture = system_.resourceManager->getTexture("resources/textures/enemybullet_spritesheet.png");
	system_.animationManager->createAnimation("EnemyBullet", enemeyBulletTexture, 6);

	for (int y = 0; y < 2; y++)
	{
		for (int x = 0; x < 2; x++)
		{
			system_.animationManager->addFrameToAnimation("EnemyBullet",
				glm::vec4(
					enemeyBulletTexture->getSize().x / 2 * x,
					enemeyBulletTexture->getSize().y / 2 * y,
					enemeyBulletTexture->getSize().x / 2,
					enemeyBulletTexture->getSize().y / 2
					));
		}
	}



//	 _____ _   _ _____ __  __ ___ _____ ____
//	| ____| \ | | ____|  \/  |_ _| ____/ ___|
//	|  _| |  \| |  _| | |\/| || ||  _| \___ \
//	| |___| |\  | |___| |  | || || |___ ___) |
//	|_____|_| \_|_____|_|  |_|___|_____|____/ 



	//Enemy, Heavy Gunner
	sf::Texture* gunnerTexture = system_.resourceManager->getTexture("resources/textures/hg_spritesheet.png");
	system_.animationManager->createAnimation("Gunner", gunnerTexture, 6);
	for (int x = 0; x < 3; x++)
		{
			system_.animationManager->addFrameToAnimation("Gunner",
				glm::vec4(
					gunnerTexture->getSize().x / 3 * x,
					0,
					gunnerTexture->getSize().x / 3,
					gunnerTexture->getSize().y
					));
		}
	
	
	//Enemy, Ace
	sf::Texture* aceTexture = system_.resourceManager->getTexture("resources/textures/ace_spritesheet.png");
	system_.animationManager->createAnimation("Ace", aceTexture, 6);
	for (int y = 0; y < 4; y++)
	{
		for (int x = 0; x < 3; x++)
		{
			system_.animationManager->addFrameToAnimation("Ace",
				glm::vec4(
					aceTexture->getSize().x / 3 * x,
					aceTexture->getSize().y / 4 * y,
					aceTexture->getSize().x / 3,
					aceTexture->getSize().y / 4
					));
		}
	}
	

	
	//Enemy, Hunter
	sf::Texture* hunterTexture = system_.resourceManager->getTexture("resources/textures/hunter_spritesheet.png");
	system_.animationManager->createAnimation("Hunter", hunterTexture, 6);
	for (int x = 0; x < 3; x++)
	{
		system_.animationManager->addFrameToAnimation("Hunter",
			glm::vec4(
				hunterTexture->getSize().x / 3 * x,
				0,
				hunterTexture->getSize().x / 3,
				hunterTexture->getSize().y
				));
	}

	//Enemy, Scout
	sf::Texture* scoutTexture = system_.resourceManager->getTexture("resources/textures/scout_spritesheet.png");
	system_.animationManager->createAnimation("Scout", scoutTexture, 6);
	for (int y = 0; y < 4; y++)
	{
		for (int x = 0; x < 3; x++)
		{
			system_.animationManager->addFrameToAnimation("Scout",
				glm::vec4(
					scoutTexture->getSize().x / 3 * x,
					scoutTexture->getSize().y / 4 * y,
					scoutTexture->getSize().x / 3,
					scoutTexture->getSize().y / 4
					));
		}
	}

	//Enemy, Blimp
	sf::Texture* blimpTexture = system_.resourceManager->getTexture("resources/textures/blimp_spritesheet.png");
	system_.animationManager->createAnimation("Blimp", blimpTexture, 6);
	for (int y = 0; y < 3; y++)
	{
		system_.animationManager->addFrameToAnimation("Blimp",
			glm::vec4(
				0,
				blimpTexture->getSize().y / 3 * y,
				blimpTexture->getSize().x,
				blimpTexture->getSize().y / 3
				));
	}




	//Noise circle
	sf::Texture* noiseTexture = system_.resourceManager->getTexture("resources/textures/noise_circle_spritesheet.png");
	system_.animationManager->createAnimation("NoiseCircle", noiseTexture, 3);

	for (int y = 0; y < 2; y++)
	{
		for (int x = 0; x < 11; x++)
		{
			system_.animationManager->addFrameToAnimation("NoiseCircle",
				glm::vec4(
					noiseTexture->getSize().x / 11 * x,
					noiseTexture->getSize().y / 2 * y,
					noiseTexture->getSize().x / 11,
					noiseTexture->getSize().y / 2
					));
		}
	}

	//Detection circle
	sf::Texture* detectionTexture = system_.resourceManager->getTexture("resources/textures/detection_circle_spritesheet.png");
	system_.animationManager->createAnimation("DetectionCircle", detectionTexture, 3);

	for (int y = 0; y < 2; y++)
	{
		for (int x = 0; x < 11; x++)
		{
			system_.animationManager->addFrameToAnimation("DetectionCircle",
				glm::vec4(
					detectionTexture->getSize().x / 11 * x,
					detectionTexture->getSize().y / 2 * y,
					detectionTexture->getSize().x / 11,
					detectionTexture->getSize().y / 2
					));
		}
	}

//Pick-up balloons
		sf::Texture* cargoBalloon = system_.resourceManager->getTexture("resources/textures/cargo_balloon_spritesheet.png");
		system_.animationManager->createAnimation("CargoBalloon", cargoBalloon, 25);
		for (int y = 0; y < 2; y++)
		{
			for (int x = 0; x < 3; x++)
			{
				system_.animationManager->addFrameToAnimation("CargoBalloon",
					glm::vec4(
						cargoBalloon->getSize().x / 3 * x,
						cargoBalloon->getSize().y / 2 * y,
						cargoBalloon->getSize().x / 3,
						cargoBalloon->getSize().y / 2
						));

			}
		}

		sf::Texture* fwBalloon = system_.resourceManager->getTexture("resources/textures/fw_balloon_spritesheet.png");
		system_.animationManager->createAnimation("FWBalloon", fwBalloon, 25);
		for (int y = 0; y < 2; y++)
		{
			for (int x = 0; x < 3; x++)
			{
				system_.animationManager->addFrameToAnimation("FWBalloon",
					glm::vec4(
						fwBalloon->getSize().x / 3 * x,
						fwBalloon->getSize().y / 2 * y,
						fwBalloon->getSize().x / 3,
						fwBalloon->getSize().y / 2
						));

			}
		}

		sf::Texture* moonshineBalloon = system_.resourceManager->getTexture("resources/textures/ms_balloon_spritesheet.png");
		system_.animationManager->createAnimation("MoonshineBalloon", moonshineBalloon, 25);
		for (int y = 0; y < 2; y++)
		{
			for (int x = 0; x < 3; x++)
			{
				system_.animationManager->addFrameToAnimation("MoonshineBalloon",
					glm::vec4(
						moonshineBalloon->getSize().x / 3 * x,
						moonshineBalloon->getSize().y / 2 * y,
						moonshineBalloon->getSize().x / 3,
						moonshineBalloon->getSize().y / 2
						));

			}
		}

		sf::Texture* potatoCloudBalloon = system_.resourceManager->getTexture("resources/textures/pc_balloon_spritesheet.png");
		system_.animationManager->createAnimation("PotatoCloudBalloon", potatoCloudBalloon, 25);
		for (int y = 0; y < 2; y++)
		{
			for (int x = 0; x < 3; x++)
			{
				system_.animationManager->addFrameToAnimation("PotatoCloudBalloon",
					glm::vec4(
						potatoCloudBalloon->getSize().x / 3 * x,
						potatoCloudBalloon->getSize().y / 2 * y,
						potatoCloudBalloon->getSize().x / 3,
						potatoCloudBalloon->getSize().y / 2
						));

			}
		}
//Screen effect for the moonshine ability
		sf::Texture* moonshineEffect = system_.resourceManager->getTexture("resources/textures/moonshine_spritesheet2.png");
		system_.animationManager->createAnimation("MoonshineEffect", moonshineEffect, 3);
		for (int y = 0; y < 3; y++)
		{
			for (int x = 0; x < 4; x++)
			{
				system_.animationManager->addFrameToAnimation("MoonshineEffect",
					glm::vec4(
						moonshineEffect->getSize().x / 4 * x,
						moonshineEffect->getSize().y / 3 * y,
						moonshineEffect->getSize().x / 4,
						moonshineEffect->getSize().y / 3
						));

			}
		}

//Power-up buttons for HUD
		sf::Texture* moonshineSymbol = system_.resourceManager->getTexture("resources/textures/moonshine.png");
		system_.animationManager->createAnimation("Moonshine", moonshineSymbol, 10);
		for (int y = 0; y < 4; y++)
		{
			for (int x = 0; x < 3; x++)
			{
				system_.animationManager->addFrameToAnimation("Moonshine",
					glm::vec4(
						moonshineSymbol->getSize().x / 3 * x,
						moonshineSymbol->getSize().y / 4 * y,
						moonshineSymbol->getSize().x / 3,
						moonshineSymbol->getSize().y / 4
						));
			}
		}

		sf::Texture* potatoCloudSymbol = system_.resourceManager->getTexture("resources/textures/potatocloud.png");
		system_.animationManager->createAnimation("PotatoCloud", potatoCloudSymbol, 10);
		for (int y = 0; y < 4; y++)
		{
			for (int x = 0; x < 3; x++)
			{
				system_.animationManager->addFrameToAnimation("PotatoCloud",
					glm::vec4(
						potatoCloudSymbol->getSize().x / 3 * x,
						potatoCloudSymbol->getSize().y / 4 * y,
						potatoCloudSymbol->getSize().x / 3,
						potatoCloudSymbol->getSize().y / 4
						));
			}
		}

		sf::Texture* fireworkSymbol = system_.resourceManager->getTexture("resources/textures/firework.png");
		system_.animationManager->createAnimation("FireworkButton", fireworkSymbol, 10);
		for (int y = 0; y < 4; y++)
		{
			for (int x = 0; x < 3; x++)
			{
				system_.animationManager->addFrameToAnimation("FireworkButton",
					glm::vec4(
						fireworkSymbol->getSize().x / 3 * x,
						fireworkSymbol->getSize().y / 4 * y,
						fireworkSymbol->getSize().x / 3,
						fireworkSymbol->getSize().y / 4
						));
			}
		}

		sf::Texture* fireworkActiveSymbol = system_.resourceManager->getTexture("resources/textures/control.png");
		system_.animationManager->createAnimation("FireworkActive", fireworkActiveSymbol, 5);
		for (int y = 0; y < 4; y++)
		{
			for (int x = 0; x < 3; x++)
			{
				system_.animationManager->addFrameToAnimation("FireworkActive",
					glm::vec4(
						fireworkActiveSymbol->getSize().x / 3 * x,
						fireworkActiveSymbol->getSize().y / 4 * y,
						fireworkActiveSymbol->getSize().x / 3,
						fireworkActiveSymbol->getSize().y / 4
						));
			}
		}


}


