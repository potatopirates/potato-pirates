#ifndef POTATOPIRATES_SCOREFEEDBACK_H
#define POTATOPIRATES_SCOREFEEDBACK_H

#include <PotatoEngine\SpriteBatch.h>
#include <PotatoEngine\GUI.h>
#include <glm\glm.hpp>
#include "System.h"


class ScoreFeedback
{
public:
	ScoreFeedback(System system, int score, glm::vec2 pos);
	~ScoreFeedback();

	bool update(float deltaTime);

	void draw(sf::RenderWindow* window);
private:
	System system_;
	glm::vec2 position_;
	int score_;
	float alpha_;

	PotatoEngine::GUILabel GUILabel_;

};

#endif // POTATOPIRATES_SCOREFEEDBACK_H