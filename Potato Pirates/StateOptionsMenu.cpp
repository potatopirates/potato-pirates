#include "StateOptionsMenu.h"



StateOptionsMenu::StateOptionsMenu(System system) :
	system_(system)
{
	switch (system_.window->getSize().x)
	{
	case 600: pickedResolution = 0; break;
	case 960: pickedResolution = 1; break;
	case 1440: pickedResolution = 2; break;
	case 1600: pickedResolution = 3; break;
	case 1920: pickedResolution = 4; break;
	case 4096: pickedResolution = 5; break;
	default: pickedResolution = 3;break;
	}

	//Initilize the menu buttons
	initilizeCheckBoxs();
	initilizeButtons();
	initilizeSliders();

	//Set up background
	backgroundImage_ = PotatoEngine::GUIImage(system_.resourceManager,
		glm::vec2(0),
		glm::vec2(
			system_.cameraView->getSize().x,
			
			system_.cameraView->getSize().y),
		"resources/textures/optionsBG.png", glm::vec4(0.0f));

	//Music
	if (system_.audioManager->getLatestMusicPlayed() != "resources/music/Fretless radio.ogg")
	{
		system_.audioManager->playMusic("resources/music/Fretless radio.ogg", true);
	}

	//Add resolution options
	resolutions_.emplace_back("600�480");
	resolutions_.emplace_back("960x540");
	resolutions_.emplace_back("1440x900");
	resolutions_.emplace_back("1600x900");
	resolutions_.emplace_back("1920x1080");
	resolutions_.emplace_back("4096�2304");


	resolutionLabel_ = PotatoEngine::GUILabel(
		system_.resourceManager,
		glm::vec2(
			system_.cameraView->getSize().x * 0.34,
			system_.cameraView->getSize().y * 0.26),
		resolutions_[pickedResolution],
		"resources/fonts/StardosStencil-Regular.ttf",
		system_.cameraView->getSize().y * 0.04
		);

	resolutionLabel_.setOrigin(PotatoEngine::StylePos::X_MIDDLE | PotatoEngine::StylePos::Y_MIDDLE);
	resolutionLabel_.setColor(sf::Color(0, 0, 0, 255));

}

StateOptionsMenu::~StateOptionsMenu()
{
}

void StateOptionsMenu::update(float deltaTime)
{
	for (unsigned int i = 0; i < GUIMenuButtons_.size(); i++)
	{
		GUIMenuButtons_[i].update(system_.inputManager, deltaTime);
	}

	for (unsigned int i = 0; i < GUISliders_.size(); i++)
	{
		GUISliders_[i].update(system_.inputManager, deltaTime);
	}

	for (unsigned int i = 0; i < GUICheckBoxs_.size(); i++)
	{
		GUICheckBoxs_[i].update(system_.inputManager, deltaTime);
	}

	for (unsigned int i = 0; i < GUINumberBoxs_.size(); i++)
	{
		GUINumberBoxs_[i].update(system_.inputManager, deltaTime);
	}


	if (system_.audioManager->getMusicVolume() != GUISliders_[0].getValue())
	{
		int newMusicVol = static_cast<int>(GUISliders_[0].getValue());
		system_.audioManager->setMusicVolume(static_cast<float>(newMusicVol));
		
		GUINumberBoxs_[0].setNumber(newMusicVol);
		GUISliders_[0].setValue(static_cast<float>(newMusicVol));

	}
	else if (system_.audioManager->getMusicVolume() != GUINumberBoxs_[0].getNumber())
	{
		int newMusicVol = GUINumberBoxs_[0].getNumber();

		system_.audioManager->setMusicVolume(static_cast<float>(newMusicVol));
		GUISliders_[0].setValue(static_cast<float>(newMusicVol));
	}

	if (system_.audioManager->getSoundVolume() != GUISliders_[1].getValue())
	{

		int newSoundVol = static_cast<int>(GUISliders_[1].getValue());
		system_.audioManager->setSoundVolume(static_cast<float>(newSoundVol));
		GUINumberBoxs_[1].setNumber(newSoundVol);
		GUISliders_[1].setValue(static_cast<float>(newSoundVol));
	}
	else if (system_.audioManager->getSoundVolume() != GUINumberBoxs_[1].getNumber())
	{
		int newSoundVol = GUINumberBoxs_[1].getNumber();

		system_.audioManager->setSoundVolume(static_cast<float>(newSoundVol));
		GUISliders_[1].setValue(static_cast<float>(newSoundVol));
	}

	



	if (resolutionLabel_.getText() != resolutions_[pickedResolution])
	{
		resolutionLabel_.setText(resolutions_[pickedResolution]);
		
		if (pickedResolution > 0)
		{
			GUIMenuButtons_[3].setEnabled(true);
		}

		if (pickedResolution < resolutions_.size()-1)
		{
			GUIMenuButtons_[4].setEnabled(true);
		}
	}
}

void StateOptionsMenu::draw()
{
	spriteBatch_.begin();

	//Draw the menu background
	backgroundImage_.draw(&spriteBatch_);

	//Draw all the gui menu objects.
	for (unsigned int i = 0; i < GUIMenuButtons_.size(); i++)
	{
		GUIMenuButtons_[i].draw(&spriteBatch_);
	}

	for (unsigned int i = 0; i < GUISliders_.size(); i++)
	{
		GUISliders_[i].draw(&spriteBatch_);
	}

	for (unsigned int i = 0; i < GUICheckBoxs_.size(); i++)
	{
		GUICheckBoxs_[i].draw(&spriteBatch_);
	}

	for (unsigned int i = 0; i < GUINumberBoxs_.size(); i++)
	{
		GUINumberBoxs_[i].draw(&spriteBatch_);
	}
	


	spriteBatch_.end();

	system_.window->draw(spriteBatch_);

	//Draw text
	for (unsigned int i = 0; i < GUIMenuButtons_.size(); i++)
	{
		GUIMenuButtons_[i].drawText(system_.window);
	}

	for (unsigned int i = 0; i < GUINumberBoxs_.size(); i++)
	{
		GUINumberBoxs_[i].drawText(system_.window);
	}

	resolutionLabel_.drawText(system_.window);
}

void StateOptionsMenu::initilizeButtons()
{
	float xHelper = static_cast<float>(system_.cameraView->getSize().x / 5.0f);
	float yHelper = static_cast<float>(system_.cameraView->getSize().y / 10.0f);

	float xSizeHelper = static_cast<float>(
		system_.cameraView->getSize().x / 2.0f - xHelper / 2);
	
	int* tmp = &pickedResolution;

	std::string imagePath = "resources/textures/Mainbutton_spritesheet.png";
	std::string fontPath = "resources/fonts/StardosStencil-Regular.ttf";
	unsigned int fontSize = 50;

	
	//Apply the settings button!

	GUIMenuButtons_.push_back(PotatoEngine::GUIButton(
		system_.resourceManager,
		glm::vec2(
			system_.cameraView->getSize().x * 0.65,
			system_.cameraView->getSize().y * 0.72),
		glm::vec2(xHelper + xHelper / 2, yHelper),
		imagePath,
		"Apply",
		fontSize,
		fontPath,
		[=](PotatoEngine::GUIButton& object, float deltaTime)
	{
		int w, h;
		switch (*tmp)
		{
		case 0: w = 600; h = 480; break;
		case 1: w = 960; h = 540; break;
		case 2: w = 1440; h = 900; break;
		case 3: w = 1600; h = 900; break;
		case 4: w = 1920; h = 1080; break;
		case 5: w = 4096; h = 2304; break;
		}

		
		sf::Uint32 winStyle;
		if (GUICheckBoxs_[0].isEnabled() && GUICheckBoxs_[1].isEnabled())
		{
			winStyle = sf::Style::None;
			w = sf::VideoMode::getDesktopMode().width;
			h = sf::VideoMode::getDesktopMode().height;
		}
		else if (GUICheckBoxs_[0].isEnabled())
		{
			winStyle = sf::Style::Fullscreen;
		}
		else if (GUICheckBoxs_[1].isEnabled())
		{
			winStyle = sf::Style::None;
		}
		else
		{
			winStyle = sf::Style::Titlebar;
		}

		system_.settingsParser->set("fullscreen", (GUICheckBoxs_[0].isEnabled()));
		system_.settingsParser->set("borderless", (GUICheckBoxs_[1].isEnabled()));


		system_.window->create(sf::VideoMode(w, h), "potato pirates", winStyle);
		system_.window->setView(*system_.cameraView);
		system_.window->setMouseCursorVisible(false); //Hide default cursor

		*system_.gameState = GameStates::STATE_OPTIONS;
		system_.audioManager->playSound("resources/sfx/button_click.ogg");
	}));

	//Save button!
	GUIMenuButtons_.push_back(PotatoEngine::GUIButton(
		system_.resourceManager,
		glm::vec2(
			system_.cameraView->getSize().x * 0.65,
			system_.cameraView->getSize().y * 0.83),
		glm::vec2(xHelper + xHelper / 2, yHelper),
		imagePath,
		"Save settings",
		fontSize,
		fontPath,
		[=](PotatoEngine::GUIButton& object, float deltaTime)
	{
		system_.settingsParser->set("music", system_.audioManager->getMusicVolume());
		system_.settingsParser->set("sfx", system_.audioManager->getSoundVolume());

		system_.settingsParser->set("width", static_cast<int>(system_.window->getSize().x));
		system_.settingsParser->set("height", static_cast<int>(system_.window->getSize().y));

		system_.settingsParser->set("fullscreen", (GUICheckBoxs_[0].isEnabled()));
		system_.settingsParser->set("borderless", (GUICheckBoxs_[1].isEnabled()));


		system_.settingsParser->saveToFile();


		*system_.gameState = GameStates::STATE_OPTIONS;
		system_.audioManager->playSound("resources/sfx/button_click.ogg");
	}));
	//Back
	GUIMenuButtons_.push_back(PotatoEngine::GUIButton(
		system_.resourceManager,
		glm::vec2(
			system_.cameraView->getSize().x * 0.05,
			system_.cameraView->getSize().y * 0.83),
		glm::vec2(xHelper + xHelper / 2, yHelper),
		imagePath,
		"Back",
		fontSize,
		fontPath,
		[=](PotatoEngine::GUIButton& object, float deltaTime)
	{
		*system_.gameState = GameStates::STATE_MENU;
		system_.audioManager->playSound("resources/sfx/button_click.ogg");
	}));


	
	//Arrow left button

	GUIMenuButtons_.push_back(PotatoEngine::GUIButton(
		system_.resourceManager,
		glm::vec2(
			system_.cameraView->getSize().x * 0.235,
			system_.cameraView->getSize().y * 0.23),
		glm::vec2(
			system_.cameraView->getSize().x * 0.03,
			system_.cameraView->getSize().y * 0.055),
		"resources/textures/arrowbtn_spritesheet.png",
		"",
		fontSize,
		fontPath,
		[=](PotatoEngine::GUIButton& object, float deltaTime)
	{
		*tmp = *tmp - 1;
		
		if (*tmp <= 0)
		{
			object.setEnabled(false);
		}	

		system_.audioManager->playSound("resources/sfx/button_click.ogg");
	}, *tmp > 0));

	GUIMenuButtons_.push_back(PotatoEngine::GUIButton(
		system_.resourceManager,
		glm::vec2(
			system_.cameraView->getSize().x * 0.415,
			system_.cameraView->getSize().y * 0.23),
		glm::vec2(
			system_.cameraView->getSize().x * 0.03,
			system_.cameraView->getSize().y * 0.055),
		"resources/textures/arrowbtnR_spritesheet.png",
		"",
		fontSize,
		fontPath,
		[=](PotatoEngine::GUIButton& object, float deltaTime)
	{
		*tmp = *tmp + 1;

		if (*tmp >= resolutions_.size()-1)
		{
			object.setEnabled(false);
		}
		system_.audioManager->playSound("resources/sfx/button_click.ogg");
	},
		*tmp < 5));



}

void StateOptionsMenu::initilizeSliders()
{
	float xHelper = static_cast<float>(system_.cameraView->getSize().x / 5.0f);
	float yHelper = static_cast<float>(system_.cameraView->getSize().y / 20.0f);

	float xSizeHelper = static_cast<float>(
		system_.cameraView->getSize().x * 0.63);

	//Music slider
	GUISliders_.emplace_back(system_.resourceManager,
		glm::vec2(
			system_.cameraView->getSize().x * 0.55,
			system_.cameraView->getSize().y * 0.27),
		glm::vec2(
			system_.cameraView->getSize().x * 0.31,// - system_.cameraView->getSize().x * 0.63,
			yHelper),
		"resources/textures/slider_spritesheet.png",
		0,
		100,
		system_.audioManager->getMusicVolume());

	//Audio slider
	GUISliders_.emplace_back(system_.resourceManager,
		glm::vec2(
			system_.cameraView->getSize().x * 0.55,
			system_.cameraView->getSize().y * 0.40),
		glm::vec2(
			system_.cameraView->getSize().x * 0.31,// - system_.cameraView->getSize().x * 0.63,
			yHelper),
		"resources/textures/slider_spritesheet.png",
		0,
		100,
		system_.audioManager->getSoundVolume());



	//Set up textboxz
	//Music
	GUINumberBoxs_.emplace_back(
		system_.resourceManager,
		glm::vec2(
			system_.cameraView->getSize().x * 0.865,
			system_.cameraView->getSize().y * 0.275),
		glm::vec2(
			system_.cameraView->getSize().x * 0.095,
			system_.cameraView->getSize().y * 0.04),
		"NULL",
		"resources/fonts/StardosStencil-Regular.ttf",
		static_cast<int>(system_.audioManager->getMusicVolume()),
		0,
		100,
		sf::Color(0,0,0,255)
		);

	//Sound
	GUINumberBoxs_.emplace_back(
		system_.resourceManager,
		glm::vec2(
			system_.cameraView->getSize().x * 0.865,
			system_.cameraView->getSize().y * 0.405),
		glm::vec2(
			system_.cameraView->getSize().x * 0.095,
			system_.cameraView->getSize().y * 0.04),
		"NULL",
		"resources/fonts/StardosStencil-Regular.ttf",
		static_cast<int>(system_.audioManager->getSoundVolume()),
		0,
		100,
		sf::Color(0, 0, 0, 255)
		);
}

void StateOptionsMenu::initilizeCheckBoxs()
{

	bool fullscreen, borderless;
	system_.settingsParser->get("fullscreen", fullscreen);
	system_.settingsParser->get("borderless", borderless);



	sf::WindowHandle handle = system_.window->getSystemHandle();

	GUICheckBoxs_.emplace_back(
		system_.resourceManager, 
		glm::vec2(
			system_.cameraView->getSize().x * 0.23,
			system_.cameraView->getSize().y * 0.34),
		glm::vec2(
			system_.cameraView->getSize().x * 0.04,
			system_.cameraView->getSize().y * 0.07),
		"resources/textures/checkbutton_spritesheet.png",
		fullscreen);

	GUICheckBoxs_.emplace_back(
		system_.resourceManager,
		glm::vec2(
			system_.cameraView->getSize().x * 0.23,
			system_.cameraView->getSize().y * 0.46),
		glm::vec2(
			system_.cameraView->getSize().x * 0.04,
			system_.cameraView->getSize().y * 0.07),
		"resources/textures/checkbutton_spritesheet.png",
		borderless);
}
