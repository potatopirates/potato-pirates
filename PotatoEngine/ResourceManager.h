#ifndef POTATOENGINE_RESOURCEMANAGER_H
#define POTATOENGINE_RESOURCEMANAGER_H

#include "TextureCache.h"
#include "SoundBufferCache.h"
#include <SFML\Audio.hpp>
namespace PotatoEngine
{
	class ResourceManager
	{
	public:
		//Constructors
		ResourceManager();
		//Deconstructors
		~ResourceManager();

		/// <summary>
		/// Returns a pointer to a texture of the inputed filepath.
		/// </summary>
		///<param name="texturePath">The file location of the texture</param>
		/// <returns></returns>
		sf::Texture* getTexture(std::string texturePath);
		
		/// <summary>
		/// Returns a pointer to a soundbuffer of the inputed filepath.
		/// </summary>
		///<param name="texturePath">The file location of the sound</param>
		/// <returns></returns>
		sf::SoundBuffer* getSoundBuffer(std::string soundPath);
	
	private:
		TextureCache textureCache_;
		SoundBufferCache soundBufferCache_;
	};
}

#endif // POTATOENGINE_RESOURCEMANAGER_H