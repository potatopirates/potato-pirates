#include "StateCredits.h"



StateCredits::StateCredits()
{
}

StateCredits::StateCredits(System system) :
	system_(system),
	movingBG_(0),
	moveWaiter_(0)
{
	//Initialize the state buttons.
	initilizeButtons();

	//Set up the background
	background_ = system_.resourceManager->getTexture("resources/textures/creditsBG.png");
	background_->setRepeated(true);

	system_.resetView();
	*system_.cursor = false;
}


StateCredits::~StateCredits()
{
}

void StateCredits::update(float deltaTime)
{
	for (unsigned int i = 0; i < GUIButtons_.size(); i++)
	{
		GUIButtons_[i].update(system_.inputManager, deltaTime);
	}

	if (moveWaiter_ > 60)
	{

		movingBG_ += deltaTime * 3;
	}
	else
	{

		moveWaiter_ += deltaTime;
	}

	if (system_.inputManager->isAnyKeyPressed())
	{
		*system_.gameState = GameStates::STATE_MENU;
	}
}

void StateCredits::draw()
{
	spriteBatch_.begin();

	//Draw the background

	spriteBatch_.draw(background_,
		sf::FloatRect(
			0,
			0, 
			system_.cameraView->getSize().x,
			system_.cameraView->getSize().y),
		sf::IntRect(
			0,
			movingBG_,
			system_.cameraView->getSize().x, 
			system_.cameraView->getSize().y ),
		sf::Color(255,255,255,255));

	//Draw the GUI objects
	for (unsigned int i = 0; i < GUIButtons_.size(); i++)
	{
		//GUIButtons_[i].draw(&spriteBatch_);
	}

	spriteBatch_.end();
	system_.window->draw(spriteBatch_);

	for (unsigned int i = 0; i < GUIButtons_.size(); i++)
	{
		//GUIButtons_[i].drawText(system_.window);
	}
}

void StateCredits::initilizeButtons()
{
	float xHelper = static_cast<float>(system_.cameraView->getSize().x / 5.0f);
	float yHelper = static_cast<float>(system_.cameraView->getSize().y / 10.0f);

	float xSizeHelper = static_cast<float>(
		system_.cameraView->getSize().x / 2.0f - xHelper / 2);


	glm::vec2 margin(
		system_.cameraView->getSize().x / 2 - xHelper - 50,
		150);
	std::string imagePath = "resources/textures/Mainbutton_spritesheet.png";
	std::string fontPath = "resources/fonts/StardosStencil-Regular.ttf";
	unsigned int fontSize = 50;

	GUIButtons_.push_back(PotatoEngine::GUIButton(
		system_.resourceManager,
		glm::vec2(
			system_.cameraView->getSize().x * 0.05,
			system_.cameraView->getSize().y * 0.83),
		glm::vec2(xHelper + xHelper / 2, yHelper),
		imagePath,
		"Back",
		fontSize,
		fontPath,
		[=](PotatoEngine::GUIButton& object, float deltaTime)
	{
		*system_.gameState = GameStates::STATE_MENU;
	}));

}
