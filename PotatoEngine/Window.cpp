#pragma once
#include "Window.h"

namespace PotatoEngine
{
	Window::Window()
		:
		m_iScreenWidth(0),
		m_iScreenHeight(0),
		m_sWindowName("")
	{
		//Empty
	}


	Window::~Window()
	{
		//Empty
	}

	int Window::createWindow(std::string windowName, int screenWidth, int screenHeight, bool fullscreen)
	{
		m_iScreenWidth = screenWidth;
		m_iScreenHeight = screenHeight;
		m_sWindowName = windowName;

		//Check if fullscreen or default.
		unsigned int flags;
		if (!fullscreen)
		{
			flags = sf::Style::Default;
		}
		else
		{
			flags = sf::Style::Fullscreen;
		}

		//Open an  Window
		m_pxWindow = new sf::Window(
			sf::VideoMode(m_iScreenWidth, m_iScreenHeight),
			m_sWindowName,
			flags,
			sf::ContextSettings(32));
		m_pxWindow->setVerticalSyncEnabled(true);

		if (m_pxWindow == nullptr)
		{
			//Handle error when failed to create window.
		}

		//Get OpenGL version on system and print.
		std::printf("*** OpenGL Version: %s ***\n", glGetString(GL_VERSION));

		//Set the background color to blue
		glClearColor(0.0f, 0.0f, 1.0f, 1.0f);

		//Enable alpha blending
		glEnable(GL_TEXTURE_2D);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


		//Return 0 if everythings is okey :)
		return 0;
	}

	void Window::setResolution(int width, int height)
	{
		m_iScreenWidth = width;
		m_iScreenHeight = height;

		//Change size of window
		m_pxWindow->setSize(sf::Vector2u(m_iScreenWidth, m_iScreenHeight));

		//Fix gl (most be called after resolution change to fix "glaps" and diffrnet problems that can happen.
		glViewport(0, 0, m_iScreenWidth, m_iScreenHeight);
		glMatrixMode(GL_PROJECTION);
		glOrtho(0, m_iScreenWidth, 0, m_iScreenHeight, -1, 1);
		glLoadIdentity();
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glClear(GL_COLOR_BUFFER_BIT);
		glLoadIdentity();
	}




	void Window::swapBuffer()
	{	
		// end the current frame (internally swaps the front and back buffers)
		m_pxWindow->display();
	}

	void Window::setFullscreen(bool fullscreen)
	{
		if (fullscreen)
		{
			m_pxWindow = new sf::Window(
				sf::VideoMode(m_iScreenWidth, m_iScreenHeight),
				m_sWindowName,
				sf::Style::Fullscreen,
				sf::ContextSettings(32));
			m_pxWindow->setVerticalSyncEnabled(true);
		}
		else
		{
			m_pxWindow = new sf::Window(
				sf::VideoMode(m_iScreenWidth, m_iScreenHeight),
				m_sWindowName,
				sf::Style::Default,
				sf::ContextSettings(32));
			m_pxWindow->setVerticalSyncEnabled(true);
		}
	}
}