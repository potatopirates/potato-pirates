#ifndef POTATOPIRATES_STATEOPTIONSMENU_H
#define POTATOPIRATES_STATEOPTIONSMENU_H
#include "GameState.h"
#include "System.h"

#include <PotatoEngine\GUI.h>
class StateOptionsMenu : public GameState
{
public:
	//Constructors
	StateOptionsMenu(System system);
	//Deconstructor
	~StateOptionsMenu();

	/// <summary>
	/// Updates the state
	/// </summary>
	///<param name="deltaTime">Time used for physics calculating.</param>
	/// <returns></returns>
	void update(float deltaTime);

	/// <summary>Draw the state on the system window</summary>
	void draw();

private:
	void initilizeButtons();
	void initilizeSliders();
	void initilizeCheckBoxs();

	//Variables
	System system_;

	PotatoEngine::SpriteBatch spriteBatch_;

	std::vector<PotatoEngine::GUIButton> GUIMenuButtons_;
	std::vector<PotatoEngine::GUISlider> GUISliders_;

	std::vector<PotatoEngine::GUICheckBox> GUICheckBoxs_;

	std::vector<PotatoEngine::GUINumerBox> GUINumberBoxs_;

	//Background
	PotatoEngine::GUIImage backgroundImage_;


	//Resolution variables
	std::vector<std::string> resolutions_;
	int pickedResolution;
	PotatoEngine::GUILabel resolutionLabel_;
};

#endif // POTATOPIRATES_STATEOPTIONSMENU_H