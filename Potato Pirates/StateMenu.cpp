#include "StateMenu.h"



StateMenu::StateMenu(System system) :
	system_(system)
{
	//Initilize the menu buttons
	initilizeMenuButtons();

	
	//Set up background

	backgroundImage_ = PotatoEngine::GUIImage(system_.resourceManager,
		glm::vec2(0),
		glm::vec2(system_.cameraView->getSize().x, system_.cameraView->getSize().y),
		"resources/textures/menuBG.png", glm::vec4(0.0f));

	//Music
	if (system_.audioManager->getLatestMusicPlayed() != "resources/music/Fretless radio.ogg")
	{
		system_.audioManager->playMusic("resources/music/Fretless radio.ogg", true);
	}

	system_.resetView();
	*system_.cursor = true;
}


StateMenu::~StateMenu()
{
}

void StateMenu::initilizeMenuButtons()
{

	float xHelper = static_cast<float>(system_.cameraView->getSize().x / 5.0f);
	float yHelper = static_cast<float>(system_.cameraView->getSize().y / 10.0f);

	float xSizeHelper = static_cast<float>(
		system_.cameraView->getSize().x / 2.0f - xHelper / 2);


	glm::vec2 margin(
		system_.cameraView->getSize().x / 2 - xHelper - 50,
		150);
	std::string imagePath = "resources/textures/Mainbutton_spritesheet.png";
	std::string fontPath = "resources/fonts/StardosStencil-Regular.ttf";
	unsigned int fontSize = 50;

	glm::vec2 buttonSize(xHelper, yHelper);
	std::string latestGame;
	system_.settingsParser->get("latestGame", latestGame);



	if (latestGame != "NULL") //Should check if there is a "latest game" to continue.
	{
		GUIMenuButtons_.push_back(PotatoEngine::GUIButton(
			system_.resourceManager,
			glm::vec2(
				xSizeHelper,
				yHelper * 0) + margin,
			glm::vec2(xHelper + xHelper / 2, yHelper),
			imagePath,
			"Continue",
			fontSize,
			fontPath,
			[=](PotatoEngine::GUIButton& object, float deltaTime)
		{
			std::string loadPath = *system_.gameDictionary;
			loadPath += "//saves//";
			loadPath += latestGame;
			loadPath += ".potato";

			system_.playerSaveData->loadFromFile(loadPath);
			*system_.gameState = GameStates::STATE_MISSIONSELECT;
			system_.audioManager->playSound("resources/sfx/button_click.ogg");
		}));
	}
	GUIMenuButtons_.push_back(PotatoEngine::GUIButton(
		system_.resourceManager,
		glm::vec2(
			xSizeHelper,
			yHelper * 1.2) + margin,
		glm::vec2(xHelper + xHelper / 2, yHelper),
		imagePath,
		"New Game",
		fontSize,
		fontPath,
		[=](PotatoEngine::GUIButton& object, float deltaTime)
	{
		*system_.gameState = GameStates::STATE_NEWGAME;
		system_.audioManager->playSound("resources/sfx/button_click.ogg");
	}));

	GUIMenuButtons_.push_back(PotatoEngine::GUIButton(
		system_.resourceManager,
		glm::vec2(
			xSizeHelper,
			yHelper * 2.4) + margin,
		glm::vec2(xHelper + xHelper / 2, yHelper),
		imagePath,
		"Load Game",
		fontSize,
		fontPath,
		[=](PotatoEngine::GUIButton& object, float deltaTime)
	{
		*system_.gameState = GameStates::STATE_LOADGAME;
		system_.audioManager->playSound("resources/sfx/button_click.ogg");
	}));



	GUIMenuButtons_.push_back(PotatoEngine::GUIButton(
		system_.resourceManager,
		glm::vec2(
			xSizeHelper,
			yHelper * 3.6) + margin,
		glm::vec2(xHelper + xHelper / 2, yHelper),
		imagePath,
		"High Score",
		fontSize,
		fontPath,
		[=](PotatoEngine::GUIButton& object, float deltaTime)
	{
		*system_.gameState = GameStates::STATE_HIGHSCORE;
		system_.audioManager->playSound("resources/sfx/button_click.ogg");
	}));

	GUIMenuButtons_.push_back(PotatoEngine::GUIButton(
		system_.resourceManager,
		glm::vec2(
			xSizeHelper,
			yHelper * 4.8) + margin,
		glm::vec2(xHelper + xHelper / 2, yHelper),
		imagePath,
		"Options",
		fontSize,
		fontPath,
		[=](PotatoEngine::GUIButton& object, float deltaTime)
	{
		*system_.gameState = GameStates::STATE_OPTIONS;
		system_.audioManager->playSound("resources/sfx/button_click.ogg");
	}));

	GUIMenuButtons_.push_back(PotatoEngine::GUIButton(
		system_.resourceManager,
		glm::vec2(
			xSizeHelper,
			yHelper * 6.0) + margin,
		glm::vec2(xHelper + xHelper / 2, yHelper),
		imagePath,
		"Credits",
		fontSize,
		fontPath,
		[=](PotatoEngine::GUIButton& object, float deltaTime)
	{
		*system_.gameState = GameStates::STATE_CREDITS;
		system_.audioManager->playSound("resources/sfx/button_click.ogg");
	}));

	GUIMenuButtons_.push_back(PotatoEngine::GUIButton(
		system_.resourceManager,
		glm::vec2(
			xSizeHelper,
			yHelper * 7.2) + margin,
		glm::vec2(xHelper + xHelper / 2, yHelper),
		imagePath,
		"Quit",
		fontSize,
		fontPath,
		[=](PotatoEngine::GUIButton& object, float deltaTime)
	{
		*system_.gameState = GameStates::STATE_EXIT;
		system_.audioManager->playSound("resources/sfx/button_click.ogg");
	}));
}


void StateMenu::update(float deltaTime)
{
	for (unsigned int i = 0; i < GUIMenuButtons_.size(); i++)
	{
		GUIMenuButtons_[i].update(system_.inputManager, deltaTime);
	}
}

void StateMenu::draw()
{
	menuSpriteBatch_.begin();

	//Draw the menu background
	backgroundImage_.draw(&menuSpriteBatch_);

	//Draw all the gui menu objects.
	for (unsigned int i = 0; i < GUIMenuButtons_.size(); i++)
	{
		GUIMenuButtons_[i].draw(&menuSpriteBatch_);
	}

	menuSpriteBatch_.end();

	system_.window->draw(menuSpriteBatch_);
	
	for (unsigned int i = 0; i < GUIMenuButtons_.size(); i++)
	{
		GUIMenuButtons_[i].drawText(system_.window);
	}
}

