# README #
Potato Pirates is a school project at Uppsala University Campus Gotland.

#Team:#

Lead Coder: Pontus Berglund

Lead Artist: Ricardo Aranda
## Coders: ##
* Pontus Berglund
* Erik Nilsson
* Jakob Sthillert
## Art: ##
* Peo Johansson
* Ricardo Aranda
* Sebastian Engstrand

If you find any unknown bugs in the game, be sure to report an issue and be as spesific as posible.

The main branch should for the most part be a working branch, all other codes will first have to go true a branch and QA check before being merged into the main branch.

Be sure to read the devblogs from the coders if you wish to keep up with how to code changes overtime:
https://drive.google.com/folderview?id=0B3mRAmV5nEDldE5vZ0ZfNC1UaDg&usp=sharing