#ifndef POTATOENGINE_SOUNDBUFFERCACHE_H
#define POTATOENGINE_SOUNDBUFFERCACHE_H

#include <map>
#include <SFML\Audio.hpp>

class SoundBufferCache
{
public:
	//Constructor
	SoundBufferCache();
	//Deconstructor
	~SoundBufferCache();

	/// <summary>
	/// Returns a pointer to a sound buffer stored in a map based on the file path.
	/// </summary>
	///<param name="soundPath">The file location of the sound</param>
	/// <returns></returns>
	sf::SoundBuffer* getSoundBuffer(std::string soundPath);

private:
	std::map<std::string, sf::SoundBuffer> soundBufferMap_;			///Map of all cached sound buffers.
};

#endif // POTATOENGINE_SOUNDBUFFERCACHE_H