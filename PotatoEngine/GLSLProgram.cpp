#include "GLSLProgram.h"
#include <SFML/OpenGL.hpp>

namespace PotatoEngine
{
	GLSLProgram::GLSLProgram()
	{
		//Empty
	}


	GLSLProgram::~GLSLProgram()
	{
		//Empty
	}

	//Compile our shader
	void GLSLProgram::compileShaders(
		const std::string &vertexShaderFilePath,
		const std::string &fragmentShaderFilepath)
	{
		//Laod our shaders from file (its also compile them)
		if (vertexShaderFilePath != "NULL" 
			&& !shader_.loadFromFile(vertexShaderFilePath, sf::Shader::Vertex))
		{
			// Error...
		}

		if (fragmentShaderFilepath != "NULL"
			&&!shader_.loadFromFile(fragmentShaderFilepath, sf::Shader::Fragment))
		{
			// Error...
		}

	}


	//Enable the shader and all its attributes we added with addAttribute
	void GLSLProgram::use()
	{
		sf::Shader::bind(&shader_);
	}

	//Disable the shader and all its attributes
	void GLSLProgram::unuse()
	{
		sf::Shader::bind(NULL);
	}

	sf::Shader * GLSLProgram::getShader()
	{
		return &shader_;
	}

}
