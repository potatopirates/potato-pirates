#ifndef POTATOPIRATES_ABILITYFIREWORK_H
#define POTATOPIRATES_ABILITYFIREWORK_H
#include "System.h"
#include <glm\glm.hpp>
#include <PotatoEngine\SpriteBatch.h>

class AbilityFirework
{
public:
	//Constructors
	AbilityFirework();
	AbilityFirework(System system, glm::vec2 pos);

	//Deconstructors
	~AbilityFirework();

	bool update(float deltaTime);
	void draw(PotatoEngine::SpriteBatch* spriteBatch);

	void detonate();


	glm::vec4 getHitBox() const;


	bool isActive() const { return active_; }

	void setActive(bool value) { active_ = value; }


	glm::vec3 getExplosionCir() const;

private:
	//Variables
	System system_;
	sf::Texture* texture_;
	PotatoEngine::AnimatedTexture animatedTexture_;

	sf::Texture* test_;

	glm::vec2 pos_;
	glm::vec2 explosionSize_;
	glm::vec4 hitBoxMargin_;

	float explosionRadious_;
	float speed_;

	float yOrgin;
	float ySpeed_;
	float disorientation_;

	bool explode_;
	bool active_;

	float movementHelper_;
	bool goUp_;
	glm::vec2 direction_;

	//Draw variables
	sf::IntRect uvRect_;
	sf::Color color_;



	sf::IntRect uvRect2_;
};

#endif //POTATOPIRATES_ABILITYFIREWORK_H