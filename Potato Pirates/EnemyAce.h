#ifndef POTATOPIRATES_ENEMYACE_H
#define POTATOPIRATES_ENEMYACE_H

#include "EnemyBaseClass.h"
class EnemyAce :
	public EnemyBaseClass
{
public:
	//Constructors
	EnemyAce();
	EnemyAce(System system, glm::vec2 startPosition, Detection* playerDetection);
	//Deconstructors
	~EnemyAce();

	void fire(std::vector<Bullet>* bullets, Player* player) override;
	void update(float deltaTime, Player* player) override;

private:
	float fireDelay_;
	float cooldown_;
	int counter_;
	glm::vec2 bulletDirection_;

};

#endif // POTATOPIRATES_ENEMYACE_H