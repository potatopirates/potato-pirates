#ifndef POTATOPIRATES_SYSTEM_H
#define POTATOPIRATES_SYSTEM_H
#include <PotatoEngine/GLSLProgram.h>
#include <PotatoEngine/InputManager.h>
#include <PotatoEngine/ResourceManager.h>
#include <PotatoEngine/AudioManager.h>
#include <PotatoEngine/CollisionManager.h>
#include <PotatoEngine/AnimationManager.h>
#include"PotatoEngine\SettingsParser.h"

#include "GameState.h"

//***************************************************************************
//*	System struct, keeps pointer to all diffrent system components			*
//*	Like audio manager, input manager etc									*
//***************************************************************************
struct System
{
	//Constructors
	System() { /*~Empty~*/ };
	System(GameStates *GameState,
		sf::RenderWindow *Window,
		sf::View *CameraView,
		PotatoEngine::GLSLProgram *TextureProgram,
		PotatoEngine::InputManager *InputManager,
		PotatoEngine::ResourceManager *ResourceManager,
		PotatoEngine::AudioManager *AudioManager,
		PotatoEngine::CollisionManager *CollisionManager,
		PotatoEngine::AnimationManager *AnimationManager,
		PotatoEngine::SettingsParser* SettingsParser,
		PotatoEngine::SettingsParser* PlayerSaveData,
		bool* Cursor,
		std::string* GameDictionary)
		:
		gameState(GameState),
		window(Window),
		cameraView(CameraView),
		textureProgram(TextureProgram),
		inputManager(InputManager),
		resourceManager(ResourceManager),
		audioManager(AudioManager),
		collisionManager(CollisionManager),
		animationManager(AnimationManager),
		settingsParser(SettingsParser),
		playerSaveData(PlayerSaveData),
		cursor(Cursor),
		gameDictionary(GameDictionary)
	{ /*Empty*/ };

	void resetView(sf::FloatRect rect = sf::FloatRect(0, 0, 1920, 1080))
	{
		cameraView->reset(rect);
		cameraView->setViewport(sf::FloatRect(0.f, 0.f, 1.0f, 1.0f));
		window->setView(*cameraView);
	}

	//Pointer variables of system.
	GameStates* gameState;
	sf::RenderWindow* window;
	sf::View* cameraView;
	PotatoEngine::GLSLProgram* textureProgram;
	PotatoEngine::InputManager* inputManager;
	PotatoEngine::ResourceManager* resourceManager;
	PotatoEngine::AudioManager* audioManager;
	PotatoEngine::CollisionManager* collisionManager;
	PotatoEngine::AnimationManager* animationManager;
	PotatoEngine::SettingsParser* settingsParser;

	bool* cursor;
	std::string* gameDictionary;


	PotatoEngine::SettingsParser* playerSaveData;

};

#endif // POTATOPIRATES_SYSTEM_H